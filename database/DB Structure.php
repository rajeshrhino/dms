<?php

##### Securing Mongo DB ########

// First start mongo with --auth
mongod --port 27017 --dbpath /Users/rajesh/Sites/mongodb/data/db

// Create DB admin user
use dms;
db.createUser(
  {
    user: "intdmsadmin",
    pwd: "D4e78htfb3d",
    roles: [ { role: "readWrite", db: "dms" } ]
  }
)

// Exit from the shell
// Start mongo with --auth
mongod --auth --port 27017 --dbpath /Users/rajesh/Sites/mongodb/data/db

// Login into mongo
mongo --port 27017 -u "intdmsadmin" -p "D4e78htfb3d" --authenticationDatabase "dms"

#####  Basic DMS DB stucture ##### 

// Import into Database - START

db.roles.insert({_id : 1, role_name : 'Super Admin'})
db.roles.insert({_id : 2, role_name : 'Account Admin'})
db.roles.insert({_id : 3, role_name : 'User'})

db.languages.insert({_id : 1, language: 'english', language_name : 'English', locale : 'en_GB'})

db.packages.insert({_id : 1, package_name : 'Individual', description: 'For individual use'})
db.packages.insert({_id : 2, package_name : 'Corporate', description: 'For company use'})

db.account_status.insert({_id : 1, status_name : 'Active'})
db.account_status.insert({_id : 2, status_name : 'Locked'})
db.account_status.insert({_id : 3, status_name : 'Pending'})
db.account_status.insert({_id : 4, status_name : 'Suspended'})
db.account_status.insert({_id : 5, status_name : 'Closed'})

db.users.insert({_id : 1, 
				  username : 'admin', 
				  password : '0ebJ7gmFvUZhM', 
				  account_status: NumberInt(1), 
                  first_name : 'System', 
				  last_name : 'Administrator', 
				  email : 'admin@intdms.com', 
                  created_date : NumberInt(1336993480), 
				  closed_date : '', 
				  login_date : '',
				  timezone  : 'Europe/London', 
                  language_id : 1, 
				  hash: '1f3870be274f6c49b3e31a0c6728957f',
				  role_id : NumberInt(1)
				  })

db.companies.insert({_id : 1, company_name : 'Int DMS', website: 'www.intdms.com' , street_address : 'Somewhere', city: 'London', postcode: 'WC1X'})

db.users_access.insert({_id : 1, user_id : 1, last_access_status : 1, failed_attempts : 0 , last_access : 1336993480, ip_address : '127.0.0.1'})

db.users_package.insert({_id : 1, user_id : 1, package_id : 1, company_id : 1 , is_free_account : 1, number_of_users : 1})

db.site_settings.insert({name: 'users_can_register' , value: 1})
db.site_settings.insert({name: 'invite_code_mandatory' , value: 1})
db.site_settings.insert({name: 'memcache_enabled', value: 1})
db.site_settings.insert({name: 'site_under_maintenance', value: 1})
db.site_settings.insert({name: 'global_from_email_address', value: 'admin@intdms.com'})
db.site_settings.insert({name: 'global_from_email_name', value: '{_int} DMS Admin'})
db.site_settings.insert({name: 'contact_us_receiving_email_address', value: 'rajeshrhino@gmail.com , boby.pv@gmail.com'})

db.invitecodes.insert({_id : 1, invite_code : 'INTDMS001', is_active: 1 , created_date : NumberInt(1336993480)})

// Import into Database - END
?>
