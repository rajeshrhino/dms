<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Document Management System  Password hasing 
 *
 * A library to calculate Salt ,creating password Hash and comparing the passwords etc
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Boby Varghese
 * @copyright   Copyright (c) 2012 Boby Vargehse 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */


class Password 
{
	
	function __construct() 
	{
		
	}
	/**
	 * 
	 * @param String $username
	 * @param String $password
	 * @return NULL|string
	 */
	public function makeSalt($username=null,$password=null) 
	{
		if(is_null($username)||is_null($password))
		{
			return null;
		}
		$salt_length = round(strlen($password)/2);
		$salt = $username .  $password;
		for ($i=0;$i<1000;$i++) 
		{
			$salt = md5($salt.substr($salt, $salt_length));
		}
		return $salt;
	}
	/**
	 * 
	 * @param String $password
	 * @param String $salt
	 * @param Integer $iterations
	 * @return NULL|string $hash
	 */
	public function createHash($password,$salt,$iterations) 
	{
		if(is_null($password)||is_null($salt))
		{
			return null;
		}
		if(empty($iterations) || !is_int($iterations)) {
			$iterations = 100;
		}
				
		$hash = md5($password,$salt);
		
		for ($i = 0; $i < $iterations; ++$i)
		{
			$hash = crypt($hash . $password,$salt);
		}		
		
		return $hash;
	}
}