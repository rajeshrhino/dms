<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Document Management System  Abstract calss loader 
 *
 * A library to calculate Salt ,creating password Hash and comparing the passwords etc
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Boby Varghese
 * @copyright   Copyright (c) 2012 Boby Vargehse 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */


class Abstractloader
{
	//private $ci;
    function __construct() {
    	//$this->ci =& get_instance();
    }
    public function load($filename=null) {
    	
    	if ($filename == '')
    	{
    		return;
    	}
    	
    	$path = '';    	
    	
    	if (($last_slash = strrpos($filename, '/')) !== FALSE)
    	{
    		// The path is in front of the last slash
    		$path = substr($filename, 0, $last_slash + 1);
    	
    		// And the file name behind it
    		$filename = substr($filename, $last_slash + 1);
    	}    	
    	
    	if ( file_exists(APPPATH.'libraries/'.$path.$filename.'.php'))
    	{
    		require_once(APPPATH.'libraries/'.$path.$filename.'.php');    		
    		
    		//$file = ucfirst($filename);    		
    	}
    	
    }
}
