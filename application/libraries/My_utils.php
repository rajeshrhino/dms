<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Document Management System Utils
 *
 * A library for commonly used functions in Document Management System
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class My_utils {

	/**
	 * MY_Utils Constructor
	 */
	
	public function __construct()
	{
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Drop a collection
	 *
	 * @access	public
	 * @params	string	collection name
	 */
	public function drop_collection($collection_name)
	{
		if (empty($collection_name))
		{
			return;
		}
		
		$CI =& get_instance();
			
		$count = $CI->mongo_db->count($collection_name);
		
		if ($count)
		{
			$CI->mongo_db->drop_collection($collection_name);
		}
	}
}

/* End of file MY_Utils.php */
/* Location: ./application/libraries/MY_Utils.php */