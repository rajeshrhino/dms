<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Document Management System Utils
 *
 * A library for commonly used functions in Document Management System
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class Timezone {

    public static $timezone = 'GMT';

	/**
	 * Timezone Constructor
	 */
	
	public function __construct()
	{
		
	}
	
	// --------------------------------------------------------------------

	/**
	 * Set time zone  
	 *
	 * @access	public
	 * @param	array    user details     
	 * @return	array	
	 */
    public function set_time_zone($timezone = NULL) 
    {
        if (empty($timezone)) 
        {
            $timezone = self::$timezone;
        }
        
        date_default_timezone_set($timezone); 
    }
    
    // --------------------------------------------------------------------

	/**
	 * Get time zones list  
	 *
	 * @access	public
	 * @param	array    user details     
	 * @return	array	
	 */
    public function get_time_zones() 
    {
        static $regions = array(
            'Africa' => DateTimeZone::AFRICA,
            'America' => DateTimeZone::AMERICA,
            'Antarctica' => DateTimeZone::ANTARCTICA,
            'Asia' => DateTimeZone::ASIA,
            'Atlantic' => DateTimeZone::ATLANTIC,
            'Europe' => DateTimeZone::EUROPE,
            'Indian' => DateTimeZone::INDIAN,
            'Pacific' => DateTimeZone::PACIFIC
        );
        
        $tzlist = array();
        
        foreach ($regions as $name => $mask) 
        {
            $time_zones_areas = DateTimeZone::listIdentifiers($mask);
            
            $time_zones_areas_refined = array();
            
            foreach ($time_zones_areas as $key => $value) 
            {
                $time_zones_areas_refined[$value] = $value;
            }
            $tzlist[$name] = $time_zones_areas_refined;
        }
        
        return $tzlist; 
    }
}

/* End of file MY_Utils.php */
/* Location: ./application/libraries/MY_Utils.php */