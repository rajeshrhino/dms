<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Document Management System Utils
 *
 * A library for commonly used functions in Document Management System
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

// ------------------------------------------------------------------------

/**
 * Helpers functions for Company
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
// --------------------------------------------------------------------
  
 
class CompanyLibrary {

	/**
	 * Users Constructor
	 */
	
	public function __construct()
	{
		
	}
	
	// --------------------------------------------------------------------
  
	/**
	 * Function to get users of a company
	 *
	 * @param 	int    $company_id		Company Id
	 * @access	public
	 * @return	array $users	Users of the company	
	 */
	function get_users_in_company($company_id)
	{
		if (empty($company_id))
		{
			return;
		}
		
		$CI =& get_instance();
		
		$CI->load->model('users_package');
		
		$users = $CI->users_package->get_records_count(array('company_id' => $company_id));
		
		return $users;
	}
}

/* End of file CompanyLibrary.php */
/* Location: ./application/libraries/CompanyLibrary.php */