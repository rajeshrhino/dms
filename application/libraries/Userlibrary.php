<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Document Management System Utils
 *
 * A library for commonly used functions in Document Management System
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

// ------------------------------------------------------------------------

/**
 * Helpers functions for users
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
// --------------------------------------------------------------------
  
 
class UserLibrary {

	/**
	 * Users Constructor
	 */
	
	public function __construct()
	{
		
	}
	
	// --------------------------------------------------------------------
  
	/**
	 * Function to get display name of a user
	 *
	 * @param 	object    $user		User object
	 * @access	public
	 * @return	string display_name
	 */
	function get_display_name($user)
	{
		if (empty($user))
		{
			return;
		}
		
		if (!empty($user->first_name) OR !empty($user->last_name)) 
		{
			$display_name = $user->first_name.' '.$user->last_name;
		}
		else 
		{
			$display_name = $user->username;
		}
		return $display_name;
	}
	
	// --------------------------------------------------------------------

	/**
	 * Function to get preferred date format of a user
	 *
	 * @param object    $user		User object
	 * @param string    $type		Long or short date format
	 * @access	public
	 * @return	string date_format
	 */
	function get_preferred_date_format($user , $type = 'short')
	{
		if (empty($user))
		{
			return;
		}
		
		if ($type == 'long')
		{
			return LONG_DATE_FORMAT;
		}
		
		return SHORT_DATE_FORMAT;
	}

	// --------------------------------------------------------------------
		
	/**
	 * Get logged in user details
	 *
	 * @static
	 * @access public
	 */
	function get_logged_in_user_details()
	{
		$CI =& get_instance();
		
		$session_data = $CI->session->userdata('logged_in');
		
		$CI->load->model('users');
			
		$user_details = $CI->users->get_single_record(array('_id' => $session_data['_id']));
		
		return $user_details;
	}
	
	// --------------------------------------------------------------------
		
	/**
	 * Load basic user information to $data array
	 *
	 * @param object    $user		User object
	 * @param array     $data		Data array
	 * @static
	 * @access public
	 */
	function load_basic_user_info($user , &$data)
	{
		$data['_id'] 		= $user->_id;
		
		$data['name'] 		= $this->get_display_name($user);
		
		$data['role_id'] 	= $user->role_id;
		
		$data['username'] 	= $user->username;
		
	}
	
	// --------------------------------------------------------------------

	/**
	 * Retrives the data from the form POST and composes an array of user details  
	 *
	 * @access	public
	 * @return	array	
	 */
    public function get_posted_user_details() 
    {
    	$CI =& get_instance();
		
        return array( 'username'        => $CI->input->post('username') , 
					  // Set some random string as password, as all new users will use one-time login link to login and set password
                      'password'        => substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", mt_rand(0, 50) , 1) . substr( md5( time() ), 1) ,
                      'account_status'  => 3 ,
                      //'package_id'      => 1 ,    
                      'first_name'      => $CI->input->post('first_name') ,
                      'last_name'       => $CI->input->post('last_name') ,
                      'email'           => $CI->input->post('email') , 
                      'created_date'    => time() ,
                      'closed_date'     => '' , 
					  'login_date'		=> '' ,
                      'timezone'        => $CI->input->post('timezone_id') , 
                      'language_id'     => $CI->input->post('language_id') ,
                      //'role_id'         => 3 ,
                      //'company_id'      => '' ,
                      'hash'            => md5(utf8_encode($CI->input->post('email'))) , 
                    );                  
    }
	
	// --------------------------------------------------------------------

	/**
	 * Retrives the data from the form POST and composes an array of company details  
	 *
	 * @access	public
	 * @return	array	
	 */
    public function get_posted_company_details() 
    {
    	$CI =& get_instance();
		
        return array( 'company_name'    => $CI->input->post('company_name') , 
                      'street_address'  => $CI->input->post('company_street_address') ,
                      'city'            => $CI->input->post('company_city') ,
                      'postcode'        => $CI->input->post('company_postcode') ,
                    );                  
    }
	
	// --------------------------------------------------------------------

	/**
	 * Delete a user and related data/documents/folders
	 *
	 * @access	public
	 * @return	array	
	 */
    public function delete_user($user_id)
    {
    	$CI =& get_instance();
			
		$CI->load->model('users');
		
		$CI->load->model('users_package');
	
		// Get user details
		$user_details = $CI->users->get_single_record(array('_id' => $user_id));
		$username = $user_details->username;
	
		// Delete the user record
		$CI->users->delete($user_id);
		
		// Delete the user package
		$CI->users_package->delete($user_id);
		
		$CI->load->library('my_utils');
			
		// Drop folders, files, chunks collection of the user
		$CI->my_utils->drop_collection($username.'.folders');
		$CI->my_utils->drop_collection($username.'.files');
		$CI->my_utils->drop_collection($username.'.chunks');
    }
	
	// --------------------------------------------------------------------

	/**
	 * Checks the current password of user
	 *
	 * @access	public
	 * @return	bool	
	 */
    public function check_current_password($uid , $password) 
    {
		$CI =& get_instance();
		
        $CI->load->model('users');
		
		$CI->load->library('password');
		
		// Get user details
		$user_details = $CI->users->get_single_record(array('_id' => $uid));
		
		$salt = $CI->password->makeSalt($user_details->username, $password);
        
        $hash_password = $CI->password->createHash($password, $salt, null);
		
		if($hash_password !== $user_details->password)
        {
            $data['passcurr_error'] = 'Your current password is incorrect';
            
            return FALSE; 
        }  
        
        return TRUE;
    }
	
	// --------------------------------------------------------------------
    
    /**
	 * Login the user and create session
	 *
	 * @access	public
	 * @params  object     user object
	 * @return	void
	 */
    public function login_user($user) 
    {
		if (empty($user->_id)) 
        {
            return;
        }
		
		$CI =& get_instance();
		
		$CI->session->set_userdata('logged_in', array('_id' => $user->_id));
	}
	
	// --------------------------------------------------------------------
    
    /**
	 * Logout the user and destroy session
	 *
	 * @access	public
	 * @return	void
	 */
    public function logout_user() 
    {
		$CI =& get_instance();
		
		$CI->session->sess_destroy('logged_in');
	}
	
	// --------------------------------------------------------------------
    
    /**
	 * Validate the invitation code
	 *
	 * @access	public
	 * @return	void
	 */
    public function validate_invite_code($invite_code , &$data) 
    {
		if (empty($invite_code)) 
        {
            return;
        }
		
		$CI =& get_instance();
		
		$CI->load->model('invitecodes');
		
		$invite_code_details = (array) $CI->invitecodes->get_single_record(array('invite_code' => $invite_code));

		if (!empty($invite_code_details) && $invite_code_details['is_active'] == 1)
		{
			$data['is_valid'] = TRUE;
		}
		else
		{
			$data['invite_code_error'] = $CI->lang->line('error_invite_code_invalid');
			
			$data['is_valid'] = FALSE;
		}
	}
	
	// --------------------------------------------------------------------
    
    /**
	 * Record the failed login attempts and block the account
	 *
	 * @access	public
	 * @params  object     user object
	 * @return	void
	 */
    public function record_last_login($user) 
    {
        if (empty($user->_id)) 
        {
            return;
        }
		
        $CI =& get_instance();
		
        $CI->load->model('users_access');
        
        $user_access = $CI->users_access->get_single_record(array('user_id' => $user->_id));
       
        if ( isset($user_access) AND $user_access->failed_attempts >= 3) 
        {
            $CI->users->update($user->_id , array('account_status' => 0));
            
            $CI->users_access->update_last_access($user->_id , array('last_access_status' => 0 , 'failed_attempts' => ++$user_access['failed_attempts']), 'update');                
        }
        else 
        {
            $CI->users_access->update_last_access($user->_id , array('last_access_status' => 1 , 'failed_attempts' => 0) , 'insert');    
        }
    } 
}

/* End of file Users.php */
/* Location: ./application/libraries/Users.php */
