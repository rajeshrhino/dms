<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Document Management System Utils
 *
 * A library for commonly used functions in Document Management System
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

// ------------------------------------------------------------------------

/**
 * Library for files/folders
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
// --------------------------------------------------------------------
  
 
class Files {

	/**
	 * Files Constructor
	 */
	
	public function __construct()
	{
		
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get the breadcrumbs for folder/file view
	 */
	public function get_bread_crumbs($folder_id , $username , &$breadcrumbs)
    {
		$CI =& get_instance();
		
		if ($folder_id == 0)
		{
			return;
		}
		
		$folder_id = (int) $folder_id;
		
		$CI->load->model('folders');
		
		$folder = $CI->folders->get_single_record(array('_id' => $folder_id) , $username);
		
		$breadcrumbs[$folder->_id] = array($folder->name , 'document/'.$folder->_id.'/list');
		
		if ($folder->parent_id != 0)
		{
			self::get_bread_crumbs($folder->parent_id , $username , $breadcrumbs);
		}
	}
}

/* End of file Files.php */
/* Location: ./application/libraries/Files.php */