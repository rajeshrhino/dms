<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * file Management System  File Upload 
 *
 * A library to upload and download files
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan  
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */


class UploadHandler extends MY_Model
{
    /*
    *  Function to upload files
    */     
    public function post($files , $username , $folder_id = NULL) 
    {
        if (empty($username))
        {
            return;
        }
        
        $this->connect();
        
        $grid = $this->mongo_db->getGridFS($username);
        
        $filetype = $files['file']['type'];
        
        $storedfile = $grid->storeUpload("file" , array("username" => $username , "file_type" => $filetype , "folder_id" => $folder_id));
         
        return $storedfile;
    }
    
    /*
    *  Function to view files
    */
    public function viewlist($username , $folder_id , $preferred_date_format = 'D d M Y H:i:s') 
    {
        if (empty($username))
        {
            return;
        }
        
        $this->connect();

        $grid = $this->mongo_db->getGridFS($username);
        
        $files = array();
        
        $i = 0;
        	
        foreach ($grid->find() as $file) 
        {
			if ($file->file["folder_id"] != $folder_id)
			{
				continue;
			}
			
    		$id = (string) $file->file['_id'];

            $files[$i]['id'] = $id;    

            $files[$i]['file_name'] = htmlspecialchars($file->file["filename"]); 
            
            $files[$i]['file_type'] = isset($file->file["file_type"]) ? $file->file["file_type"] : 'application/octet-stream';
            
            $files[$i]['uploaded_date'] = date($preferred_date_format , $file->file['uploadDate']->sec);
			
			// This field is used for sorting the array of documents
			$files[$i]['uploaded_timestamp'] = $file->file['uploadDate']->sec;
			
			// Chunk size
			$files[$i]['length'] = $this->bytes_to_size($file->file['length'] , 2);
            
            $i++;
	    }
		
    	return $files;
    }
    
    /*
    *  Function to delete file
    */
    public function delete_file($fid , $username) 
    {
        if (empty($username) OR empty($fid))
        {
            return;
        }
        
        $this->connect();
        
        $grid = $this->mongo_db->getGridFS($username);
        
        $result = $grid->delete(new MongoId($fid));
    }
	
	/*
    *  Function to update file
    */
    public function update_file($fid , $username , $params) 
    {
        if (empty($username) OR empty($fid))
        {
            return;
        }
        
        $this->connect();
        
        $grid = $this->mongo_db->getGridFS($username);
		
        $result = $grid->update(array("_id" =>new MongoId($fid)) , array('$set' => $params));
    }
    
    /*
    *  Function to get file
    */
    public function get_file($fid , $username) 
    {
        if (empty($username) OR empty($fid))
        {
            return;
        }
        
        $this->connect();
        
        $grid = $this->mongo_db->getGridFS($username);
        
        $file = $grid->findOne(array('_id' => new MongoId($fid)));
        
        return $file;
    }
	
	// --------------------------------------------------------------------
	
	/**
     * Convert bytes to human readable format
     *
     * @param integer bytes Size in bytes to convert
     * @return string
     */
    function bytes_to_size($bytes, $precision = 2)
    {  
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        $terabyte = $gigabyte * 1024;
       
        if (($bytes >= 0) && ($bytes < $kilobyte)) {
            return $bytes . ' B';
     
        } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
            return round($bytes / $kilobyte, $precision) . ' KB';
     
        } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
            return round($bytes / $megabyte, $precision) . ' MB';
     
        } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
            return round($bytes / $gigabyte, $precision) . ' GB';
     
        } elseif ($bytes >= $terabyte) {
            return round($bytes / $terabyte, $precision) . ' TB';
        } else {
            return $bytes . ' B';
        }
    }

    function connect() {

        $config = $this->config->item('mongo_db')['default'];

        $dns = "mongodb://{$config['hostname']}:{$config['port']}/{$config['database']}";
        
        if(isset($config['no_auth']) == TRUE && $config['no_auth'] == TRUE)
        {
            $options = array();
        }
        else
        {
            $options = array('username'=>$config['username'], 'password'=>$config['password']);
        }
        $this->connect = new MongoClient($dns, $options);
        $this->mongo_db = $this->connect->selectDB($config['database']);
    }
    
}

