<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Dashboard Controller Class
 *
 * This class object enables the user to view the dashboard after logged in.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
class Dashboard extends MY_Controller 
{
	/**
	 * Dashboard Constructor
	 */
    public function __construct()
    {
        parent::__construct();
		
		// Check whether the user is logged in or not
		$this->check_logged_in();
		
		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
    }

    // --------------------------------------------------------------------

    public function index()
    {
		// Load the neccessary modals
		$this->load->model('users_access');
		$this->load->model('users');
		
		// Get the user last access details
		$user_access = $this->users_access->get_single_record(array('user_id' => $this->user->_id));
		
		// Assign values to the view/template
		if (!empty($user_access))
		{
			$this->data['access'] = date('dS M Y H:i' , $user_access->last_access);
			$this->data['ip_address'] = $user_access->ip_address;
		}
		
		$this->data['users_count'] = $this->users->count_users();
		
		$this->load_page('backend', 'backend/dashboard', TRUE, $this->data);
    }
    
    public function quickFileSearchJson() 
	{
		// Get the search string
		$search = $this->input->get("search");
    	$this->load->model('files');
		
    	$mainArr = array();    	
    	//$json = $this->files->get_records(array('filename'=>'/'.$search.'/','username' => $this->user->username),$this->user->username);
    	$json = $this->files->get_records(array( 'filename'=>array('$regex' =>$search,'$options'=>'i'),'username' => $this->user->username),$this->user->username);
    	foreach ($json as $fobj) 
		{
    		//$mainArr[] = array('file_name'=>$fobj->filename,'value'=> anchor(site_url(array('document',$fobj->folder_id,'list')), $fobj->filename, 'title='.$fobj->filename));
    		//$mainArr[] = array('file_name'=>$fobj->filename,'link'=> site_url(array('document',$fobj->folder_id,'list')));
			
			// Show the file name and link to the file info page where the user can view the file information and download the file
			$mainArr[] = array('file_name'=>$fobj->filename,'link'=> site_url(array('document/info',$fobj->folder_id,$fobj->_id))); 
    	}
    	//$datelsea['json'] = json_encode(array(array('file_name'=>'three'),array('file_name'=>$search),array('file_name'=>'one')));
    	//$data['json'] = json_encode($json);
        //$this->load_page('backend', 'json_view', FALSE, $data);
        //$this->load->view('json_view', $data);
        if(count($mainArr) < 1) 
		{
        	$data['json'] = json_encode(array(array('file_name' => 'No files found')));
        } 
		else 
		{
        	$data['json'] = json_encode($mainArr);
        }
        
        $this->load->view('json_view', $data);
    }
}