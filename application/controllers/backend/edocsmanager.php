<?php



class Edocsmanager extends MY_Controller
{
	/**
	 * Dashboard Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}
	/*******************************************
	 * Home page of edocs file manager
	 * created at : 28-DEC-2012
	 */
	public function index()
	{
		$data = array();
		$this->load_page('documents', 'documents/file_manager', TRUE, $data);
	}
	public function saveuploadeddocs()
	{
		//$this->output->enable_profiler(TRUE);
		$this->load->library('UploadHandler');
		if($this->input->is_ajax_request()) {
			$res =  new UploadHandler();
		}
	}
	public function listdocs()
	{
		//'elFinder','elFinderConnector','elFinderVolumeDriver','elFinderVolumeLocalFileSystem'
		$opts = array(
		 'debug' => true,
				/* 'roots' => array(
						array(
								'driver'        => 'MySQL',   // driver for accessing file system (REQUIRED)
								'path'          => BASEPATH.'/../img/gallery/',         // path to files (REQUIRED)
								'URL'           => 	'/dms/img/gallery/', // URL to files (REQUIRED)
								'accessControl' => 'access',             // disable and hide dot starting files (OPTIONAL)
								'attributes' => array(
										array(
												'pattern' => '/./', //You can also set permissions for file types by adding, for example, .jpg inside pattern.
												'read'    => true,
												'write'   => true,
												'locked'  => true
										)
								)
						)
				) */
			'roots' => array(
							array(
									'driver'        => 'MySQL',
									'host'   		=> 'localhost',
									'user'   		=> 'elf_user',
									'pass'   		=> 'elf_pass',
									'db'     		=> 'elfinder',
									'path'   		=> 1,
							)					
					)	
		);
		
		$this->abstractloader->load('efinder/elFinderVolumeDriver.class');
		//$this->abstractloader->load('efinder/elFinderVolumeLocalFileSystem');
		$this->abstractloader->load('efinder/elFinderVolumeMySQL.class');
		$this->abstractloader->load('efinder/elFinder.class');
		$this->abstractloader->load('efinder/elFinderConnector.class');
		//$this->load->library('efinder/ElFinderVolumeLocalFileSystem','elFinderVolumeLocalFileSystem');
		//$this->load->library('efinder/elFinderVolumeLocalFileSystem');
		
		//$this->load->library('efinder/ElFinder',$opts,'elFinder');
		//$this->load->library('efinder/ElFinderConnector',array('elFinderObj'=>$this->elFinder,'debug'=>true),'elFinderConnector');
		
		// run elFinder
		$connector = new elFinderConnector(new elFinder($opts));
		$connector->run();
		
	}
}