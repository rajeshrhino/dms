<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Upload file
 *
 * This class object is used for folder related functions.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
class Folder extends MY_Controller 
{
    /**
	 * File Upload Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();

		// Check permission for the current controller
		$this->check_permission(get_class($this));
    }

    // --------------------------------------------------------------------
    
    public function index()
    {
		
    }
	
	// --------------------------------------------------------------------
	
    /**
	 * Create a folder
	 *
	 * @access	public
	 * @params 	array	$_POST
	 * @return	boolean true or false
	 */
    public function create_folder()
    {
		$params = array();
		
		$params['name']			= $this->input->post('folder_name');
		
		$params['title'] 		= $this->input->post('folder_title');
		
		$params['description'] 	= $this->input->post('folder_description');
		
		if ($this->input->post('parent_id'))
		{
			$params['parent_id'] = (int) $this->input->post('parent_id');
		}
		else
		{
			$params['parent_id'] = 0;
		}
		
		if (!empty($params['name']))
		{
			if ($this->check_folder_exists('return') == FALSE)
			{
				$this->load->model('folders');
				
				$folder = $this->folders->insert($params , $this->user->username);
				
				$this->session->set_flashdata('message', $this->lang->line('message_folder_create_confirmation'));
				
				echo TRUE;
			}
			else
			{
				echo FALSE;
			}
		}	

    }

	// --------------------------------------------------------------------
	
    /**
	 * Edit a folder
	 *
	 * @access	public
	 * @params 	array	$_POST
	 * @return	boolean true or false
	 */
    public function edit_folder()
    {
		$params = array();
		
		$id						= (int) $this->input->post('folder_id');
		
		$params['name']			= $this->input->post('folder_name');
		
		$params['title'] 		= $this->input->post('folder_title');
		
		$params['description'] 	= $this->input->post('folder_description');
		
		if ($this->input->post('parent_id'))
		{
			$params['parent_id'] = (int) $this->input->post('parent_id');
		}
		else
		{
			$params['parent_id'] = 0;
		}
		
		if (!empty($params['name']))
		{
			if ($this->check_folder_exists_for_edit('return') == FALSE)
			{
				$this->load->model('folders');
				
				$folder = $this->folders->update($id , $params , $this->user->username);
				
				$this->session->set_flashdata('message', $this->lang->line('message_folder_edit_confirmation'));
				
				echo TRUE;
			}
			else
			{
				echo FALSE;
			}
		}	

    }
	
	// --------------------------------------------------------------------
	
    /**
	 * Delete a folder
	 *
	 * @access	public
	 * @params 	array	$_POST
	 * @return	boolean true or false
	 */
    public function delete_folder()
    {
		$params = array();
		
		$id						= (int) $this->input->post('folder_id');
		
		if (!empty($id))
		{
			if ($this->check_if_sub_folder_or_file_exists('return') == FALSE)
			{
				$this->load->model('folders');
				
				$folder = $this->folders->delete($id , $this->user->username);
				
				echo TRUE;
			}
			else
			{
				echo FALSE;
			}
		}	

    }
	
	// --------------------------------------------------------------------
	
    /**
	 * Create if a folder exists in the current path. Duplicate folders can exists in different directory
	 *
	 * @access	public
	 * @params 	array	$_POST
	 * @return	boolean true or false
	 */
    public function check_folder_exists($return_type = 'echo')
    {
		$is_exists = FALSE;
		
		$params = array();
		
		$params['name']			= $this->input->post('folder_name');
		
		if ($this->input->post('parent_id'))
		{
			$params['parent_id'] = (int) $this->input->post('parent_id');
		}
		else
		{
			$params['parent_id'] = 0;
		}
		
		if (!empty($params['name']))
		{
			$this->load->model('folders');
			
			$folder = $this->folders->get_single_record($params , $this->user->username);
			
			if ($folder)
			{
				$is_exists = TRUE;
			}
		}
		
		if ($return_type == 'return')
		{
			return $is_exists;
		}
		
		echo $is_exists;
    }
	
	// --------------------------------------------------------------------
	
    /**
	 * Create if a folder exists in the current path in the same name other than the folder which is edited.
	 *
	 * @access	public
	 * @params 	array	$_POST
	 * @return	boolean true or false
	 */
    public function check_folder_exists_for_edit($return_type = 'echo')
    {
		$is_exists = FALSE;
		
		$params = array();
		
		$name		= (string) $this->input->post('folder_name');
		
		$id			= (int) $this->input->post('folder_id');
		
		if ($this->input->post('parent_id'))
		{
			$params['parent_id'] = (int) $this->input->post('parent_id');
		}
		else
		{
			$params['parent_id'] = 0;
		}
		
		$this->load->model('folders');
		
		$folders = $this->folders->get_records($params , $this->user->username);
		
		//print_r ($_POST);exit;
		
		foreach ($folders as $folder)
		{
			if ($folder->_id != $id AND $folder->name == $name)
			{
				//echo "$folder->name == $name<br/>";
				$is_exists = TRUE;
			}
		}
		
		if ($return_type == 'return')
		{
			return $is_exists;
		}
		
		echo $is_exists;
    }
	
	// --------------------------------------------------------------------
	
    /**
	 * Create if a sub folder or file exists in a folder
	 *
	 * @access	public
	 * @params 	array	$_POST
	 * @return	boolean true or false
	 */
    public function check_if_sub_folder_or_file_exists($return_type = 'echo')
    {
		$is_exists = FALSE;
		
		$params = array();
		
		$id			= (int) $this->input->post('folder_id');
		
		$params['parent_id'] = $id;
		
		// Check if sub folder exists in the folder
		$this->load->model('folders');
		$folders = (array) $this->folders->get_records($params , $this->user->username);
		if ($folders)
		{
			$is_exists = TRUE;
		}
		
		// Check if files exists in the folder
		$this->load->library('uploadhandler');
		// Get the user's documents/files from the database for this current folder
		$files = $this->uploadhandler->viewlist($this->user->username , $id , '');
		//print_r ($files);exit;
		if ($files)
		{
			$is_exists = TRUE;
		}
		
		if ($return_type == 'return')
		{
			return $is_exists;
		}
		
		echo $is_exists;
    }
}

/* End of file folder.php */
/* Location: ./application/controllers/document/folder.php */