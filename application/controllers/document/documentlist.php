<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Document List Controller Class
 *
 * This class object enables user to view the list of documents and folders.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class Documentlist extends MY_Controller 
{
	/**
	 * Document List Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();

		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
		
		// Load related libraries
        $this->load->library('form_validation');
		$this->load->library('table');
    }

    // --------------------------------------------------------------------
    
    public function index()
    {
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['error_message'] = $this->session->flashdata('error_message');
		
		// Get the folder ID from URL
		$this->data['folder_id'] = (int) $this->uri->segment(2);
		
		// Get Bread crumbs 
		$this->load->library('files');
		$this->files->get_bread_crumbs($this->data['folder_id'] , $this->user->username , $breadcrumbs);
		$breadcrumbs[0] = array(lang('label_file_manager') , 'document/0/list'); // This is file manager home bread crumb
		
		$this->data['breadcrumbs'] = array_reverse($breadcrumbs);
		
		//Compose the table
		$tmpl = array (
					//'table_open'          => '<table id="documents_list_table" class="table table-striped table-bordered bootstrap-datatable datatable">',
					'table_open'          => '<table id="documents_list_table" class="table table-striped table bootstrap-datatable datatable">',

					'heading_row_start'   => '<tr>',
					'heading_row_end'     => '</tr>',
					'heading_cell_start'  => '',
					'heading_cell_end'    => '',

					'row_start'           => '<tr class="document_rows">',
					'row_end'             => '</tr>',
					'cell_start'          => '<td>',
					'cell_end'            => '</td>',

					'row_alt_start'       => '<tr class="document_rows">',
					'row_alt_end'         => '</tr>',
					'cell_alt_start'      => '<td>',
					'cell_alt_end'        => '</td>',

					'table_close'         => '</table>'
			  );

		$this->table->set_template($tmpl);
		
		$this->table->set_heading('<th width="5%">
								  <div class="btn-group">
								  <a href="#" data-toggle="dropdown" class="btn dropdown-toggle" title="Select"><span class="caret"></span></a>
								  <ul class="dropdown-menu">
								    <li><a href="#">'.anchor('#' , lang('label_folders'), array('onClick'=>'return select_files_or_folders(1);')).'</a></li>
									<li><a href="#">'.anchor('#' , lang('label_documents'), array('onClick'=>'return select_files_or_folders(2);')).'</a></li>
									<li><a href="#">'.anchor('#' , lang('label_all'), array('onClick'=>'return select_files_or_folders(3);')).'</a></li>
									<li><a href="#">'.anchor('#' , '<span class="icon-remove"></span> '.lang('label_none'), array('onClick'=>'return select_files_or_folders(4);')).'</a></li>
								  </ul>
								</div>
								  </th>', 
								  '<th width="5%">&nbsp;</th>', 
								  '<th width="20%">'.lang('label_name').'</th>', 
								  '<th width="15%">'.lang('label_type').'</th>' , 
								  '<th width="15%">'.lang('label_size').'</th>', 
								  '<th width="20%">'.lang('label_uploaded_date').'</th>', 
								  '<th width="5%">'.lang('label_actions').'</th>'
								  );
		//$this->table->set_heading('' , lang('label_name'), lang('label_type'), lang('label_size') , lang('label_uploaded_date'), lang('label_actions'));
		
		$is_data_available = 0;
		
		// Get the folder list
		$this->load->model('folders');
		
		if ($this->data['folder_id'] == '0')
		{
			$this->data['current_folder_name'] = lang('label_home');
		}
		else
		{
			$current_folder = $this->folders->get_single_record(array('_id' => $this->data['folder_id']) , $this->user->username);
			
			$this->data['current_folder_name'] = $current_folder->name;
		}
		
		$folders = $this->folders->get_records(array('parent_id' => $this->data['folder_id']) , $this->user->username);
		
		foreach($folders as $folder)
		{
			$folder_details = '';
			if ($folder->title)
			{
				$folder_details .= ' ('.$folder->title.')';
			}
			
			if ($folder->description)
			{
				$folder_details .= '<br /><small>'.$folder->description.'</small>';
			}
			
			$folder_details .= '<br /><br /><br /><br /><div id="'.$folder->_id.'" class="folder-action-links" style="display: none;">'.anchor('#' , lang('label_edit'), 
								array('id'=>$folder->_id, 'data-original-title' => lang('label_edit_folder').' `'.$folder->name.'`' , 'data-rel'=>'tooltip', "onClick"=>"return edit_folder(".$folder->_id.", '".$folder->name."' , '".$folder->title."' , '".$folder->description."' );")).
								' | '.
								anchor('#' , lang('label_delete') , array('id'=>$folder->_id, 'data-original-title' => lang('label_delete_folder').' `'.$folder->name.'`' , 'data-rel'=>'tooltip', "onClick"=>"return delete_folder(".$folder->_id.", '".$folder->name."');")).'</div>';
			
			$this->table->add_row(form_checkbox('folders[]', $folder->_id , FALSE , 'class="select-folders"'),
								 '<div class="folder-name" id="'.$folder->_id.'">'.anchor('document/'.$folder->_id.'/list' , '<span class="icon32 icon-color icon-folder-open" title="'.$folder->name.'"></span>').'</div>',
								 anchor('document/'.$folder->_id.'/list' , $folder->name).$folder_details,
								  NULL, 
								  NULL, 
								  NULL, 
								  '<br /><br /><br /><br /><br />'
								  );
			$is_data_available = 1;
		}
		
		$this->load->library('uploadhandler');
			
		// Get the user's documents/files from the database for this current folder
		$files = $this->uploadhandler->viewlist($this->user->username , $this->data['folder_id'] , $this->userlibrary->get_preferred_date_format($this->user , 'long'));
		
		foreach($files as $file)
		{
			$this->table->add_row(form_checkbox('files[]', $file['id'] , FALSE , 'class="select-files"'),
								  '<div class="file-name" id="'.$file['id'].'"><span class="icon32 icon-color icon-document" title="'.$file['file_name'].'"></span></div>',
								  anchor('document/info/'.$this->data['folder_id'].'/'.$file['id'] , $file['file_name']),
								  $file['file_type'], 
								  $file['length'], 
								  $file['uploaded_date'] , 
								  anchor('document/documentlist/download/'.$file['id'] , '<span class="icon icon-color icon-arrowthick-s" title="Download"></span>').'&nbsp;'.
								  anchor('document/documentlist/delete/'.$this->data['folder_id'].'/'.$file['id'] , '<span class="icon icon-color icon-close" title="Delete"></span>' , array('onClick' => "return confirm('".lang('message_delete_confirm')."')")).'<br /><br /><br /><br />'
								  );
			$is_data_available = 1;
		}
		
		if ($is_data_available == 1)
		{
			$this->data['table'] = $this->table->generate();
		}
		else
		{
			$this->data['table'] = '<div class="alert alert-info"><p>'.lang('message_folder_empty').'</p></div>';
		}
		
		$this->load_page('backend', 'document/documentlist', TRUE, $this->data);
    }
    
	// --------------------------------------------------------------------
    
    public function mass_action()
    {
		if ($this->input->post('mass_action'))
		{
			$mass_action = $this->input->post('mass_action');
		} 
		
		if ($mass_action == 3)
		{
			$folders = $this->input->post('folders');
			
			$files = $this->input->post('files');
			
			$delete_done = 0;
			
			// Delete the selected files
			if ($files)
			{
				foreach ($files as $dont_care => $fid)
				{
					$this->load->library('uploadhandler');
				
					$this->uploadhandler->delete_file($fid , $this->user->username);
					
					$delete_done++;
				}
			}
			
			if ($folders)
			{
				$folders_not_deleted = array();
				
				// Delete the selected folders
				foreach ($folders as $dont_care => $folder_id)
				{
					$this->load->model('folders');
									
					// Check whether subfolders or files exist in the folder
					if ($this->check_if_sub_folder_or_file_exists('return' , $folder_id) == FALSE)
					{
						$this->folders->delete((int) $folder_id , $this->user->username);
						
						$delete_done++;
					}
					else
					{
						$folder = $this->folders->get_single_record(array('_id' => (int) $folder_id) , $this->user->username);
						
						$folders_not_deleted[] = $folder->name; 
					}
				}
				
				// Set messages if any
				if (!empty($folders_not_deleted))
				{
					$this->session->set_flashdata('error_message', '<strong>'.@implode(', ', $folders_not_deleted).'</strong> '.$this->lang->line('cannot_delete_folders'));
				}
			}
			if ($delete_done > 0)
			{
				$this->session->set_flashdata('message', $this->lang->line('selected_delete_confirmation'));
			}
		}
		
		$folder_id = $this->input->post('parent_id');	
			
		redirect('document/'.$folder_id.'/list', 'refresh');
    }
	
    // --------------------------------------------------------------------
    
    public function delete()
    {
		$fid = $this->uri->segment(5);
		
		$folder_id = $this->uri->segment(4);
        
        if ($fid)
        {
            $this->load->library('uploadhandler');
            
            $this->uploadhandler->delete_file($fid , $this->user->username);
            
            $this->session->set_flashdata('message', $this->lang->line('message_delete_file_confirmation'));
        }
        else
        {
            $this->session->set_flashdata('message', $this->lang->line('message_cannot_delete_file'));
        }        
            
		redirect('document/'.$folder_id.'/list', 'refresh');
    }        
    
    // --------------------------------------------------------------------
    
    public function download()
    {
        $fid = $this->uri->segment(4);
        
        if ($fid)
        {
            $this->load->library('uploadhandler');
            
            $file = $this->uploadhandler->get_file($fid , $this->user->username);
            
            if (null === $file) 
            {
                header('HTTP/1.0 404 Not Found');
                
                exit;
            }
            $filename = $file->file["filename"];
            $filename = str_replace(" ", "_", $filename);
            $filetype = isset($file->file["filetype"]) ? $file->file["filetype"] : 'application/octet-stream';
        
            header('Content-Description: File Transfer');
            header('Content-type: '.$filetype);
            header('Content-disposition: attachment; filename='.$filename);
            echo $file->getBytes();
        }
    }
	
	// --------------------------------------------------------------------
	
    /**
	 * Create if a sub folder or file exists in a folder
	 *
	 * @access	public
	 * @params 	array	$_POST
	 * @return	boolean true or false
	 */
    public function check_if_sub_folder_or_file_exists($return_type = 'echo', $folder_id)
    {
		$is_exists = FALSE;
		
		$params = array();
		
		$params['parent_id'] = (int) $folder_id;
		
		// Check if sub folder exists in the folder
		$this->load->model('folders');
		$folders = (array) $this->folders->get_records($params , $this->user->username);
		if ($folders)
		{
			$is_exists = TRUE;
		}
		
		// Check if files exists in the folder
		$this->load->library('uploadhandler');
		// Get the user's documents/files from the database for this current folder
		$files = $this->uploadhandler->viewlist($this->user->username , $folder_id , '');
		//print_r ($files);exit;
		if ($files)
		{
			$is_exists = TRUE;
		}
		
		if ($return_type == 'return')
		{
			return $is_exists;
		}
		
		echo $is_exists;
    }
}

/* End of file documentlist.php */
/* Location: ./application/controllers/document/documentlist.php */