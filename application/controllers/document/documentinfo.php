<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Document View Controller Class
 *
 * This class object enables user to view document information and download the document.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class Documentinfo extends MY_Controller 
{
    /**
	 * Document Info Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();

		// Check permission for the current controller
		$this->check_permission(get_class($this));
	
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
    }

    // --------------------------------------------------------------------
    
    public function index()
    {
		// Get the folder ID from URL
		$this->data['folder_id'] = (int) $this->uri->segment(3);
		
		// Get Bread crumbs 
		$this->load->library('files');
		$this->files->get_bread_crumbs($this->data['folder_id'] , $this->user->username , $breadcrumbs);
		$breadcrumbs[0] = array(lang('label_file_manager') , 'document/0/list'); // This is file manager home bread crumb
		
		$this->data['breadcrumbs'] = array_reverse($breadcrumbs);
		
		$fid = $this->uri->segment(4);
		if ($fid)
        {
            $this->load->library('uploadhandler');
			// Get the file info	
            $file = $this->uploadhandler->get_file($fid , $this->user->username);
			
			// Assign the file info to the view/template
			$this->data['file_id'] = htmlspecialchars($file->file["_id"]); 
			$this->data['file_name'] = htmlspecialchars($file->file["filename"]); 
            $this->data['file_type'] = isset($file->file["file_type"]) ? $file->file["file_type"] : 'application/unknown';
			$this->data['uploaded_date'] = date($this->userlibrary->get_preferred_date_format($this->user , 'long') , $file->file['uploadDate']->sec);
			// Chunk size
			$this->data['length'] = $this->uploadhandler->bytes_to_size($file->file['length'] , 2);
		}	
		
		$this->load_page('backend', 'document/documentinfo', TRUE, $this->data);
    }
    
    // --------------------------------------------------------------------
    
    public function delete()
    {
		$fid = $this->uri->segment(5);
		
		$folder_id = $this->uri->segment(4);
        
        if ($fid)
        {
            $this->load->library('uploadhandler');
            
            $this->uploadhandler->delete_file($fid , $this->user->username);
            
            $this->session->set_flashdata('message', 'File deleted.');
        }
        else
        {
            $this->session->set_flashdata('message', 'Cannot Delete File.');
        }        
            
		redirect('document/'.$folder_id.'/list', 'refresh');
    }        
    
    // --------------------------------------------------------------------
    
    public function download()
    {
		$fid = $this->uri->segment(4);
        
        if ($fid)
        {
            $this->load->library('uploadhandler');
            
            $file = $this->uploadhandler->get_file($fid , $this->user->username);
            
            if (null === $file) 
            {
                header('HTTP/1.0 404 Not Found');
                
                exit;
            }
            $filename = $file->file["filename"];
            $filename = str_replace(" ", "_", $filename);
            $filetype = isset($file->file["filetype"]) ? $file->file["filetype"] : 'application/octet-stream';
        
            header('Content-Description: File Transfer');
            header('Content-type: '.$filetype);
            header('Content-disposition: attachment; filename='.$filename);
            echo $file->getBytes();
        }
    }
}

/* End of file documentlist.php */
/* Location: ./application/controllers/document/documentlist.php */