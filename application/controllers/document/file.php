<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * File view/upload/delete
 *
 * This class object is used to view/upload/delete file to the database.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class Document extends MY_Controller 
{
    /**
	 * File Upload Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();

		// Check permission for the current controller
		$this->check_permission(get_class($this));
    }

	// --------------------------------------------------------------------
    
    public function index()
    {
	
	}
	
    // --------------------------------------------------------------------
	
    /**
	 * Upload a file
	 *
	 * @access	public
	 * @params 	array	$_POST
	 */
    public function uploadfile()
    {
		$max_upload = (int)(ini_get('upload_max_filesize'));
			
		$max_upload_in_bytes = $max_upload * pow(1024,2);
		
		//print_r ($_FILES);exit;
		
		if (!empty($_FILES))
		{
			$acceptable = array(
				'application/pdf',
				'application/msword',
				'image/jpeg',
				'image/jpg',
				'image/gif',
				'image/png',
				'text/plain',
				'text/csv',
				'application/vnd.ms-excel',
				'application/vnd.ms-powerpoint',
				
			);
			
			$this->lang->load('message', 'english');
			
			if(($_FILES['file']['size'] >= $max_upload_in_bytes) OR $_FILES["file"]["size"] == 0) 
			{
				$errors[] = $this->lang->line('file_too_large').$max_upload.$this->lang->line('megabytes');
			}
			
			if(!in_array($_FILES['file']['type'], $acceptable) && (!empty($_FILES["file"]["type"]))) 
			{
				$errors[] = $this->lang->line('file_type_invalid');
			}
			
			if (!empty($errors))
			{
				$errors = @implode('<br />' , $errors);
				
				$data['json'] = '{"message":"'.$errors.'"}';
			}
			// Load User Library
			$this->load->library('userlibrary');
			// Get logged in user details
			$user = $this->userlibrary->get_logged_in_user_details();
			
			$folder_id = (int) $this->input->post('folder_id');
		
			$this->load->library('uploadhandler');
			
			$stored_file = $this->uploadhandler->post($_FILES , $user->username , $folder_id);
			
			if ($stored_file)
			{
				$data['json'] = '{"message":"File Upload Success"}';
			}
			
			$this->load_page('backend', 'json_view', FALSE, $data);
		}
    }
	
	// --------------------------------------------------------------------
	
    /**
	 * Drag and drop - update a file's folder
	 *
	 * @access	public
	 * @params 	array	$_POST
	 */
    public function drag_and_drop_update_file_folder($return_type = 'echo')
    {
		$status = FALSE;
		
		// Load User Library
		$this->load->library('userlibrary');
		// Get logged in user details
		$user = $this->userlibrary->get_logged_in_user_details();
	
		$folder_id		= $this->input->post('folder_id');
		
		$file_id		= $this->input->post('file_id');
		
		if (!empty($folder_id) AND !empty($file_id))
		{
			$this->load->library('uploadhandler');

			$file_details = $this->uploadhandler->get_file($file_id , $user->username);
			
			$update_file = $this->uploadhandler->update_file( $file_id , $user->username , array('folder_id' => $folder_id));
			
			$status = TRUE;
		
		}
		
		if ($return_type == 'return')
		{
			return $status;
		}
		
		echo $status;
	}
}

/* End of file file.php */
/* Location: ./application/controllers/document/file.php */