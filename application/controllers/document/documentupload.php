<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Account Setttings Controller Class
 *
 * This class object enables user to update password.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
class Documentupload extends MY_Controller 
{
    /**
	 * Document Upload Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();

		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
		
        $this->load->library('form_validation');
    }

    // --------------------------------------------------------------------
    
    public function index()
    {
		$max_upload = (int)(ini_get('upload_max_filesize'));
			
		$max_upload_in_bytes = $max_upload * pow(1024,2);
		
		$this->data['max_upload'] = $max_upload;
		
		$this->data['message'] = $this->session->flashdata('message');
		
		// Get the folder ID from URL
		$this->data['folder_id'] = (int) $this->uri->segment(2);
		
		// Get Bread crumbs 
		$this->load->library('files');
		$this->files->get_bread_crumbs($this->data['folder_id'] , $this->user->username , $breadcrumbs);
		$breadcrumbs[0] = array(lang('label_file_manager') , 'document/0/list'); // This is file manager home bread crumb
		
		$this->data['breadcrumbs'] = array_reverse($breadcrumbs);
		
		if ($this->data['folder_id'] != 0)
		{
			// Get the folder list
			$this->load->model('folders');
			
			$folder = $this->folders->get_single_record(array('_id' => $this->data['folder_id']) , $this->user->username);
			
			$this->data['folder_name'] = $folder->name; 
		}
		else
		{
			$this->data['folder_id'] = 0;
			
			$this->data['folder_name'] = 'Home';
		}
		
		
		$this->load_page('backend', 'document/documentupload', TRUE, $this->data);
		
		//$this->load_page('document/documentupload', TRUE, $data);
    }
}

/* End of file documentupload.php */
/* Location: ./application/controllers/document/documentupload.php */