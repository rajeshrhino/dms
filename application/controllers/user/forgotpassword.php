<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Forgot Password Controller Class
 *
 * This class object enables the user forgot password process.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class Forgotpassword extends MY_Controller 
{
    /**
	 * Login Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('form_validation');
    }

    // --------------------------------------------------------------------

    public function index()
    {
        $this->load->helper('form');
        
        //$this->form_validation->set_message('required', $this->lang->line('error_login_required'));
        
		if ($this->form_validation->run() == FALSE)
    	{
            $this->data['message'] = $this->session->flashdata('message');
            
            $this->data['registration_message'] = $this->session->flashdata('registration_message');
            
    		$this->load_page('website', 'user/forgotpassword', FALSE, $this->data);
    	} 
        else 
        {
            $this->send_password_reset_email();    
        }
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Send email with password reset link
	 *
	 * @access	public
	 * @return	void
	 */  
    public function send_password_reset_email() 
    {
        $this->load->model('users');

        $email = $this->input->post('email');
        
        // Check whether the user account exists
        $is_account_exists = $this->check_account_exists($email);
        
        if ($is_account_exists == false) 
        {
            $this->session->set_flashdata('message', $this->lang->line('message_account_not_exists'));
            
            redirect('forgotpassword', 'refresh');
            
            exit;
        }
        
        // Check whether the user account status is active
        $account_status = $this->check_account_status($email);
        
        if ($account_status['status'] == false) 
        {
            $this->session->set_flashdata('message', $this->lang->line($account_status['message']));
            
            redirect('forgotpassword', 'refresh');
        }
        
        $user = (array) $this->users->get_single_record(array('email' => $email));
		
        if(!empty($user)) 
        {
            $user['checksum'] = $this->generate_check_sum($user['_id']);
			
			$this->session->set_flashdata('registration_message', $this->lang->line('message_forgot_password_msg'));
                    
            // Send forgot password email to user
            $this->send_email($user, 'user_registration/forgotpassword', 'Reset your account password');
        } 
        
        $redirect_to = 'login';
        
        redirect($redirect_to, 'refresh');
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Check the account status whether it is locked
	 *
	 * @access	public
	 * @params  string     username     
	 * @return	void
	 */
    public function check_account_status($email) 
    {
        $account_status = array();
    
        $user = $this->users->get_single_record(array('email' => $email));
        
        if ($user->account_status != 1) 
        {
            $account_status['status'] = false;
            
            switch ($user->account_status) {
                case 2:
                    $account_status['message'] = 'message_account_locked';        
                    break;
                case 3:
                    $account_status['message'] = 'message_account_pending';
                    break;
                case 4:
                    $account_status['message'] = 'message_account_suspended';
                    break;
                case 5:
                    $account_status['message'] = 'message_account_closed';
                    break;    
            }   
        }
        else 
        {
            $account_status['status'] = true;
        }
        
        return $account_status;
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Check whether the user account exists
	 *
	 * @access	public
	 * @params  string     username     
	 * @return	void
	 */
    public function check_account_exists($email)
    {
        $user = $this->users->get_single_record(array('email' => $email));
        
        if (empty($user)) 
        {
            return false;   
        }
        
        return true;
    }
    
    // --------------------------------------------------------------------
  
    /**
	 * Logs out the user and redirect to home page
	 *
	 * @access	public
	 * @return	void
	 */  
  
    public function logout()
    {
        $this->session->sess_destroy();
        
        redirect('home', 'refresh');
    }
    
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */