<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Activate Account Controller Class
 *
 * This class object enables the user login process.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class Activeaccount extends MY_Controller 
{
    /**
	 * Activate Account Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('form_validation');
    }

    // --------------------------------------------------------------------

    public function index()
    {
		if ($this->input->post())
		{
			$post_details = $this->input->post();
			
			$val_array = @explode(VALUE_SEPERATOR, $post_details['checksum']);
			
			$user_id = (int)$val_array[3];
			
			$status = $this->validate_check_sum($user_id, $post_details['checksum']);
			
			if ($status) 
			{
				$this->session->set_flashdata('message', $this->lang->line('message_password_set_form'));
			
				// Log in the user and redirect to password reset page
				//$this->session->set_userdata('logged_in', array('_id' => $user_id));
				$this->load->model('users');
				
				// Load user object
				$user = $this->users->get_single_record(array('_id' => $user_id));
				
				// Login the user
				$this->userlibrary->login_user($user);
				
				// Record login information, after logging in the user
				$this->userlibrary->record_last_login($user);
				
				// Redirect to password reset form/page
				redirect('resetpassword/'.$post_details['checksum'] , 'refresh');
			}
			else
			{
				redirect('login' , 'refresh');
			}
		}
		else
		{
		
			$checksum = $this->uri->segment(3);
			
			$val_array = @explode(VALUE_SEPERATOR, $checksum);
			
			$user_id = (int)$val_array[3];
			
			/*$check_status = $this->check_account_status($user_id);
			
			if ($check_status) 
			{
				$this->session->set_flashdata('registration_message', $this->lang->line('message_account_already_activated'));
				
				redirect('login', 'refresh');    
			}*/
			
			// Check whether the URL is expired or already used
			$url_result = $this->check_checksum_url($user_id, (int)$val_array[1]);
			
			// If expired or already used, redirect to forgot password form.
			if ($url_result[0] === FALSE)
			{
				$this->session->set_flashdata('message', $url_result[1]);
				
				redirect('forgotpassword' , 'refresh');
			}
			
			$status = $this->validate_check_sum($user_id, $checksum);
			
			if ($status) 
			{
				$this->load->model('users');
				
				$user_details = $this->users->get_user_details(array('_id' => $user_id));
				
				$this->data['username'] = $user_details->username;
				
				$this->data['checksum'] = $checksum;
				
				$this->data['user_id'] = $user_id;

				// Calculate valid until datetime	
				$timestamp = $val_array[1];
				$valid_hours = $val_array[2];
				$this->data['valid_until'] = date('d M Y g:i A' , $timestamp + ($valid_hours * 60 * 60));
				
				// Send welcome email, if this is new account and is activated by the link
				if ($user_details->account_status == 3)
				{
					$user = $this->users->update($user_id, array('account_status' => 1));
					
					$user_details = $this->users->get_user_details(array('_id' => $user_id));
					
					$this->send_email($user_details, 'welcome', 'Welcome to {_int} DMS');
					
					$this->data['message'] = $this->lang->line('message_confirmation_success');
				}
				
				$this->load_page('website', 'user/activateaccount', FALSE, $this->data);
			} 
		}
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Check the account status whether it is activated or not
	 *
	 * @access	public
	 * @params  string     username     
	 * @return	void
	 */
    public function check_account_status($user_id) 
    {
        $user = $this->users->get_single_record(array('_id' => $user_id));
        
        if ($user->account_status == 3) 
        {
               return FALSE;
        }
        
        return TRUE;
    }
}

/* End of file activateaccount.php */
/* Location: ./application/controllers/activateaccount.php */