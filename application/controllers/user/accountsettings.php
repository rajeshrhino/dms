<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Account Setttings Controller Class
 *
 * This class object enables user to update password.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
session_start(); //we need to call PHP's session object to access it through CI

class Accountsettings extends MY_Controller 
{
    /**
	 * Accountsettings Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();
		
		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
		
        $this->load->library('form_validation');
    }

    // --------------------------------------------------------------------
    
    public function index()
    {
		$this->load->helper('form');
		
		$this->form_validation->set_message('required', '%s is required field');
		
		$this->form_validation->set_message('valid_email', $this->lang->line('valid_email_required'));
		
		$this->form_validation->set_message('matches', $this->lang->line('password_match_required'));
		
		$this->form_validation->set_message('min_length', $this->lang->line('min_length_required'));
		
		$this->data['user_id'] = $this->user->_id;
		
		
		$first_name = $this->input->post('first_name');
		if (!empty($first_name))
		{
			$this->data['first_name'] 	= $this->input->post('first_name');
		}
		else
		{
			$this->data['first_name'] 	= $this->user->first_name;
		}
		
		$last_name = $this->input->post('last_name');
		if (!empty($last_name))
		{
			$this->data['last_name'] 	= $this->input->post('last_name');
		}
		else
		{
			$this->data['last_name'] 	= $this->user->last_name;
		}
		
		$email = $this->input->post('email');
		if (!empty($email))
		{
			$this->data['email'] 	= $this->input->post('email');
		}
		else
		{
			$this->data['email'] 	= $this->user->email;
		}
		
		$this->data['username'] 	= $this->user->username;
		
		if ($this->form_validation->run('accountsettings') == TRUE)
		{
			$this->update_account_settings($this->user->_id);
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->data['error_message'] = $this->session->flashdata('error_message');
		
		$this->load_page('backend', 'user/accountsettings', TRUE, $this->data);
    }

    // --------------------------------------------------------------------
    
    /**
	 * Updates the user account settings
	 *
	 * @access	public
	 * @params 	array	user details
	 * @return	void
	 */
    public function update_account_settings($uid) 
    {
		$is_curr_password_valid = $this->userlibrary->check_current_password($this->user->_id , $this->input->post('passcurr'));
		
		if ($is_curr_password_valid) 
		{
			$this->load->library('password');
        
			$password = $this->input->post('password');
			
			// Update password in database, only if it is changed by the user
			if (!empty($password))
			{
				$salt = $this->password->makeSalt($this->user->username, $this->input->post('password'));
			
				$password = $this->password->createHash($this->input->post('password'), $salt, null);
				
				$update_array['password'] = array('password' => $password);
			}
			
			$update_array = array('first_name' => $this->input->post('first_name') , 'last_name' => $this->input->post('last_name') , 'email' => $this->input->post('email'));
			
			$user = $this->users->update($uid, $update_array);
			  
			if($user == true) 
			{   
				$this->session->set_flashdata('message', $this->lang->line('message_acct_settings_updated'));
			} 
			else 
			{
				$this->session->set_flashdata('error_message', $this->lang->line('message_acct_settings_update_fail'));
			}
		}
		else
		{
			$this->session->set_flashdata('error_message', $this->lang->line('message_current_pass_incorrect'));
		}
		
        redirect('accountsettings', 'refresh');
    }
}

/* End of file accountsettings.php */
/* Location: ./application/controllers/accountsettings.php */