<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Login Controller Class
 *
 * This class object enables the user login process.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class Login extends MY_Controller 
{
    /**
	 * Login Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in, if YES redirect to backend home screen
		// Dont check this for logout
		$function = $this->uri->segment(1);
		
		$user_id = $this->check_logged_in(TRUE);
		
		if (isset($user_id) AND $user_id > 0 AND $function !== 'logout')
		{
			redirect('edocs' , 'refresh');
		}
		
        $this->load->library('form_validation');
    }

    // --------------------------------------------------------------------

    public function index()
    {
        $this->load->helper('form');
        
        $this->lang->load('form_validation', 'english');
        
        $this->lang->load('form_label', 'english');
        
        $this->form_validation->set_message('required', $this->lang->line('error_login_required'));
		
		// Check if the login is happening in maintenance mode
		$this->load->model('site_settings');
		
		$settings = $this->site_settings->get_settings(array('name' => 'site_under_maintenance'));
		
		if ($settings['site_under_maintenance'] == 1)
		{
				$this->data['is_maintenance'] = 1;
		}
        
        if ($this->form_validation->run() == FALSE)
    	{
            $this->data['message'] = $this->session->flashdata('message');
            
            $this->data['registration_message'] = $this->session->flashdata('registration_message');
            
    		$this->load_page('website', 'user/login', FALSE, $this->data);
    	} 
        else 
        {
            $this->authenticate();    
        }
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Authenticates the user and redirects to Home dashboard
	 *
	 * @access	public
	 * @return	void
	 */  
    public function authenticate() 
    {
        $this->load->model('users');
        
        $this->lang->load('message', 'english');

        $user_name = $this->input->post('username');
        
        $password  = $this->input->post('password');
        
        // Check whether the user account exists
        $is_account_exists = $this->check_account_exists($user_name);
        
        if ($is_account_exists == false) 
        {
            $this->session->set_flashdata('message', $this->lang->line('message_account_not_exists'));
            
            redirect('login', 'refresh');
            
            exit;
        }
        
        // Check whether the user account status is active
        $account_status = $this->check_account_status($user_name);
        
        if ($account_status['status'] == false) 
        {
            $this->session->set_flashdata('message', $this->lang->line($account_status['message']));
            
            redirect('login', 'refresh');
            
            exit;
        }
        
        $this->load->library('password');
        
        $salt = $this->password->makeSalt($user_name, $password);
        
        $password = $this->password->createHash($password, $salt, null);
        
		$user = $this->users->get_single_record(array('username' => $user_name, 'password' => $password, 'account_status' => 1));
		
		// Record login information, even if it is failed
		$this->userlibrary->record_last_login($user);
		
        if(!empty($user)) 
        {
			// Login the user. Send the user object
			$this->userlibrary->login_user($user);
		
            Timezone::$timezone = 'UTC';
            
            $redirect_to = 'edocs';
        } 
        else 
        {
            $this->session->set_flashdata('message', $this->lang->line('message_invalid_login'));
            
            $redirect_to = 'login';    
        }
        
        
        redirect($redirect_to, 'refresh');
    }
    
    
    
    // --------------------------------------------------------------------
    
    /**
	 * Check the account status whether it is locked
	 *
	 * @access	public
	 * @params  string     username     
	 * @return	void
	 */
    public function check_account_status($user_name) 
    {
        $account_status = array();
    
        $user = $this->users->get_single_record(array('username' => $user_name));
        
        if ($user->account_status != 1) 
        {
            $account_status['status'] = false;
            
            switch ($user->account_status) {
                case 2:
                    $account_status['message'] = 'message_account_locked';        
                    break;
                case 3:
                    $account_status['message'] = 'message_account_pending';
                    break;
                case 4:
                    $account_status['message'] = 'message_account_suspended';
                    break;
                case 5:
                    $account_status['message'] = 'message_account_closed';
                    break;    
            }   
        }
        else 
        {
            $account_status['status'] = true;
        }
        
        return $account_status;
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Check whether the user account exists
	 *
	 * @access	public
	 * @params  string     username     
	 * @return	void
	 */
    public function check_account_exists($user_name) 
    {
        $user = $this->users->get_single_record(array('username' => $user_name));
        
        if (empty($user)) 
        {
            return false;   
        }
        
        return true;
    }
    
    // --------------------------------------------------------------------
  
    /**
	 * Logs out the user and redirect to home page
	 *
	 * @access	public
	 * @return	void
	 */  
  
    public function logout()
    {
		$this->userlibrary->logout_user();
		
        redirect('home', 'refresh');
    }
    
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */