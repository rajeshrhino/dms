<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Registration Controller Class
 *
 * This class object enables the user registration process.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class Register extends MY_Controller 
{
    /**
	 * Registration Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('form_validation');
    }

    // --------------------------------------------------------------------
    
    public function index()
    {
		// check if user registration is allowed
		$this->load->model('site_settings');
		
		// Get the site setting - 'User can register?'
		$settings = $this->site_settings->get_settings(array('name' => 'users_can_register'));
		
		// Dont display the registration form if 'User can register?' is set to FALSE 
		// Display a different view/template instead
		if ($settings['users_can_register'] == 0)
		{
			$this->load_page('website', 'user/registration_disabled', FALSE, $this->data);
		}
		else
		{
			// Get the site setting - 'Is the invite code mandatory?'
			$use_invite_code = $this->site_settings->get_settings(array('name' => 'invite_code_mandatory'));
			
			$this->data['invite_code_mandatory'] = $use_invite_code['invite_code_mandatory'];
			
			if ($this->data['invite_code_mandatory'] == 1)
			{
				$validation_array = 'registerindividualwithinvitationcode';
			}
			else
			{
				$validation_array = 'registerindividual';
			}
			
			$this->load->helper('form');
			
			$this->lang->load('form_validation', 'english');
			
			$this->lang->load('form_label', 'english');
			
			$this->form_validation->set_message('required', $this->lang->line('error_register_required'));
			
			$this->form_validation->set_message('min_length', $this->lang->line('min_length_required'));
			
			//$this->form_validation->set_message('matches', $this->lang->line('password_match_required'));
			
			$this->form_validation->set_message('alpha_dash', $this->lang->line('alpha_dash_required'));
			
			//$this->load->model('packages');
			
			//$data['packages'] = $this->packages->get_packages();
			
			$this->load->model('languages');
			
			$this->data['languages'] = $this->languages->get_languages();
			
			$this->data['timezones'] = $this->timezone->get_time_zones();
						
			if ($this->form_validation->run($validation_array) == TRUE)
			{
				$this->data['is_valid'] = TRUE;
				
				// Validate invitation code, if mandatory
				if ($this->data['invite_code_mandatory'] == 1)
				{
					$this->load->library('userlibrary');
					
					$this->userlibrary->validate_invite_code($this->input->post('invite_code') , $this->data);
				}	
				
				$this->load->model('users');
				
				$this->data = $this->check_availability($this->data);
				
				if ($this->data['is_available'] AND $this->data['is_valid']) 
				{
					$this->register();    
				}
			}
			
			$this->load_page('website', 'user/register', FALSE, $this->data);
		}
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Registers the user and redirects to Login form
	 *
	 * @access	public
	 * @return	void
	 */
    public function register() 
    {
        $this->load->model('users');
		
		$this->load->model('users_package');
        
		// Load User Library
		$this->load->library('userlibrary');
		
        $user_details = $this->userlibrary->get_posted_user_details();
        
        $this->load->library('password');
        
        $salt = $this->password->makeSalt($user_details['username'], $user_details['password']);
        
        $user_details['password'] = $this->password->createHash($user_details['password'], $salt, null);
        
		// Set the user as 'User'
		$user_details['role_id'] = 3;
		
        $user = $this->users->insert($user_details);
        
		$user_package_details['user_id'] = $user;
		$user_package_details['package_id'] = 1;
		$user_package_details['company_id'] = '';
		$user_package_details['is_free_account'] = 0;
		$user_package_details['number_of_users'] = 1;
		
		$this->users_package->insert($user_package_details);
		
        $this->lang->load('message', 'english');
          
        if($user) 
        {
            $user_details['checksum'] = $this->generate_check_sum($user);
                    
            // Send confirmation email to user
            $this->send_email($user_details, 'user_registration/individual', 'Confirm your registration');
        
            $this->session->set_flashdata('registration_message', $this->lang->line('message_registration_success'));
        } 
        else 
        {
            $this->session->set_flashdata('registration_message', $this->lang->line('message_registration_failed'));
        }
        
        redirect('login', 'refresh');
    }
    
    // --------------------------------------------------------------------

	/**
	 * Checks whether username and email-address already exists in the database
	 *
	 * @access	public
	 * @return	array	
	 */
    public function check_availability($data = NULL) 
    {
        $data['is_available'] = TRUE; 
        
        $this->lang->load('form_validation');
        
        if(!$this->users->check_availability('username', $this->input->post('username')))
        {
            $data['username_error'] = $this->lang->line('error_username_exists');
            
            $data['is_available'] = FALSE; 
        }  
        
        if(!$this->users->check_availability('email', $this->input->post('email')))
        {
            $data['email_error'] = $this->lang->line('error_email_exists');
            
            $data['is_available'] = FALSE;
        }
        
        return $data;
    }
    
    // --------------------------------------------------------------------

	/**
	 * Checks whether username exists in the database
	 *
	 * @access	public
	 * @return	array	
	 */
    public function check() 
    {
        $this->load->model('users');
        
        echo $this->users->check_availability('username', $this->input->post('username'));
    }
}

/* End of file register.php */
/* Location: ./application/controllers/register.php */