<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Reset Password Controller Class
 *
 * This class object enables user to update password.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
session_start(); //we need to call PHP's session object to access it through CI

class Resetpassword extends MY_Controller 
{
    /**
	 * Accountsettings Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();
		
		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
		
        $this->load->library('form_validation');
    }

    // --------------------------------------------------------------------
    
    public function index()
    {
	
		$this->load->helper('form');
		
		$this->data['user_id'] = $this->user->_id;
		
		$this->data['checksum'] = $this->uri->segment(2);
		
		$this->form_validation->set_message('required', $this->lang->line('error_register_required'));
		
		$this->form_validation->set_message('min_length', $this->lang->line('min_length_required'));
			
		$this->form_validation->set_message('matches', $this->lang->line('password_match_required'));

		if ($this->form_validation->run('resetpassword') == TRUE)
		{
			// Update the password only if the checksum is validated
			$status = $this->validate_check_sum($this->user->_id, $this->input->post('checksum'));
			
			if ($status)
			{
				$this->update_password($this->user->_id);
			}
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load_page('backend', 'user/resetpassword', TRUE, $this->data);
    }
	
    
    // --------------------------------------------------------------------
    
    /**
	 * Updates the user account settings
	 *
	 * @access	public
	 * @params 	array	user details
	 * @return	void
	 */
    public function update_password($uid)
    {
        //$this->load->model('users');
		
		//$user_details = $this->users->get_single_record(array('_id' => $this->user->_id));
        
		$this->load->library('password');
        
        $salt = $this->password->makeSalt($this->user->username, $this->input->post('password'));
        
        $password = $this->password->createHash($this->input->post('password'), $salt, null);
		
        $user = $this->users->update($uid, array('password' => $password));
          
        if($user == true) 
        {   
            $this->session->set_flashdata('message', '<div class="success-message">Password updated successful.</div>');
        } 
        else 
        {
            $this->session->set_flashdata('message', '<div class="error-message">For some reason, your attempt to update password has failed. Please try again later or contact the administrator.</div>');
        }
        
        redirect('edocs', 'refresh');
    }
}

/* End of file account_settings.php */
/* Location: ./application/controllers/account_settings.php */