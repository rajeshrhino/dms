<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Corporate Admin Panel - User list Controller Class
 *
 * This class object enables user to update password.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 

class Corporateuserslist extends MY_Controller
{	
    /**
	 * Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();
		
		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
    }
    
	// --------------------------------------------------------------------
	/**
	 * Function to display users list
	 */
	public function index()
	{
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->data['error_message'] = $this->session->flashdata('error_message');
		
		$this->load->model('users');
		
		$this->load->model('users_package');
		
		$this->load->model('companies');
		
		$company_id = 1;
		
		$company_details = $this->companies->get_single_record(array('_id' => $company_id));
		
		$this->data['company_id'] = $company_id;
		
		$this->data['company_name'] 	= $company_details->company_name;
		
		if ($company_details->street_address)
		{
			$address[] = $company_details->street_address;
		}
		if ($company_details->city)
		{
			$address[] = $company_details->city;
		}
		if ($company_details->city)
		{
			$address[] = $company_details->postcode;
		}
		
		$this->data['full_address'] = @implode('<br />' , $address);
		
		$users_packages = (array) $this->users_package->get_records(array('company_id' => $company_id));
		
		//$users = (array) $this->users->get_records(array());
		
		$this->load->library('table');
		
		$tmpl = array (
			'table_open'          => '<table id="documents_list_table" class="table table-striped table-bordered bootstrap-datatable datatable">',

			'heading_row_start'   => '<tr>',
			'heading_row_end'     => '</tr>',
			'heading_cell_start'  => '<th>',
			'heading_cell_end'    => '</th>',

			'row_start'           => '<tr>',
			'row_end'             => '</tr>',
			'cell_start'          => '<td>',
			'cell_end'            => '</td>',

			'row_alt_start'       => '<tr>',
			'row_alt_end'         => '</tr>',
			'cell_alt_start'      => '<td>',
			'cell_alt_end'        => '</td>',

			'table_close'         => '</table>'
		
		);
		$this->table->set_template($tmpl);
		  
		$this->table->set_heading(lang('label_first_name'), lang('label_last_name'), lang('label_email') , lang('label_registered') , '');
  
		foreach($users_packages as $users_package_record)
		{
			//if ($user_record->_id != 1)
			//{ 
			
			if ($user_record = $this->users->get_single_record(array('_id' => $users_package_record->user_id)))
			{
				$this->table->add_row($user_record->first_name , 
										  $user_record->last_name , 
										  $user_record->email , 
										  date( $this->userlibrary->get_preferred_date_format($this->user , 'short') , $user_record->created_date ) ,
										  anchor('admin/userslist/deleteuser/'.$user_record->_id.'/'.$company_id , '<span class="icon icon-color icon-close" title="Delete"></span>') 
										  );
			}
			//}
		}
		  
		// Assign values to the view/template
		$this->data['table'] = $this->table->generate(); 

		$this->load_page('backend' , 'corporate/corporateuserslist', TRUE , $this->data);
	}
}

/* End of file userslist.php */
/* Location: ./application/controllers/corporate/corporateuserslist.php */