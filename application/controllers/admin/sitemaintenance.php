<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Site maintenance Controller Class
 *
 * This class object enables admin dashboard.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
class Sitemaintenance extends MY_Controller 
{
    /**
	 * Site Maintenance Constructor
	 */
    public function __construct()
    {
        parent::__construct();
		
		// Check whether the user is logged in or not
		$this->check_logged_in();
		
		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
    }

    // --------------------------------------------------------------------

    public function index()
    {
		// Load the neccessary modals
		$this->load->model('site_settings');
		
		if ($this->input->post() != FALSE)
		{
			$post_details = $this->input->post();
			
			// Set the checkboxes to 0, if they are not part of POST form data, as 'unticked' checkboxes are not passed as POST form data
			if (!isset($post_details['site_under_maintenance']))
			{
				$post_details['site_under_maintenance'] = 0;
			}
			
			$result = $this->site_settings->update_entry('site_under_maintenance' , array('value' => $post_details['site_under_maintenance']));
			
			$this->session->set_flashdata('message', $this->lang->line('settings_saved_confirmation'));
			
			redirect('admin/sitemaintenance' , 'refresh');
		}
		
		// Get the site settings
		$settings = $this->site_settings->get_settings(array('name' => 'site_under_maintenance'));
		
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->data['site_under_maintenance'] = $settings['site_under_maintenance'];
		
		$this->load_page('backend', 'admin/form/site_maintenance', TRUE, $this->data);
    }
}

/* End of file admindashboard.php */
/* Location: ./application/controllers/admin/admindashboard.php */