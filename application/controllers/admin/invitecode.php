<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Admin Panel - Invitation Code list Controller Class
 *
 * This class object enables admin to view list of invitation codes.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 

class Invitecode extends MY_Controller
{	
    /**
	 * Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();
		
		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
    }
	
	// --------------------------------------------------------------------
	
	/**
	 * Function to display the companies list page
	 */
	public function index()
	{
		$this->data['message'] = $this->session->flashdata('message');
	
		$this->load->model('invitecodes');
		
		$invitecodes = (array) $this->invitecodes->get_records();
		
		$this->load->library('table');
		
		$tmpl = array (
			'table_open'          => '<table id="documents_list_table" class="table table-striped table-bordered bootstrap-datatable datatable">',

			'heading_row_start'   => '<tr>',
			'heading_row_end'     => '</tr>',
			'heading_cell_start'  => '<th>',
			'heading_cell_end'    => '</th>',

			'row_start'           => '<tr>',
			'row_end'             => '</tr>',
			'cell_start'          => '<td>',
			'cell_end'            => '</td>',

			'row_alt_start'       => '<tr>',
			'row_alt_end'         => '</tr>',
			'cell_alt_start'      => '<td>',
			'cell_alt_end'        => '</td>',

			'table_close'         => '</table>'
		
		);
		$this->table->set_template($tmpl);
		  
		$this->table->set_heading(lang('label_invite_code_name'), lang('label_date_created') , lang('label_is_active') , lang('label_actions'));
  
		foreach($invitecodes as $invitecode)
		{

			$delete_link = anchor('admin/invitecode/delete/'.$invitecode->_id , '<span class="icon icon-color icon-close" title="Delete"></span>' , array('onClick' => "return confirm('".lang('message_delete_invitecode_confirm')."')"));
		
			$is_active = 'No';
			if ($invitecode->is_active == 1)
			{
				$is_active = 'Yes';
			}
		
			$this->table->add_row($invitecode->invite_code , 
								  date( $this->userlibrary->get_preferred_date_format($this->user , 'short') , $invitecode->created_date ) ,	
								  $is_active ,
								  anchor('admin/invitecode/edit/'.$invitecode->_id , '<span class="icon icon-color icon-edit" title="Edit '.$invitecode->invite_code.'"></span>').'&nbsp;'.
								  $delete_link
								  );
		}
		  
		// Assign values to the view/template
		$this->data['table'] = $this->table->generate(); 

		$this->load_page('backend' , 'admin/invitecodeslist', TRUE , $this->data);
	}
	
	// --------------------------------------------------------------------
	/**
	 * Function to build form for add/edit company record
	 */
	public function build_form()
	{
		$this->load->library('form_validation');
		
		$operation = $this->input->post('operation');
		
		if (empty($operation))
		{
			$operation = $this->uri->segment(3);
		}
		
		if ($operation == 'edit')
		{
			$invite_code_id = (int) $this->uri->segment(4);
			
			if (empty($invite_code_id))
			{
				$invite_code_id = (int) $this->input->post('invite_code_id');
			}
			
			// Get company details
			$this->load->model('invitecodes');
			$invite_code_details = $this->invitecodes->get_single_record(array('_id' => $invite_code_id));
			
			$this->data['invite_code_id'] = $invite_code_id;
			
			$this->data['invite_code'] = $invite_code_details->invite_code;
			$this->data['is_active'] = $invite_code_details->is_active;
		}
		else
		{
			$operation = 'add';
			$this->data['invite_code'] = '';
			$this->data['is_active'] = '';
		}
		
		$this->data['operation'] = $operation;
		
		$this->form_validation->set_message('required', $this->lang->line('error_login_required'));
		
		if ($this->form_validation->run('invitecode') == FALSE)
		{
			if(validation_errors() != false) 
			{ 
				$this->data['error_message'] = $this->lang->line('message_enter_mandatory_fields');
				
				$this->data['is_error'] = 1;
			}
		}
		else
		{
			if ($this->check_invite_code_exist($this->input->post('operation') , 'return') == TRUE)
			{
				$this->data['error_message'] = $this->lang->line('error_invite_code_exists');
				
				$this->data['is_error'] = 1;
			}
			else
			{
				$this->save_invite_code();
			}
		}
		
		$this->load_page('backend' , 'admin/form/invite_codes', TRUE , $this->data);
	}
	
	// --------------------------------------------------------------------
	/**
	 * Function to save the company record to database
	 */
	public function save_invite_code()
	{
		$this->load->model('invitecodes');
		
		$operation = $this->input->post('operation');

		$post_details = $this->input->post();
		
		if (!isset($post_details['is_active']))
		{
			$post_details['is_active'] = 0;
		}
		
		$invite_code_details = array( 'invite_code'    	=> $post_details['invite_code'] , 
									  'is_active'  		=> $post_details['is_active'] ,
									);
		
		if ($operation == 'edit')
		{
			$invite_code_id = $this->invitecodes->update((int) $this->input->post('invite_code_id') , $invite_code_details);									
		}
		
		if ($operation == 'add')
		{
			$invite_code_details['created_date'] = time();
			$invite_code_id = $this->invitecodes->insert($invite_code_details);
		}
		
		$this->session->set_flashdata('message', $this->lang->line('message_invite_code_saved'));
		
		redirect('admin/invitecode', 'refresh');
	}
	
	// --------------------------------------------------------------------
	/**
	 * Function to check if any other company exist in same name
	 */
	public function check_invite_code_exist($action = 'add' , $return_type = 'echo')
	{
		$is_exists = FALSE;
		
		$invite_code		= (string) $this->input->post('invite_code');
		
		if ($action == 'edit')
		{
			$invite_code_id			= (int) $this->input->post('invite_code_id');
		}
		
		$this->load->model('invitecodes');
		
		$invitecodes = (array) $this->invitecodes->get_records(array('invite_code' => $invite_code));
		
		if ($action == 'add')
		{
			if (count($invitecodes) > 0)
			{
				$is_exists = TRUE;
			}
		}
		
		if ($action == 'edit')
		{
			foreach ($invitecodes as $invitecode)
			{
				if ($invitecode->_id != $invite_code_id AND $invitecode->invite_code == $invite_code)
				{
					$is_exists = TRUE;
				}
			}
		}
		
		if ($return_type == 'return')
		{
			return $is_exists;
		}
		
		echo $is_exists;
	}
	
	// --------------------------------------------------------------------
	/**
	 * Function to delete a company record from database
	 */
	public function delete()
	{
		$invite_code_id = (int) $this->uri->segment(4);
		
		if (!empty($invite_code_id))
		{
			$this->load->model('invitecodes');
		
			$this->invitecodes->delete($invite_code_id);
		}
		
		redirect('admin/invitecode', 'refresh');
	}
}

/* End of file companieslist.php */
/* Location: ./application/controllers/admin/companieslist.php */