<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Admin Panel - Message templates Controller Class
 *
 * This class object enables admin to view all message/email templates
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 

class Messagetemplates extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();
		
		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
    }
    
	public function index()
	{
		
		$this->load->model('messagetemplate');
		
		$templates = (array) $this->messagetemplate->get_records();
		
		$this->load->library('table');
		
		$tmpl = array (
			'table_open'          => '<table id="documents_list_table" class="table table-striped table-bordered bootstrap-datatable datatable">',

			'heading_row_start'   => '<tr>',
			'heading_row_end'     => '</tr>',
			'heading_cell_start'  => '<th>',
			'heading_cell_end'    => '</th>',

			'row_start'           => '<tr>',
			'row_end'             => '</tr>',
			'cell_start'          => '<td>',
			'cell_end'            => '</td>',

			'row_alt_start'       => '<tr>',
			'row_alt_end'         => '</tr>',
			'cell_alt_start'      => '<td>',
			'cell_alt_end'        => '</td>',

			'table_close'         => '</table>'
		
		);
		$this->table->set_template($tmpl);
		  
		$this->table->set_heading(lang('label_message_title'), lang('label_message_subject'));
  
		foreach($templates as $template)
		{
			$this->table->add_row($user_record->message_title , $user_record->message_subject);
		}
		  
		// Assign values to the view/template
		$this->data['table'] = $this->table->generate(); 

		$this->load_page('backend' , 'admin/messagetemplates', TRUE , $this->data);
	}
	
	public function build_form()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_message('required', $this->lang->line('error_login_required'));
		
		if ($this->form_validation->run('messagetemplate') == FALSE)
		{
			if(validation_errors() != false) 
			{ 
				$this->data['error_message'] = $this->lang->line('message_enter_mandatory_fields');
				
				$this->data['is_error'] = 1;
			}
		}
		else
		{
			$this->save_template();
		}
		
		$this->load_page('backend' , 'admin/form/messagetemplates', TRUE , $this->data);
	}
	
	public function save_template()
	{
		$this->load_page('backend' , 'admin/form/messagetemplates', TRUE , $this->data);
	}
}

/* End of file messagetemplates.php */
/* Location: ./application/controllers/admin/messagetemplates.php */