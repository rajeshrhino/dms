<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Site settings Controller Class
 *
 * This class object enables admin dashboard.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
class Sitesettings extends MY_Controller 
{
    /**
	 * Admin Dashboard Constructor
	 */
    public function __construct()
    {
        parent::__construct();
		
		// Check whether the user is logged in or not
		$this->check_logged_in();
		
		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
    }

    // --------------------------------------------------------------------

    public function index()
    {
		$this->load->library('form_validation');
		
		// Load the neccessary modals
		$this->load->model('site_settings');
		
		if ($this->form_validation->run('sitesettings') == FALSE)
		{
			if(validation_errors() != false) 
			{ 
				$this->data['error_message'] = $this->lang->line('message_enter_mandatory_fields');
				
				$this->data['is_error'] = 1;
			}
		}
		else
		{
			$post_details = $this->input->post();
			
			// Set the checkboxes to 0, if they are not part of POST form data, as 'unticked' checkboxes are not passed as POST form data
			if (!isset($post_details['users_can_register']))
			{
				$post_details['users_can_register'] = 0;
			}
			if (!isset($post_details['invite_code_mandatory']))
			{
				$post_details['invite_code_mandatory'] = 0;
			}
			if (!isset($post_details['memcache_enabled']))
			{
				$post_details['memcache_enabled'] = 0;
			}
			
			foreach ($post_details as $entry => $value)
			{
				if ($entry == 'submit') 
				{
					continue;
				}
				
				// Update the settings entry
				$result = $this->site_settings->update_entry($entry , array('value' => $value));
			}
			
			$this->data['message'] = $this->lang->line('settings_saved_confirmation');
		}
		
		// Get the site settings
		$settings = $this->site_settings->get_settings();
		
		foreach ($settings as $entry => $value)
		{
			$this->data[$entry] = $value;
		}
		
		$this->data['view_name'] = 'site_settings';
		
		$this->load_page('backend', 'admin/form/site_settings', TRUE, $this->data);
    }
}

/* End of file admindashboard.php */
/* Location: ./application/controllers/admin/admindashboard.php */