<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Admin Panel - Companies list Controller Class
 *
 * This class object enables admin to view list of companies.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 

class Companieslist extends MY_Controller
{	
    /**
	 * Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();
		
		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
    }
	
	// --------------------------------------------------------------------
	
	/**
	 * Function to display the companies list page
	 */
	public function index()
	{
		$this->data['message'] = $this->session->flashdata('message');
	
		$this->load->model('companies');
		
		$companies = (array) $this->companies->get_records();
		
		$this->load->library('table');
		
		$tmpl = array (
			'table_open'          => '<table id="documents_list_table" class="table table-striped table-bordered bootstrap-datatable datatable">',

			'heading_row_start'   => '<tr>',
			'heading_row_end'     => '</tr>',
			'heading_cell_start'  => '<th>',
			'heading_cell_end'    => '</th>',

			'row_start'           => '<tr>',
			'row_end'             => '</tr>',
			'cell_start'          => '<td>',
			'cell_end'            => '</td>',

			'row_alt_start'       => '<tr>',
			'row_alt_end'         => '</tr>',
			'cell_alt_start'      => '<td>',
			'cell_alt_end'        => '</td>',

			'table_close'         => '</table>'
		
		);
		$this->table->set_template($tmpl);
		  
		$this->table->set_heading(lang('label_company_name'), lang('label_number_of_user') , lang('label_list_of_user') , lang('label_actions'));
  
		foreach($companies as $company)
		{
			// Load User Library
			$this->load->library('companylibrary');
			
			$users_count = $this->companylibrary->get_users_in_company($company->_id);
			
			if (count($users_count) > 0)
			{
				$delete_link = '&nbsp;';
			}
			else
			{
				$delete_link = anchor('admin/companies/delete/'.$company->_id , '<span class="icon icon-color icon-close" title="Delete"></span>' , array('onClick' => "return confirm('".lang('message_delete_company_confirm')."')"));
			}
		
			$this->table->add_row($company->company_name , 
								  //$company->city , 
								  //$company->postcode ,
								  $users_count,
								  anchor('admin/companyuserslist/'.$company->_id , lang('label_list')) ,
								  //anchor('admin/userslist/'.$company->_id.'/add/' , lang('label_user_add')).'&nbsp;|&nbsp;'.
								  anchor('admin/companies/edit/'.$company->_id , '<span class="icon icon-color icon-edit" title="Edit '.$company->company_name.'"></span>').'&nbsp;'.
								  $delete_link
								  );
		}
		  
		// Assign values to the view/template
		$this->data['table'] = $this->table->generate(); 

		$this->load_page('backend' , 'admin/companieslist', TRUE , $this->data);
	}
	
	// --------------------------------------------------------------------
	/**
	 * Function to build form for add/edit company record
	 */
	public function build_form()
	{
		$this->load->library('form_validation');
		
		$operation = $this->input->post('operation');
		
		if (empty($operation))
		{
			$operation = $this->uri->segment(3);
		}
		
		if ($operation == 'edit')
		{
			$company_id = (int) $this->uri->segment(4);
			
			if (empty($company_id))
			{
				$company_id = (int) $this->input->post('company_id');
			}
			
			// Get company details
			$this->load->model('companies');
			$company_details = $this->companies->get_single_record(array('_id' => $company_id));
			
			$this->data['company_id'] = $company_id;
			
			$this->data['company_name'] = $company_details->company_name;
			$this->data['company_street_address'] = $company_details->street_address;
			$this->data['company_city'] = $company_details->city;
			$this->data['company_postcode'] = $company_details->postcode;
		}
		else
		{
			$operation = 'add';
			$this->data['company_name'] = '';
			$this->data['company_street_address'] = '';
			$this->data['company_city'] = '';
			$this->data['company_postcode'] = '';
		}
		
		$this->data['operation'] = $operation;
		
		$this->form_validation->set_message('required', $this->lang->line('error_login_required'));
		
		if ($this->form_validation->run('company') == FALSE)
		{
			if(validation_errors() != false) 
			{ 
				$this->data['error_message'] = $this->lang->line('message_enter_mandatory_fields');
				
				$this->data['is_error'] = 1;
			}
		}
		else
		{
			if ($this->check_company_exist($this->input->post('operation') , 'return') == TRUE)
			{
				$this->data['error_message'] = $this->lang->line('company_name_exists');
				
				$this->data['is_error'] = 1;
			}
			else
			{
				$this->save_company();
			}
		}
		
		$this->load_page('backend' , 'admin/form/company', TRUE , $this->data);
	}
	
	// --------------------------------------------------------------------
	/**
	 * Function to save the company record to database
	 */
	public function save_company()
	{
		$this->load->model('companies');
		
		$operation = $this->input->post('operation');
		
		$company_details = array( 'company_name'    => $this->input->post('company_name') , 
								  'street_address'  => $this->input->post('company_street_address') ,
								  'city'            => $this->input->post('company_city') ,
								  'postcode'        => $this->input->post('company_postcode') ,
								);
		
		if ($operation == 'edit')
		{
			$company_id = $this->companies->update((int) $this->input->post('company_id') , $company_details);									
		}
		
		if ($operation == 'add')
		{
			$company_id = $this->companies->insert($company_details);
		}
		
		$this->session->set_flashdata('message', $this->lang->line('message_company_saved'));
		
		redirect('admin/companieslist', 'refresh');
	}
	
	// --------------------------------------------------------------------
	/**
	 * Function to check if any other company exist in same name
	 */
	public function check_company_exist($action = 'add' , $return_type = 'echo')
	{
		$is_exists = FALSE;
		
		$company_name		= (string) $this->input->post('company_name');
		
		if ($action == 'edit')
		{
			$company_id			= (int) $this->input->post('company_id');
		}
		
		$this->load->model('companies');
		
		$companies = (array) $this->companies->get_records(array('company_name' => $company_name));
		
		if ($action == 'add')
		{
			if (count($companies) > 0)
			{
				$is_exists = TRUE;
			}
		}
		
		if ($action == 'edit')
		{
			foreach ($companies as $company)
			{
				if ($company->_id != $company_id AND $company->company_name == $company_name)
				{
					$is_exists = TRUE;
				}
			}
		}
		
		if ($return_type == 'return')
		{
			return $is_exists;
		}
		
		echo $is_exists;
	}
	
	// --------------------------------------------------------------------
	/**
	 * Function to delete a company record from database
	 */
	public function delete()
	{
		$company_id = (int) $this->uri->segment(4);
		
		if (!empty($company_id))
		{
			$this->load->model('companies');
		
			$this->companies->delete($company_id);
		}
		
		redirect('admin/companieslist', 'refresh');
	}
}

/* End of file companieslist.php */
/* Location: ./application/controllers/admin/companieslist.php */