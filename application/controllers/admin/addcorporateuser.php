<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Admin Panel - User list Controller Class
 *
 * This class object enables user to update password.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 

class Addcorporateuser extends MY_Controller
{	
    /**
	 * Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
		// Check whether the user is logged in or not
		$this->check_logged_in();
		
		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
    }
    
	// --------------------------------------------------------------------
	/**
	 * Function to build form for add/edit user record
	 */
	public function index()
	{
	
		$company_id = (int) $this->uri->segment(4);
		
		if (empty($company_id))
		{
			$company_id = (int) $this->input->post('company_id');
		}
		
		$this->data['company_id'] = $company_id;
		
		$this->load->model('companies');
		
		$company_details = $this->companies->get_single_record(array('_id' => $company_id));
		
		$this->data['company_name'] = $company_details->company_name;
		
		$this->data['is_corporate_user'] = '1';
	
		$this->load->library('form_validation');
		
		$this->form_validation->set_message('required', $this->lang->line('error_register_required'));
        
        $this->form_validation->set_message('min_length', $this->lang->line('min_length_required'));
        
        $this->form_validation->set_message('matches', $this->lang->line('password_match_required'));
        
        $this->form_validation->set_message('alpha_dash', $this->lang->line('alpha_dash_required'));
		
		$this->load->model('languages');
		
        $this->data['languages'] = $this->languages->get_languages();
        
        $this->data['timezones'] = $this->timezone->get_time_zones();
		
		$this->load->model('packages');
        
        $this->data['packages'] = array('' => '-'.lang('label_select').'-') + $this->packages->get_packages();
		
		$operation = $this->input->post('operation');
		
		$this->load->model('users');
		
		if (empty($operation))
		{
			$operation = $this->uri->segment(3);
		}
		
		if ($operation == 'edit')
		{
			$user_id = (int) $this->uri->segment(4);
			
			if (empty($user_id))
			{
				$user_id = (int) $this->input->post('user_id');
			}
			
			// Get company details
			
			$user_details = $this->users->get_single_record(array('_id' => $user_id));
			
			$this->data['user_id'] = $user_id;
			
			$this->data['first_name'] 	= $user_details->first_name;
			$this->data['last_name'] 	= $user_details->last_name;
			$this->data['email'] 		= $user_details->email;
			$this->data['user_name'] 	= $user_details->username;
			
		}
		else
		{
			$operation = 'add';
			$this->data['first_name'] 				= '';
			$this->data['last_name'] 				= '';
			$this->data['email'] 					= '';
			$this->data['user_name'] 				= '';
		}
		
		$this->data['operation'] = $operation;
		
		$this->form_validation->set_message('required', $this->lang->line('error_login_required'));
		
		if ($this->form_validation->run('registerindividual_backends') == FALSE)
		{
			if(validation_errors() != false) 
			{ 
				$this->data['error_message'] = $this->lang->line('message_enter_mandatory_fields');
				
				$this->data['is_error'] = 1;
				
			}
		}
		else
		{
			$is_error = 0;
		
			if(!$this->users->check_availability('username', $this->input->post('username')))
			{
				$this->data['username_error'] = $this->lang->line('error_username_exists');
				
				$is_error = 1;
			}
		
			if(!$this->users->check_availability('email', $this->input->post('email')))
			{
				$this->data['email_error'] = $this->lang->line('error_email_exists_without_link');
				
				$is_error = 1;
			}
		
			/*if ($this->check_company_exist($this->input->post('operation') , 'return') == TRUE)
			{
				$this->data['error_message'] = $this->lang->line('company_name_exists');
				
				$this->data['is_error'] = 1;
			}
			else
			{
				$this->save_company();
			}*/
			if ($is_error != 1)
			{
				$this->save_user();
			}
			else
			{
				$this->data['is_error'] = 1;
			}
		}
		
		$this->load_page('backend' , 'admin/form/user', TRUE , $this->data);
	}
	
	// --------------------------------------------------------------------

	/**
	 * Checks whether username and email-address already exists in the database
	 *
	 * @access	public
	 * @return	array	
	 */
    public function check_availability($data = NULL) 
    {
		
        $data['is_available'] = TRUE; 
        
        $this->lang->load('form_validation');
        
        if(!$this->users->check_availability('username', $this->input->post('username')))
        {
            $data['username_error'] = $this->lang->line('error_username_exists');
            
            $data['is_available'] = FALSE; 
        }  
        
        if(!$this->users->check_availability('email', $this->input->post('email')))
        {
            $data['email_error'] = $this->lang->line('error_email_exists');
            
            $data['is_available'] = FALSE;
        }
        
        return $data;
    }
	
	// --------------------------------------------------------------------
	/**
	 * Function to show delete user confirmaton form/page
	 */
	public function build_delete_user_form()
	{
		$user_id = (int) $this->uri->segment(4);
		
		// You cant delete user id - 1
		if ($user_id == 1)
		{
			$this->session->set_flashdata('error_message', $this->lang->line('cannot_delete_main_admin'));
			
			redirect('admin/userslist', 'refresh');
		}
		
		$this->load->model('users');
		
		$user_details = $this->users->get_single_record(array('_id' => $user_id));
		
		$this->data['delete_username'] = $user_details->username;
		
		$this->data['delete_user_id'] = $user_id;
		
		$this->load_page('backend' , 'admin/form/userdelete', TRUE , $this->data);
	}
	
	// --------------------------------------------------------------------
	/**
	 * Function to save the user record to database
	 */
	public function save_user()
	{
		$this->load->model('users');
		
		$this->load->model('users_package');
		
        $user_details = $this->userlibrary->get_posted_user_details();
		
		$user_details['role_id'] = 3;
		
		$this->load->library('password');
        
        $salt = $this->password->makeSalt($user_details['username'],$user_details['password']);
        
        $user_details['password'] = $this->password->createHash($user_details['password'],$salt,null);                 
        
        $user = $this->users->insert($user_details);
        
		$user_package_details['user_id'] = $user;
		$user_package_details['package_id'] = 1;
		$user_package_details['company_id'] = '';
		$user_package_details['is_free_account'] = 0;
		$user_package_details['number_of_users'] = 1;
		
		$this->users_package->insert($user_package_details);
		
		$user_details['checksum'] = $this->generate_check_sum($user);
		
		$this->send_email($user_details, 'user_registration/individual_backend', 'An administrator created an account for you at {_int} DMS');
		
		redirect('admin/userslist', 'refresh');
	}
	
	// --------------------------------------------------------------------
	/**
	 * Function to check if any other user exist in same name
	 */
	public function check_company_exist($action = 'add' , $return_type = 'echo')
	{
		$is_exists = FALSE;
		
		$company_name		= (string) $this->input->post('company_name');
		
		if ($action == 'edit')
		{
			$company_id			= (int) $this->input->post('company_id');
		}
		
		$this->load->model('companies');
		
		$companies = (array) $this->companies->get_records(array('company_name' => $company_name));
		
		if ($action == 'add')
		{
			if (count($companies) > 0)
			{
				$is_exists = TRUE;
			}
		}
		
		if ($action == 'edit')
		{
			foreach ($companies as $company)
			{
				if ($company->_id != $company_id AND $company->company_name == $company_name)
				{
					$is_exists = TRUE;
				}
			}
		}
		
		if ($return_type == 'return')
		{
			return $is_exists;
		}
		
		echo $is_exists;
	}
	
	// --------------------------------------------------------------------
	/**
	 * Function to delete a user record from database
	 */
	public function delete()
	{
		$company_id = (int) $this->uri->segment(4);
		
		if (!empty($company_id))
		{
			$this->load->model('companies');
		
			$this->companies->delete($company_id);
		}
		
		redirect('admin/companieslist', 'refresh');
	}
}

/* End of file userslist.php */
/* Location: ./application/controllers/admin/userslist.php */