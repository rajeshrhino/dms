<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Dashboard Controller Class
 *
 * This class object enables admin dashboard.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
class Admindashboard extends MY_Controller 
{
    /**
	 * Admin Dashboard Constructor
	 */
    public function __construct()
    {
        parent::__construct();
		
		// Check whether the user is logged in or not
		$this->check_logged_in();
		
		// Check permission for the current controller
		$this->check_permission(get_class($this));
		
		// Load basic user information to $data array
		$this->userlibrary->load_basic_user_info($this->user , $this->data);
    }

    // --------------------------------------------------------------------

    public function index()
    {
		// Load the neccessary modals
		$this->load->model('users_access');
		$this->load->model('users');
		
		// Get the user last access details
		$user_access = $this->users_access->get_single_record(array('user_id' => $this->user->_id));
		
		$this->session->set_userdata('USER',$this->user->username);
		$this->session->set_userdata('NAME',$this->data['name']);
		
		// Assign values to the view/template
		$this->data['access'] = date('dS M Y H:i' , $user_access->last_access);
		$this->data['ip_address'] = $user_access->ip_address;
		$this->data['users_count'] = $this->users->count_users();
		
		$this->load_page('backend', 'admin/admindashboard', TRUE, $this->data);
    }
}

/* End of file admindashboard.php */
/* Location: ./application/controllers/admin/admindashboard.php */