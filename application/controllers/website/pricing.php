<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Pricing Controller Class
 *
 * This class object enables the pricing view.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class Pricing extends MY_Controller 
{
    /**
	 * Pricing Constructor
	 */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('form_validation');
    }

    // --------------------------------------------------------------------
    
    public function index()
    {
        $this->load->model('packages');
        
        $this->data['packages'] = $this->packages->get_packages();
        
        $this->load_page('website', 'website/pricing', FALSE , $this->data);
    }
}

/* End of file register.php */
/* Location: ./application/controllers/pricing.php */