<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Contact Controller Class
 *
 * This class object enables the pricing view.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class Contact extends MY_Controller 
{
    /**
	 * Contact Constructor
	 *
	 * The constructor loads Form Validation Library automatically
	 * whenever the class is instantiated.
	 */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('form_validation');
    }

    // --------------------------------------------------------------------
    
    /**
	 * Contact Us form
	 */
    public function index()
    {
        $this->load->helper('form');
        
		$this->data['message'] = $this->session->flashdata('message');
        
        $this->load_page('website', 'website/contact' , FALSE , $this->data);
    }
    
    // --------------------------------------------------------------------
   
    /**
	 * Function to send the message submitted from contact us form
	 */
    public function send_message()
    {
        $this->load->helper('form');

        if ($_POST)
    	{
            $user_details['first_name'] = $this->input->post('name');
            
            $user_details['last_name'] = '';
            
            $user_details['sender_email'] = $this->input->post('email');
            
            $user_details['comments'] = $this->input->post('comments');
            
			//$this->config->load('email_settings');
            $this->load->library('email');
		
			// Get the site setting - for email address to send the query to
			$this->load->model('site_settings');
			$settings = $this->site_settings->get_settings(array('name' => 'contact_us_receiving_email_address'));
		
            $user_details['email'] = $settings['contact_us_receiving_email_address'];
            
            $this->send_email($user_details, 'contactus', 'Contact Us form submission from {_int} DMS website');
            
            $this->session->set_flashdata('message', $this->lang->line('message_contact_us_form_thank_you'));
            
            redirect('login', 'contact');
            
            exit;
        }   
    }
}

/* End of file register.php */
/* Location: ./application/controllers/pricing.php */