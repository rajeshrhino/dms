<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Terms Controller Class
 *
 * This class object enables the pricing view.
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

class Terms extends MY_Controller 
{
    /**
	 * Terms Constructor
	 */
    public function __construct()
    {
        parent::__construct();
    }

    // --------------------------------------------------------------------
    
    public function index()
    {
        $this->load_page('website', 'website/terms' , FALSE , $this->data);
    }
}

/* End of file terms.php */
/* Location: ./application/controllers/terms.php */