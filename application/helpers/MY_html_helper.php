<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

if ( ! function_exists('load_cssjs'))
{
	function load_cssjs($name,$jsorcss)
	{
		$CI =& get_instance();
		$CI->config->load('cssjs');
		$cssjsArr = $CI->config->item('dms_header');
		$allArr = $CI->config->item('all','map');
		$pageArr = array();
		if(!empty($name)) {
			$pageArr =	$CI->config->item($name,'map');
		}
		$head_elements = "";
		if(empty($jsorcss))
		{
			
			foreach (array('css','js') as $val) {
				
				switch($val)
				{
					case 'css':
						foreach ($allArr['css'] as $val) {			
							$head_elements .='<link rel="stylesheet" type="text/css" href="'.base_url().$cssjsArr['css'][$val].'">';
						}
						if(isset($pageArr['css'])) {
							foreach ($pageArr['css'] as $val) {
								$head_elements .='<link rel="stylesheet" type="text/css" href="'.base_url().$cssjsArr['css'][$val].'">';
							}
						}
						break;
				
					case 'js':
						foreach ($allArr['js'] as $val) {
							$head_elements .='<script src="'.base_url().$cssjsArr['js'][$val].'"></script>';
						}
						if(isset($pageArr['js'])) {
							foreach ($pageArr['js'] as $val) {
								$head_elements .='<script src="'.base_url().$cssjsArr['js'][$val].'"></script>';
							}	
						}
						break;
				
					default:
						
				}
			}				
			
		} else {
			$arr1 =	isset($allArr[$jsorcss]) ? $allArr[$jsorcss]: array();
			$arr2 =	isset($pageArr[$jsorcss]) ? $pageArr[$jsorcss]: array();
			
			if($jsorcss=='css') {
				foreach ($arr1 as $val) {
					$head_elements .='<link rel="stylesheet" type="text/css" href="'.base_url().$cssjsArr['css'][$val].'">';
				}
				foreach ($arr2 as $val) {
					$head_elements .='<link rel="stylesheet" type="text/css" href="'.base_url().$cssjsArr['css'][$val].'">';
				}
			}
			if($jsorcss=='js') {
				foreach ($arr1 as $val) {
					$head_elements .='<script src="'.base_url().$cssjsArr['js'][$val].'"></script>';
				}
				foreach ($arr2 as $val) {
					$head_elements .='<script src="'.base_url().$cssjsArr['js'][$val].'"></script>';
				}
			}			
		}
	return 	$head_elements;
	}
}