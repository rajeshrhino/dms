<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * MY_Model Class
 *
 * This class object is used for the general model functions
 *
 * @package		CodeIgniter
 * @subpackage	Core
 * @category	Core
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
class MY_Model extends CI_Model 
{
    /**
	 * MY_Model Constructor
	 */
    public function __construct()
    {
        parent::__construct();
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Gets the maximum value from a collection and increments with 1  
	 *
	 * @access	public
	 * @return	interger
	 */
    function get_max_id_from_collection($collection) 
    {
        $collection = $this->mongo_db->select(array('_id'))->order_by(array('_id' => -1))->limit(1)->get($collection);
        
        $result = current($collection);

        if (!empty($result->_id)) {

            $id = ++$result->_id;

        } else {

            $id = 1;

        }

        return $id;
    }
	
	// --------------------------------------------------------------------
    
    /**
	 * Gets the maximum id for a field from a collection and increments with 1  
	 *
	 * @access	public
	 * @return	interger
	 */
    function get_max_field_id_from_collection($collection , $field_name) 
    {
        $collection = $this->mongo_db->select(array($field_name))->order_by(array($field_name => -1))->limit(1)->get($collection);
        
        $result = current($collection);
        
        return ++$result->_id;
    }
}