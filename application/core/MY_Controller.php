<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * MY_Controller Class
 *
 * This class object is used for the home page.
 *
 * @package		CodeIgniter
 * @subpackage	Core
 * @category	Core
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */
 
class MY_Controller extends CI_Controller 
{
	// Declaration of data array
	protected $data;
	
	// Declaration of user object
	protected $user;
	
    /**
	 * MY_Controller Constructor
	 */
    public function __construct()
    {
        parent::__construct();
		
		// Load the labels, messages, validations messages based in the user's preferred language
		$this->lang->load('form_label', 'english');
		$this->lang->load('form_validation', 'english');
		$this->lang->load('message', 'english');

		// Load User Library	
		$this->load->library('userlibrary');
		$this->user = $this->userlibrary->get_logged_in_user_details();
		
		// Get the site setting - 'Site Under Maintenance?'
		$this->load->model('site_settings');
		$settings = $this->site_settings->get_settings(array('name' => 'site_under_maintenance'));
		
		// Check if website is under maintenance, if YES redirect to maintenance page
		// Super admins can override the site maintenance mode and login and can access backend 
		$controller = $this->uri->segment(1);
		if ($controller !== 'maintenance' AND $controller !== 'login' AND $settings['site_under_maintenance'] == 1 AND @$this->user->role_id != 1)
		{
			redirect('maintenance', 'refresh');
		}
		
		if ($settings['site_under_maintenance'] == 1 AND @$this->user->role_id == 1)
		{
			$this->data['display_maintenance_mode_message'] = 1;
		}
		
		// Check if user is logged in, to display Logout link instead of Login
		if (isset($this->user))
		{
			$this->data['user_logged_in'] = 1;
		}
		
		// Set the timezone to the one preferred by the user
		$timezones = $this->timezone->set_time_zone('GMT');
    }
    
    // --------------------------------------------------------------------
  
    /**
	 * Load webpages with header and footer  
	 *
	 * @access	public
	 * @param	string   website or backend
	 * @param	string   view name
	 * @param	array    data                  
	 */
    public function load_page($panel = 'website' , $viewname, $include_menu = FALSE, $data = NULL)
    {
		$this->load->view($panel . '/common/header', $data);
		
        if ($include_menu)
        {
            $this->load->view($panel . '/common/menu', $data);
        }
        
        $this->load->view($viewname, $data);
		
		$this->load->view($panel . '/common/footer');
    }
	
	// --------------------------------------------------------------------
  
    /**
	 * Checks whether user is logged in or not
	 *
	 * @access	public
	 * @return	void
	 */
    public function check_logged_in($override_redirect = FALSE)
    {
        if($session_data = $this->session->userdata('logged_in'))
		{
			//return TRUE;
			return $session_data['_id'];
		}
		else
		{
			//If no session, redirect to login form
			// Over ride redirect for certain pages/forms, so that the controller can redirect to any page/form instead of login form
			if ($override_redirect == FALSE)
			{
				$this->session->set_flashdata('message', $this->lang->line('message_login_to_continue'));
				
				redirect('login', 'refresh');
			}
		}
		//return FALSE;
    }
    
    // --------------------------------------------------------------------

	/**
	 * Send email  
	 *
	 * @access	public
	 * @param	array    user details     
	 * @return	array	
	 */
    public function send_email($data , $email_template = 'welcome' , $subject = 'Welcome to {_int} DMS') 
    {
        if (is_object($data)) 
        {
            $data = (array)$data;
        }
       
        $this->load->library('email');
          
		// Get the site setting - for from email address and name
		$this->load->model('site_settings');
		$settings = $this->site_settings->get_settings();
		  
        $this->email->from($settings['global_from_email_address'], $settings['global_from_email_name']);
          
        $this->email->to($data['email']);
          
        $this->email->subject($subject);
        
		if (!empty($data['first_name']) OR !empty($data['last_name'])) 
		{
			$data['display_name'] = $data['first_name'].' '.$data['last_name'];
		}
        
		if (!empty($data['username']))
		{
			$data['display_name'] = $data['username'];
            
            $data['username'] = $data['username'];
		}
        
        $message = $this->load->view('email_templates/'.$email_template, $data , TRUE); 
          
        $this->email->message($message);
          
        $status = $this->email->send();
    }
    
    // --------------------------------------------------------------------

	/**
	 * Generate a checksum for a user Id   
	 *
	 * @param int    $user_id
     * @param int    $ts         timestamp that checksum was generated
     * @param int    $live       life of this checksum in hours/ 'inf' for infinite
     * @param string $hash       contact hash, if sent, prevents a query in inner loop
     *
     * @return array ( $cs, $ts, $live )
     * @static
     * @access public	
	 */
    public function generate_check_sum($user_id, $ts = NULL, $live = NULL, $hash = NULL)  
    {   
        $this->load->model('users');
        
        $hash = $this->users->get_user_details(array('_id' => (int)$user_id))->hash;
        
        if (!$ts) 
        {
            $ts = time();
        }
    
        if (!$live) 
        {
            $live = CHECKSOME_LIFE_TIME;
        }
        
        $sep = VALUE_SEPERATOR;
        
        //$checksum = md5("{$hash}{$sep}{$user_id}{$sep}{$ts}{$sep}{$live}");
         
        return "{$hash}{$sep}{$ts}{$sep}{$live}{$sep}{$user_id}";
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Validate the checksum for user Id
     *
     * @param int    $user_id
     * @param string $cs         checksum to match against
     * @param int    $ts         timestamp that checksum was generated
     * @param int    $live       life of this checksum in hours/ 'inf' for infinite
     *
     * @return boolean           true if valid, else false
     * @static
     * @access public
     */
    public function validate_check_sum($user_id, $checksum)
    {
        $input = @explode(VALUE_SEPERATOR, $checksum);
    
        $inputCS = $input[0];
        
        $inputTS = $input[1];
        
        $inputLF = $input[2];
        
        $orginal_checksum = $this->generate_check_sum($user_id, $inputTS, $inputLF);
        
        if ($orginal_checksum != $checksum) 
        {
            return FALSE;
        }
    
        // no life limit for checksum
        if ($inputLF == 'inf') 
        {
            return TRUE;
        }
    
        // checksum matches so now check timestamp
        $now = time();
        
        return ($inputTS + ($inputLF * 60 * 60) >= $now);
    }
	
	// --------------------------------------------------------------------
    
    /**
     * Sort a multidimensional array using a array field
     *
     * @param array		$to_sort_array         	array to be sorted
     * @param string    $array_field         	array field by which the array needs to be sorted
     * @param string    $sort_order       		sort order ASC or DESC
     *
     * @return array    $sorted_array       	Sorted array
     * @static
     * @access public
     */
    public function sort_array_by_field(&$to_sort_array, $array_field, $sort_order = 'DESC')
    {
		foreach ($to_sort_array as $value)
		{
			$sort_field_array[] = $value[$array_field];
		}
		
		array_multisort($sort_field_array, SORT_DESC, $to_sort_array);
	}
	
	// --------------------------------------------------------------------
    
    /**
     * Check permission whether the user has permission to access this controller
     *
	 * @param string    $controller		Controller class name
     * @static
     * @access public
     */
    public function check_permission($controller)
    {
		$this->load->library('userlibrary');
		
		$user = $this->userlibrary->get_logged_in_user_details();
		
		$role_id = $user->role_id;
		
		// Load the config permissions array
		$permissions = config_item('permissions');
		
		if (in_array($role_id , $permissions['backend'][$controller]))
		{
			return TRUE;
		}
		
		else
		{
			$this->session->set_flashdata('message', $this->lang->line('message_access_denied'));
			
			//If access denied, redirect to dashboard
            redirect('edocs/dashboard/home', 'refresh');
		}
	}
	
	// --------------------------------------------------------------------
  
    /**
	 * Checks whether user is admin user (User id : 1)
	 *
	 * @access	public
	 * @return	void
	 */
    public function is_admin_user()
    {
        $session_data = $this->session->userdata('logged_in');
            
        if($session_data['_id'] == 1)
		{
			return TRUE;
		}
		
		return FALSE;
    }
	
	// --------------------------------------------------------------------
    
    /**
	 * Check whether the checksum is is expired or already used
	 *
	 * @access	public
	 * @params  interger    user id
     * @params  string     	timestamp when the URL is create
	 * @return	void
	 */
    public function check_checksum_url($user_id , $timestamp) 
    {
		$this->load->model('users_access');
		
        // Get the user last access details
		$user_access = $this->users_access->get_single_record(array('user_id' => $user_id));
		
		// Get current time
		$current = time();
		
		$timeout = CHECKSOME_LIFE_TIME * 60 * 60;
		
		if ($current - $timestamp > $timeout)
		{
			return array (FALSE , $this->lang->line('one_time_login_link_expired'));
		}
        
        if (isset($user_access) AND $timestamp < $user_access->last_access)
		{
			return array (FALSE , $this->lang->line('one_time_login_link_used'));
		}
		
        return array(TRUE);
    }
}

/* End of file MY_Controller.php */
/* Location: ./application/libraries/MY_Controller.php */