<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Site settings Model Class
 *
 * This class object is used to access the packages collection
 *
 * @package		CodeIgniter
 * @subpackage	Model
 * @category	Model
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

Class Site_settings extends MY_Model
{
    /**
     * private instance to hold the collections name
     *
     * @var string
     * @static
     */
    private $collection_name = 'site_settings';
    
    /**
	 * General Constructor
	 */
    function __construct() 
    {
        parent::__construct();
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives an entry from site settings collection
	 *
	 * @access	public
	 * @params  string   collection name
	 * @return	array
	 */
    function get_settings($params = null) 
	{
        if (empty($params)) 
        {
            $results = $this->mongo_db->get($this->collection_name);    
        }
        else 
        {        
	        $results = $this->mongo_db->where($params)->get($this->collection_name);
        }
		
		$settings = array();
		
		foreach ($results as $result)
		{
			$settings[$result->name] = $result->value;
		}
        
		if (!empty($settings)) 
		{
			return $settings;
		}
	}
	
	// --------------------------------------------------------------------
    
    /**
	 * Updates a entry in site settings collection
	 *
	 * @access	public
	 * @params 	array 	   user details	
	 * @return	void
	 */
    function update_entry($name, $params) 
    {
        return $this->mongo_db->where(array('name' => $name))->set($params)->update($this->collection_name);
    }
}

/* End of file site_settings.php */
/* Location: ./application/model/site_settings.php */