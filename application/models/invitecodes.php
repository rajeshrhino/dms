<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Invitation codes Model Class
 *
 * This class object is used to access the documents in 'invitecodes' collection.
 *
 * @package		CodeIgniter
 * @subpackage	Model
 * @category	Model
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

Class Invitecodes extends MY_Model
{
    /**
     * private instance to hold the collections name
     *
     * @var string
     * @static
     */
    private $collection_name = 'invitecodes';
    
    // --------------------------------------------------------------------
    
    /**
	 * User Constructor
	 */
    function __construct() 
    {
        parent::__construct();
        
        $this->load->driver('cache');
        
        //var_dump($this->cache->memcached->is_supported());
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives single document from a collection 
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_single_record($params) 
	{
		$result_set = $this->mongo_db->where($params)->limit(1)->get($this->collection_name);

        if (!empty($result_set)) 
        {
            $result = current($result_set);
            
            if (is_object($result)) 
    		{
    			return $result;
    		}
        }
        
        return NULL;
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives documents from a collection 
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_records($params = array()) 
	{
		$results = $this->mongo_db->where($params)->get($this->collection_name);
		
		if (!empty($results)) 
		{
			return $results;
		}
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * Inserts a company document in the database 
	 *
	 * @access	public
	 * @params  array    company details
	 * @return	bool
	 */
    function insert($params)
	{
        $company = $this->mongo_db->insert($this->collection_name, array('_id' => $this->get_max_id_from_collection($this->collection_name)) + $params );
        
        return $company;                 
	}
    
	// --------------------------------------------------------------------
    
    /**
	 * Updates a company record
	 *
	 * @access	public
	 * @params  integer    company id           
	 * @params 	array 	   company details	
	 * @return	void
	 */
    function update($id, $params) 
    {
        return $this->mongo_db->where(array( '_id' => $id ))->set($params)->update($this->collection_name);
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Delete a company record
	 *
	 * @access	public
	 * @params  integer    company id           
	 * @return	void
	 */
    function delete($id) 
    {
		return $this->mongo_db->where(array( '_id' => $id ))->delete($this->collection_name);
    }
	
	// --------------------------------------------------------------------
    
    /**
	 * Retrives documents from a collection where id is not equal to
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_records_where_ne($params) 
	{
		$results = $this->mongo_db->where_ne('_id' , $id)->get($this->collection_name);
		
		if (!empty($results)) 
		{
			return $results;
		}
	}
	
	// --------------------------------------------------------------------
    
    /**
	 * Checks whether a value exists for a field in collection 
	 *
	 * @access	public
	 * @params  string    fieldname
	 * @params  string    value                 
	 * @return	bool
	 */
    function check_availability($fieldname, $value)
	{
        $user = $this->mongo_db->where(array($fieldname => $value))->limit(1)->get($this->collection_name);
        
        if (!empty($user))
		{
            $result = current($user);
            
            if (is_object($result)) 
    		{
    			return false;
    		}
            else 
            {
                return true;
            }
		}
		else
		{
			return true;
		}
	}
}

/* End of file invitecodes.php */
/* Location: ./application/model/invitecodes.php */