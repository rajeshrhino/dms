<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Packages Model Class
 *
 * This class object is used to access the packages collection
 *
 * @package		CodeIgniter
 * @subpackage	Model
 * @category	Model
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

Class Packages extends MY_Model
{
    /**
     * private instance to hold the collections name
     *
     * @var string
     * @static
     */
    private $collection_name = 'packages';
    
    /**
	 * General Constructor
	 */
    function __construct() 
    {
        parent::__construct();
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives documents from a collection 
	 *
	 * @access	public
	 * @params  string   collection name
	 * @params  array    search params	
	 * @return	array
	 */
    function get_packages($params = null) 
	{
        if (empty($params)) 
        {
            $results = $this->mongo_db->get($this->collection_name);    
        }
        else 
        {        
	        $results = $this->mongo_db->where($params)->get($this->collection_name);
        }
		
		$packages = array();
		
		foreach ($results as $result)
		{
			$packages[$result->_id] = $result->package_name; 
		}
        
		if (!empty($packages)) 
		{
			return $packages;
		}
	}
}

/* End of file user.php */
/* Location: ./application/model/packages.php */