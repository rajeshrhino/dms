<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * User Model Class
 *
 * This class object is used to access the documents in 'folders' collection.
 *
 * @package		CodeIgniter
 * @subpackage	Model
 * @category	Model
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

Class Messagetemplate extends MY_Model
{
    /**
     * private instance to hold the collections name
     *
     * @var string
     * @static
     */
    private $collection_name = 'message_templates';
    
    // --------------------------------------------------------------------
    
    /**
	 * User Constructor
	 */
    function __construct() 
    {
        parent::__construct();
        
        $this->load->driver('cache');
        
        //var_dump($this->cache->memcached->is_supported());
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives single document from a collection 
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_single_record($params) 
	{
		$result_set = $this->mongo_db->where($params)->limit(1)->get($this->collection_name);
		
		if (!empty($result_set)) 
        {
            $result = current($result_set);
            
            if (is_object($result)) 
    		{
    			return $result;
    		}
        }
        return NULL;
	}
	
	// --------------------------------------------------------------------
    
    /**
	 * Retrives documents from a collection where id is not equal to
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_records_where_ne($id) 
	{
		$results = $this->mongo_db->where_ne('_id' , $id)->get($this->collection_name);
		
		if (!empty($results)) 
		{
			return $results;
		}
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives documents from a collection 
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_records($params = array()) 
	{
		$results = $this->mongo_db->where($params)->get($this->collection_name);
        
		if (!empty($results)) 
		{
			return $results;
		}
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * Inserts a folder in the database 
	 *
	 * @access	public
	 * @params  array    folder details
	 * @params  string   username of the logged in user
	 * @return	bool
	 */
    function insert($params)
	{
        $folder = $this->mongo_db->insert($this->collection_name, array('_id' => $this->get_max_id_from_collection($this->collection_name)) + $params );
        
        return $folder;                 
	}
    
	// --------------------------------------------------------------------
    
    /**
	 * Updates a folder record
	 *
	 * @access	public
	 * @params  integer    folder id           
	 * @params 	array 	   folder details
	 * @return	void
	 */
    function update($id , $params) 
    {
		return $this->mongo_db->where(array( '_id' => $id ))->set($params)->update($this->collection_name);
    }
	
	// --------------------------------------------------------------------
    
    /**
	 * Delete a folder record
	 *
	 * @access	public
	 * @params  integer    folder id           
	 * @return	void
	 */
    function delete($id) 
    {
		return $this->mongo_db->where(array( '_id' => $id ))->delete($this->collection_name);
    }
}

/* End of file messagetemplates.php */
/* Location: ./application/model/messagetemplates.php */