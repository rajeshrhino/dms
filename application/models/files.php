<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * User Model Class
 *
 * This class object is used to access the documents in 'folders' collection.
 *
 * @package		CodeIgniter
 * @subpackage	Model
 * @category	Model
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

Class Files extends MY_Model
{
    /**
     * private instance to hold the collections name
     *
     * @var string
     * @static
     */
    private $collection_name = 'files';
    
    // --------------------------------------------------------------------
    
    /**
	 * User Constructor
	 */
    function __construct() 
    {
        parent::__construct();
        
        $this->load->driver('cache');
        
        //var_dump($this->cache->memcached->is_supported());
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives single document from a collection 
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_single_record($params ,  $username) 
	{
		$result_set = $this->mongo_db->where($params)->limit(1)->get($username.'.'.$this->collection_name);
		
		if (!empty($result_set)) 
        {
            $result = current($result_set);
            
            if (is_object($result)) 
    		{
    			return $result;
    		}
        }
        return NULL;
	}
	
	// --------------------------------------------------------------------
    
    /**
	 * Retrives documents from a collection where id is not equal to
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_records_where_ne($id ,  $username) 
	{
		$results = $this->mongo_db->where_ne('_id' , $id)->get($username.'.'.$this->collection_name);
		
		if (!empty($results)) 
		{
			return $results;
		}
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives documents from a collection 
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_records($params , $username) 
	{
		$results = $this->mongo_db->where($params)->get($username.'.'.$this->collection_name);
        
		if (!empty($results)) 
		{
			return $results;
		}
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * Inserts a folder in the database 
	 *
	 * @access	public
	 * @params  array    folder details
	 * @params  string   username of the logged in user
	 * @return	bool
	 */
    function insert($params , $username)
	{
        $folder = $this->mongo_db->insert($username.'.'.$this->collection_name, array('_id' => $this->get_max_id_from_collection($username.'.'.$this->collection_name)) + $params );
        
        return $folder;                 
	}
    
	// --------------------------------------------------------------------
    
    /**
	 * Updates a folder record
	 *
	 * @access	public
	 * @params  integer    folder id           
	 * @params 	array 	   folder details
	 * @return	void
	 */
    function update($id , $params , $username) 
    {
		return $this->mongo_db->where(array( '_id' => $id ))->set($params)->update($username.'.'.$this->collection_name);
    }
	
	// --------------------------------------------------------------------
    
    /**
	 * Delete a folder record
	 *
	 * @access	public
	 * @params  integer    folder id           
	 * @return	void
	 */
    function delete($id , $username) 
    {
		return $this->mongo_db->where(array( '_id' => $id ))->delete($username.'.'.$this->collection_name);
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Checks whether a value exists for a field in collection 
	 *
	 * @access	public
	 * @params  string    fieldname
	 * @params  string    value                 
	 * @return	bool
	 */
    function check_availability($fieldname, $value)
	{
        $user = $this->mongo_db->where(array($fieldname => $value))->limit(1)->get($this->collection_name);
        
        if (!empty($user))
		{
            $result = current($user);
            
            if (is_object($result)) 
    		{
    			return false;
    		}
            else 
            {
                return true;
            }
		}
		else
		{
			return true;
		}
	}
}

/* End of file user.php */
/* Location: ./application/model/users.php */