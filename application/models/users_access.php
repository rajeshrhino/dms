<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * User Model Class
 *
 * This class object is used to access the documents in 'users' collection.
 *
 * @package		CodeIgniter
 * @subpackage	Model
 * @category	Model
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

Class Users_access extends MY_Model
{
    /**
     * static instance to hold the collections name
     *
     * @var string
     * @static
     */
    private $collection_name = 'users_access';
    
    // --------------------------------------------------------------------
    
    /**
	 * User Constructor
	 */
    function __construct() 
    {
        parent::__construct();
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives single document from a collection 
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_single_record($params) 
	{
		$result_set = $this->mongo_db->where($params)->limit(1)->order_by(array('last_access' => 'DESC'))->get($this->collection_name);
        
        if (!empty($result_set)) 
        {
            $result = current($result_set);
            
            if (is_object($result)) 
    		{
    			return $result;
    		}
        }
        
        return NULL;
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives documents from a collection 
	 *
	 * @access	public
	 * @params  string   collection name
	 * @params  array    search params	
	 * @return	array
	 */
    function get_records($params) 
	{
		$results = $this->mongo_db->where($params)->get($this->collection_name);
        
		if (!empty($results)) 
		{
			return $results;
		}
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * Updates last access time, IP address for a user 
	 *
	 * @access	public
	 * @params  integer    user id           
	 * @return	void
	 */
    function update_last_access($user_id, $params , $action) 
    {
        if ($action == 'update') 
        {
            $this->mongo_db->where(array( 'user_id' => $user_id ))->set($params + array('last_access' => time(), 'ip_address' => $this->input->ip_address()))->update($this->collection_name);
        } 
        elseif ($action == 'insert') 
        {
            $user = $this->mongo_db->insert($this->collection_name, array('_id' => $this->get_max_id_from_collection($this->collection_name) , 'user_id' => $user_id) + $params + array('last_access' => time(), 'ip_address' => $this->input->ip_address()));
        }
    }
}

/* End of file user.php */
/* Location: ./application/model/users_access.php */