<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * User Model Class
 *
 * This class object is used to access the documents in 'users' collection.
 *
 * @package		CodeIgniter
 * @subpackage	Model
 * @category	Model
 * @author		Rajesh Sundararajan
 * @copyright   Copyright (c) 2012 Rajesh Sundararajan 
 * @license     GNU AGPL http://www.gnu.org/licenses/agpl.html 
 * @link		http://codeigniter.com/
 */

Class Users extends MY_Model
{
    /**
     * private instance to hold the collections name
     *
     * @var string
     * @static
     */
    private $collection_name = 'users';
    
    // --------------------------------------------------------------------
    
    /**
	 * User Constructor
	 */
    function __construct() 
    {
        parent::__construct();
        
        $this->load->driver('cache');
        
        //var_dump($this->cache->memcached->is_supported());
    }
	
    // --------------------------------------------------------------------
    
    /**
	 * Retrives single document from a collection 
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_single_record($params) 
	{
		$result_set = $this->mongo_db->where($params)->limit(1)->get($this->collection_name);
        
        if (!empty($result_set)) 
        {
            $result = current($result_set);
            
            if (is_object($result)) 
    		{
    			return $result;
    		}
        }
        
        return NULL;
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives user details from a collection 
	 *
	 * @access	public
	 * @params  array     user params array with only one key _id	
	 * @return	array
	 */
    function get_user_details($params) 
	{
        if (empty($params['_id'])) 
        {
            return;
        }
        
		if ($this->config->item('memcache_enabled') == TRUE)
		{
			$user = $this->cache->memcached->get($this->collection_name.VALUE_SEPERATOR.$params['_id']);
        
			if (!$user)
			{
				$result_set = $this->mongo_db->where($params)->limit(1)->get($this->collection_name);
				
				if (!empty($result_set)) 
				{
					$result = current($result_set);
					
					if (is_object($result)) 
					{
						$this->cache->memcached->save($this->collection_name.VALUE_SEPERATOR.$params['_id'], $result, 10);
					
						return $result;
					}
				}
			}
			else 
			{
				return $user;
			}
		}
		else
		{
			$result_set = $this->mongo_db->where($params)->limit(1)->get($this->collection_name);
				
			if (!empty($result_set)) 
			{
				$result = current($result_set);
				
				if (is_object($result)) 
				{
					$this->cache->memcached->save($this->collection_name.VALUE_SEPERATOR.$params['_id'], $result, 10);
				
					return $result;
				}
			}
		}
        
        return;
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * Retrives documents from a collection 
	 *
	 * @access	public
	 * @params  array    search params	
	 * @return	array
	 */
    function get_records($params) 
	{
		$results = $this->mongo_db->where($params)->get($this->collection_name);
        
		if (!empty($results)) 
		{
			return $results;
		}
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * Inserts a user document in the database 
	 *
	 * @access	public
	 * @params  array    user details
	 * @return	bool
	 */
    function insert($params)
	{
        $user = $this->mongo_db->insert($this->collection_name, array('_id' => $this->get_max_id_from_collection($this->collection_name)) + $params );
        
        return $user;                 
	}
    
	// --------------------------------------------------------------------
    
    /**
	 * Updates a user record
	 *
	 * @access	public
	 * @params  integer    user id           
	 * @params 	array 	   user details	
	 * @return	void
	 */
    function update($uid, $params) 
    {
        return $this->mongo_db->where(array( '_id' => $uid ))->set($params)->update($this->collection_name);
    }
    
    // --------------------------------------------------------------------
    
    /**
	 * Checks whether a value exists for a field in collection 
	 *
	 * @access	public
	 * @params  string    fieldname
	 * @params  string    value                 
	 * @return	bool
	 */
    function check_availability($fieldname, $value)
	{
        $user = $this->mongo_db->where(array($fieldname => $value))->limit(1)->get($this->collection_name);
        
        if (!empty($user))
		{
            $result = current($user);
            
            if (is_object($result)) 
    		{
    			return false;
    		}
            else 
            {
                return true;
            }
		}
		else
		{
			return true;
		}
	}
	
	// --------------------------------------------------------------------
    
    /**
	 * Gets the count from collection 
	 *
	 * @access	public
	 * @params  string    collection_name
	 * @return	bool
	 */
    function count_users()
	{
        $count = $this->mongo_db->count($this->collection_name);
        
        if (!empty($count)) 
		{
			return $count;
		}        
	}
	
	/**
	 * Delete a user record
	 *
	 * @access	public
	 * @params  integer    user id           
	 * @return	void
	 */
    function delete($id) 
    {
		return $this->mongo_db->where(array( '_id' => $id ))->delete($this->collection_name);
    }
}

/* End of file user.php */
/* Location: ./application/model/users.php */