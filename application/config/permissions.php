<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Permissions for roles to access controllers
|--------------------------------------------------------------------------
|
*/

$config['permissions']['backend'] = array(
                                    'Dashboard'			=> array(1 , 2 , 3),
									'Documentlist'		=> array(1 , 2 , 3),
									'Documentinfo'		=> array(1 , 2 , 3),
									'Documentupload'	=> array(1 , 2 , 3),
									'Folder'			=> array(1 , 2 , 3),
									'Document'			=> array(1 , 2 , 3),
									'Accountsettings'	=> array(1 , 2 , 3),
									// Below are controllers which only 'admin role' can access
									'Userslist'			=> array(1),
									'Admindashboard'	=> array(1),
									'Sitesettings'  	=> array(1),
									'Sitemaintenance'  	=> array(1),
									'Messagetemplates'	=> array(1),
									'Companieslist'		=> array(1),
									'Companyuserslist'	=> array(1),
									'Addcorporateuser'  => array(1),
									'Invitecode'  		=> array(1),
									'Resetpassword'  	=> array(1 , 2 , 3),
									'Corporateuserslist'=> array(2),
                                    );

/* End of file permissions.php */
/* Location: ./application/config/permissions.php */