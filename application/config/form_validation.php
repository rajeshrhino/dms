<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Saving Sets of Validation Rules to a Config File
|--------------------------------------------------------------------------
|
*/

$config = array(
                 'registerindividual' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email Address',
                                            'rules' => 'trim|required|xss_clean|valid_email'
                                         ),               
                                    array(
                                            'field' => 'username',
                                            'label' => 'Username',
                                            'rules' => 'trim|required|xss_clean|min_length[6]|alpha_dash'
                                         ),
                                    /*array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|xss_clean|matches[passconf]|min_length[8]|alpha_dash'
                                         ),
                                    array(
                                            'field' => 'passconf',
                                            'label' => 'Password Confirmation',
                                            'rules' => 'required|xss_clean|min_length[8]|alpha_dash'
                                         ),*/
                                    array(
                                            'field' => 'terms_and_conditions',
                                            'label' => 'Terms and Conditions',
                                            'rules' => 'required|xss_clean'
                                         )
                                    ),
				  'registerindividualwithinvitationcode' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email Address',
                                            'rules' => 'trim|required|xss_clean|valid_email'
                                         ),               
                                    array(
                                            'field' => 'username',
                                            'label' => 'Username',
                                            'rules' => 'trim|required|xss_clean|min_length[6]|alpha_dash'
                                         ),
                                    /*array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|xss_clean|matches[passconf]|min_length[8]|alpha_dash'
                                         ),
                                    array(
                                            'field' => 'passconf',
                                            'label' => 'Password Confirmation',
                                            'rules' => 'required|xss_clean|min_length[8]|alpha_dash'
                                         ),*/
                                    array(
                                            'field' => 'terms_and_conditions',
                                            'label' => 'Terms and Conditions',
                                            'rules' => 'required|xss_clean'
                                         ),
									array(
                                            'field' => 'invite_code',
                                            'label' => 'Invitation Code',
                                            'rules' => 'required|xss_clean'
                                         ), 
                                    ),					
				 'registerindividual_backends' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email Address',
                                            'rules' => 'trim|required|xss_clean|valid_email'
                                         ),               
                                    array(
                                            'field' => 'username',
                                            'label' => 'Username',
                                            'rules' => 'trim|required|xss_clean|min_length[6]|alpha_dash'
                                         ),
                                    /*array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|xss_clean|matches[passconf]|min_length[8]|alpha_dash'
                                         ),
                                    array(
                                            'field' => 'passconf',
                                            'label' => 'Password Confirmation',
                                            'rules' => 'required|xss_clean|min_length[8]|alpha_dash'
                                         ),*/
                                    ),					
                 'registercompany' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email Address',
                                            'rules' => 'trim|required|xss_clean|valid_email'
                                         ),
                                    array(
                                            'field' => 'username',
                                            'label' => 'Username',
                                            'rules' => 'trim|required|xss_clean|min_length[6]|alpha_dash'
                                         ),
                                    /*array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|xss_clean|matches[passconf]|min_length[8]|alpha_dash'
                                         ),
                                    array(
                                            'field' => 'passconf',
                                            'label' => 'Password Confirmation',
                                            'rules' => 'required|xss_clean|min_length[8]|alpha_dash'
                                         ),*/
                                    array(
                                            'field' => 'company_name',
                                            'label' => 'Company Name',
                                            'rules' => 'required|trim|xss_clean'
                                         ),     
                                    array(
                                            'field' => 'company_street_address',
                                            'label' => 'Street Address',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'company_city',
                                            'label' => 'City',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'company_postcode',
                                            'label' => 'Postal Code',
                                            'rules' => 'trim|xss_clean'
                                         ),               
                                    array(
                                            'field' => 'terms_and_conditions',
                                            'label' => 'Terms and Conditions',
                                            'rules' => 'required|xss_clean'
                                         )
                                    ),
				 'registercompanywithinvitationcode' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email Address',
                                            'rules' => 'trim|required|xss_clean|valid_email'
                                         ),
                                    array(
                                            'field' => 'username',
                                            'label' => 'Username',
                                            'rules' => 'trim|required|xss_clean|min_length[6]|alpha_dash'
                                         ),
                                    /*array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|xss_clean|matches[passconf]|min_length[8]|alpha_dash'
                                         ),
                                    array(
                                            'field' => 'passconf',
                                            'label' => 'Password Confirmation',
                                            'rules' => 'required|xss_clean|min_length[8]|alpha_dash'
                                         ),*/
                                    array(
                                            'field' => 'company_name',
                                            'label' => 'Company Name',
                                            'rules' => 'required|trim|xss_clean'
                                         ),     
                                    array(
                                            'field' => 'company_street_address',
                                            'label' => 'Street Address',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'company_city',
                                            'label' => 'City',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'company_postcode',
                                            'label' => 'Postal Code',
                                            'rules' => 'trim|xss_clean'
                                         ),               
                                    array(
                                            'field' => 'terms_and_conditions',
                                            'label' => 'Terms and Conditions',
                                            'rules' => 'required|xss_clean'
                                         ),
									array(
                                            'field' => 'invite_code',
                                            'label' => 'Invitation Code',
                                            'rules' => 'required|xss_clean'
                                         ),	 
                                    ),
				 'registercompany_backend' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email Address',
                                            'rules' => 'trim|required|xss_clean|valid_email'
                                         ),
                                    array(
                                            'field' => 'username',
                                            'label' => 'Username',
                                            'rules' => 'trim|required|xss_clean|min_length[6]|alpha_dash'
                                         ),
                                    /*array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|xss_clean|matches[passconf]|min_length[8]|alpha_dash'
                                         ),
                                    array(
                                            'field' => 'passconf',
                                            'label' => 'Password Confirmation',
                                            'rules' => 'required|xss_clean|min_length[8]|alpha_dash'
                                         ),*/
                                    array(
                                            'field' => 'company_name',
                                            'label' => 'Company Name',
                                            'rules' => 'required|trim|xss_clean'
                                         ),     
                                    array(
                                            'field' => 'company_street_address',
                                            'label' => 'Street Address',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'company_city',
                                            'label' => 'City',
                                            'rules' => 'trim|xss_clean'
                                         ),
                                    array(
                                            'field' => 'company_postcode',
                                            'label' => 'Postal Code',
                                            'rules' => 'trim|xss_clean'
                                         ),               
                                    ), 					
                 'login/index' => array(
                                    array(
                                            'field' => 'username',
                                            'label' => 'Username',
                                            'rules' => 'trim|required|xss_clean'
                                         ),
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'trim|required|xss_clean'
                                         ),
                                    ),
				'accountsettings' => array(
									array(
                                            'field' => 'email',
                                            'label' => 'Email Address',
                                            'rules' => 'trim|required|xss_clean|valid_email'
                                         ),
									array(
                                            'field' => 'passcurr',
                                            'label' => 'Current Password',
                                            'rules' => 'required|xss_clean'
                                         ),
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'xss_clean|matches[passconf]|min_length[8]|alpha_dash'
                                         ),
                                    array(
                                            'field' => 'passconf',
                                            'label' => 'Password Confirmation',
                                            'rules' => 'xss_clean|min_length[8]|alpha_dash'
                                         ),
                                    ),									
				'resetpassword' => array(
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|xss_clean|matches[passconf]|min_length[8]|alpha_dash'
                                         ),
                                    array(
                                            'field' => 'passconf',
                                            'label' => 'Password Confirmation',
                                            'rules' => 'required|xss_clean|min_length[8]|alpha_dash'
                                         ),
                                    ),					
				'messagetemplate' => array(
									array(
                                            'field' => 'message_title',
                                            'label' => 'Message Title',
                                            'rules' => 'required|xss_clean'
                                         ),
                                    array(
                                            'field' => 'message_subject',
                                            'label' => 'Message Subject',
                                            'rules' => 'xss_clean'
                                         ),
                                    array(
                                            'field' => 'text',
                                            'label' => 'Plain text format',
                                            'rules' => 'xss_clean'
                                         ),
									array(
                                            'field' => 'html',
                                            'label' => 'HTML format',
                                            'rules' => ''
                                         ),	 
                                    ),										
				'company' => array(
									array(
                                            'field' => 'company_name',
                                            'label' => 'Company Name',
                                            'rules' => 'required|xss_clean'
                                         ),
                                    array(
                                            'field' => 'company_street_address',
                                            'label' => 'Street Address',
                                            'rules' => 'xss_clean'
                                         ),
                                    array(
                                            'field' => 'company_city',
                                            'label' => 'Company City',
                                            'rules' => 'xss_clean'
                                         ),
									array(
                                            'field' => 'company_postcode',
                                            'label' => 'Postal Code',
                                            'rules' => 'xss_clean'
                                         ),	 
                                    ),
				'forgotpassword/index' => array(
										array(
                                            'field' => 'email',
                                            'label' => 'Email Address',
                                            'rules' => 'trim|required|xss_clean|valid_email'
                                         ), 
                                    ),					
				'sitesettings' => array(
										array(
                                            'field' => 'global_from_email_address',
                                            'label' => 'From E-mail Address',
                                            'rules' => 'trim|required|xss_clean|valid_email'
                                         ), 
										 array(
                                            'field' => 'global_from_email_name',
                                            'label' => 'From E-mail Name',
                                            'rules' => 'trim|required|xss_clean'
                                         ),
										 array(
                                            'field' => 'contact_us_receiving_email_address',
                                            'label' => 'Contact E-mail Addresses',
                                            'rules' => 'trim|required|xss_clean'
                                         ),
                                    ),					
				'invitecode' => array(
										array(
                                            'field' => 'invite_code',
                                            'label' => 'Invitation Code',
                                            'rules' => 'trim|required|xss_clean'
                                         ), 
                                    ),  					 
                  /*'contact' => array(
									array(
                                            'field' => 'name',
                                            'label' => 'Full Name',
                                            'rules' => 'required|xss_clean'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email Address',
                                            'rules' => 'required|xss_clean'
                                         ),
                                    array(
                                            'field' => 'comments',
                                            'label' => 'Message/Comments',
                                            'rules' => 'required|xss_clean'
                                         ),
                                    ),*/
               );

/* End of file form_validation.php */
/* Location: ./application/config/form_validation.php */