<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

/* WEBSITE ROUTES */
$route['default_controller']    		= "website/home";
$route['home']                  		= "website/home";
$route['blog']                  		= "website/home";
$route['about']                  		= "website/about";
$route['team']                  		= "website/home";
$route['company']                  		= "website/home";
$route['contact']                  		= "website/contact";
$route['contact/sendmessage']           = "website/contact/send_message";
$route['pricing']              		    = "website/pricing";
$route['terms']              		    = "website/terms";
$route['privacy']              		    = "website/privacy";
$route['cookiepolicy']                 	= "website/cookiepolicy";
$route['security']              		= "website/security";
$route['features']              		= "website/features";
$route['team']              			= "website/team";
$route['credits']              			= "website/credits";
$route['alphawarning']              	= "website/alphawarning";
$route['maintenance']              		= "website/maintenance";

$route['login']                 		= "user/login";
$route['login/maintenance']             = "user/login";
$route['forgotpassword']                = "user/forgotpassword";
$route['register']              		= "user/register";
$route['registercompany']       		= "user/registercompany";
$route['logout']                		= "user/login/logout";
$route['user/reset/(:any)']   			= "user/activeaccount";
$route['resetpassword/(:any)']   		= "user/resetpassword";
$route['resetpassword']   				= "user/accountsettings";

/* BACKEND ROUTES */
$route['document/(:any)/list']          = "document/documentlist";
$route['document/(:any)/upload']      	= "document/documentupload";
//$route['document/filefunctions']       = "document/file";
//$route['document/folder']         	= "document/folder";
$route['document/info/(:any)/(:any)']   = "document/documentinfo";
$route['accountsettings']     			= "user/accountsettings";
$route['accountsettings/(:any)']     	= "user/accountsettings";

/* ADMIN ROUTES */
$route['admin/dashboard']     			= "admin/admindashboard";
$route['admin/userslist']     			= "admin/userslist";

$route['admin/userslist/(:any)/add']   				= "admin/userslist/build_form";
$route['admin/userslist/(:any)/edit/(:any)']   		= "admin/userslist/build_form";
$route['admin/userslist/(:any)/delete/(:any)'] 		= "admin/userslist/delete";
$route['admin/userslist/save']     					= "admin/userslist/build_form";
$route['admin/userslist/deleteuser/(:any)/(:any)'] 	= "admin/userslist/build_delete_user_form";

$route['admin/companyuserslist/(:any)'] = "admin/companyuserslist";

$route['admin/companies']     			= "admin/companieslist";
$route['admin/companies/add']     		= "admin/companieslist/build_form";
$route['admin/companies/edit/(:any)']   = "admin/companieslist/build_form";
$route['admin/companies/delete/(:any)'] = "admin/companieslist/delete";
$route['admin/companies/save']     		= "admin/companieslist/build_form";
$route['admin/invitecode']     			= "admin/invitecode";
$route['admin/invitecode/add']     		= "admin/invitecode/build_form";
$route['admin/invitecode/edit/(:any)']  = "admin/invitecode/build_form";
$route['admin/invitecode/delete/(:any)']= "admin/invitecode/delete";
$route['admin/invitecode/save']     	= "admin/invitecode/build_form";
$route['admin/templates']     			= "admin/messagetemplates";
$route['admin/templates/add']     		= "admin/messagetemplates/build_form";
$route['admin/templates/save']     		= "admin/messagetemplates/build_form";

$route['edocs'] 	                	= "backend/dashboard";
$route['edocs/dashboard/home']      	= "backend/dashboard";
$route['edocs/quicksearch']  			= "backend/dashboard/quickFileSearchJson";

$route['corporate/corporateuserslist'] 	= "corporate/corporateuserslist";

$route['admin/sitesettings']     		= "admin/sitesettings";
$route['admin/sitemaintenance']     	= "admin/sitemaintenance";

$route['edocs/filemanager']    			= "backend/edocsmanager";
$route['edocs/files/list']    			= "backend/edocsmanager/listdocs";
$route['edocs/filemanager/save']    	= "backend/edocsmanager/saveuploadeddocs";

/* ADMIN ROUTES */
$route['admin/users']     			    = "admin/userslist";

$route['404_override']          		= '';

/* End of file routes.php */
/* Location: ./application/config/routes.php */