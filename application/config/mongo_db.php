<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Generally will be localhost if you're querying from the machine that Mongo is installed on
/*$config['mongo_host'][] = "localhost:27017";
//$config['mongo_host'][] = "localhost:27018";
//$config['mongo_host'][] = "localhost:27019";

// Generally will be 27017 unless you've configured Mongo otherwise
//$config['mongo_port'] = 27017;

// The database you want to work from (required)
$config['mongo_db'] = "dms";

// Leave blank if Mongo is not running in auth mode
$config['mongo_user'] = "";
$config['mongo_pass'] = "";

// Persistant connections
$config['mongo_persist'] = TRUE;
$config['mongo_persist_key'] = 'ci_mongo_persist';

// Get results as an object instead of an array
$config['mongo_return'] = 'object'; // Set to object

// When you run an insert/update/delete how sure do you want to be that the database has received the query?
// safe = the database has receieved and executed the query
// fysnc = as above + the change has been committed to harddisk <- NOTE: will introduce a performance penalty
$config['mongo_query_safety'] = 'safe';

// Supress connection error password display
$config['mongo_supress_connect_error'] = TRUE;

// If you are having problems connecting try changing this to TRUE
$config['host_db_flag'] = FALSE;*/

// Replica set
//$config['replicaSet'] = "dmsReplicaSet";

$config['mongo_db']['active'] = 'default';
$config['mongo_db']['default']['no_auth'] = FALSE;
$config['mongo_db']['default']['hostname'] = 'localhost';
$config['mongo_db']['default']['port'] = '27017';
$config['mongo_db']['default']['username'] = 'intdmsadmin';
$config['mongo_db']['default']['password'] = 'D4e78htfb3d';
$config['mongo_db']['default']['database'] = 'dms';
$config['mongo_db']['default']['db_debug'] = TRUE;
$config['mongo_db']['default']['return_as'] = 'object';
$config['mongo_db']['default']['write_concerns'] = (int)1;
$config['mongo_db']['default']['journal'] = TRUE;
$config['mongo_db']['default']['read_preference'] = NULL;
$config['mongo_db']['default']['read_preference_tags'] = NULL;

