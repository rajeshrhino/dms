<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Saving Sets Menus to be displayed in website
|--------------------------------------------------------------------------
|
*/

$config['menus']['website'] = array(
                                    'home'     => array(
                                                                'label'     => 'HOME',
                                                                'key'       => 'home',
                                                                'link_to'   => 'home',
                                                            ),
                                    /*'blog'     => array(
                                                                'label'     => 'BLOG',
                                                                'key'       => 'blog',
                                                                'link_to'   => 'blog',
                                                            ),*/
                                    'about'     => array(
                                                                'label'     => 'ABOUT',
                                                                'key'       => 'about',
                                                                'link_to'   => 'about',
                                                                'child' => array(
                                                                                    'team'     => array(
                                                                                                        'label'     => 'The Team',
                                                                                                        'key'       => 'team',
                                                                                                        'link_to'   => 'team',
                                                                                                    ),      
                                                                                    /*'company'     => array(
                                                                                                        'label'     => 'Company',
                                                                                                        'key'       => 'company',
                                                                                                        'link_to'   => 'company',
                                                                                                    ),*/
                                                                                ),
                                                            ),
									'features'     => array(
                                                                'label'     => 'FEATURES',
                                                                'key'       => 'features',
                                                                'link_to'   => 'features',
                                                            ),						
                                    'register'     => array(
                                                                'label'     => 'PRICING',
                                                                'key'       => 'pricing',
                                                                'link_to'   => 'pricing',
                                                            ),                                                  
                                    'contact'     => array(
                                                                'label'     => 'CONTACT US',
                                                                'key'       => 'contact',
                                                                'link_to'   => 'contact',
                                                            ),  
									'dashboard'     => array(
                                                                'label'     => 'MY DASHBOARD',
                                                                'key'       => 'dashboard',
                                                                'link_to'   => 'edocs',
                                                            ),
									'login'     => array(
                                                                'label'     => 'LOGIN',
                                                                'key'       => 'login',
                                                                'link_to'   => 'login',
                                                            ),
                                    );

$config['menus']['website_footer'] = array(
                                    'terms'     => array(
                                                                'label'     => 'Terms',
                                                                'key'       => 'terms',
                                                                'link_to'   => 'terms',
                                                            ),
                                    'privacy'     => array(
                                                                'label'     => 'Privacy',
                                                                'key'       => 'privacy',
                                                                'link_to'   => 'privacy',
                                                            ),
                                    'security'     => array(
                                                                'label'     => 'Security',
                                                                'key'       => 'security',
                                                                'link_to'   => 'security',
                                                            ),
                                    'cookiepolicy'     => array(
                                                                'label'     => 'Cookie Policy',
                                                                'key'       => 'cookiepolicy',
                                                                'link_to'   => 'cookiepolicy',
                                                            ),
									'credits'     => array(
                                                                'label'     => 'Credits',
                                                                'key'       => 'credits',
                                                                'link_to'   => 'credits',
                                                            ),  						
                                    );

$config['menus']['backend'] = array(
                            'Main'      => array ('roles'=>array(1, 2, 3),
                                                '0'      => array( 'label' => 'Dashboard' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-home', 'roles'=>array(1, 2, 3)),
												'1'      => array( 'label' => 'File Manager' , 'key' => 'dashboard' , 'link_to' => 'document/0/list' , 'class' => 'icon-folder-open',  'roles'=>array(1, 2, 3)),
                                                //'1'      => array( 'label' => 'UI Features' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-eye-open'),
                                                //'2'      => array( 'label' => 'Forms' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-edit'),
                                                //'3'      => array( 'label' => 'Charts' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-list-alt'),
                                                //'4'      => array( 'label' => 'Typography' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-font'),
                                                //'5'      => array( 'label' => 'Gallery' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-picture'),
                                                ),
                            'Admin'    => array ('roles'=>array(1),
												'0'      => array( 'label' => 'Dashboard' , 'key' => 'admindashboard' , 'link_to' => 'admin/admindashboard' , 'class' => 'icon-home' , 'roles'=>array(1)),
											),
							'Corporate'    => array ('roles'=>array(2),
												'0'      => array( 'label' => 'Users' , 'key' => 'corporateuserslist' , 'link_to' => 'corporate/corporateuserslist' , 'class' => 'icon-home' , 'roles'=>array(2)),
											),							
							'Users'    => array ('roles'=>array(1),
												'0'      => array( 'label' => 'Individual' , 'key' => 'userslist' , 'link_to' => 'admin/userslist' , 'class' => 'icon-user' , 'roles'=>array(1)),
												'1'      => array( 'label' => 'Corporate' , 'key' => 'companies' , 'link_to' => 'admin/companies' , 'class' => 'icon-flag' , 'roles'=>array(1)),														
                                                //'1'      => array( 'label' => 'Calendar' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-calendar'),
                                                //'2'      => array( 'label' => 'Grid' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-th'),
                                                //'3'      => array( 'label' => 'edocs Manager' , 'key' => 'dashboard' , 'link_to' => 'edocs/filemanager' , 'class' => 'icon-folder-open'),
                                                //'4'      => array( 'label' => 'Tour' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-globe'),
                                                //'5'      => array( 'label' => 'Icons' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-star'),
                                                //'6'      => array( 'label' => 'Error Page' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-ban-circle'),
                                                //'7'      => array( 'label' => 'Login Page' , 'key' => 'dashboard' , 'link_to' => 'edocs/dashboard/home' , 'class' => 'icon-lock'),
                                                ),
							'Configuration' => array ('roles'=>array(1),
												'0'      => array( 'label' => 'Site Settings' , 'key' => 'sitesettings' , 'link_to' => 'admin/sitesettings' , 'class' => 'icon-cog' , 'roles'=>array(1)),
												'1'      => array( 'label' => 'Invitation Codes' , 'key' => 'invitecode' , 'link_to' => 'admin/invitecode' , 'class' => 'icon-tag' , 'roles'=>array(1)),
												'2'      => array( 'label' => 'Maintenance Mode' , 'key' => 'sitemaintenance' , 'link_to' => 'admin/sitemaintenance' , 'class' => 'icon-wrench' , 'roles'=>array(1)),
											),
							'Others' => array ('roles'=>array(1),
												'0'      => array( 'label' => 'Go to Website' , 'key' => 'home' , 'link_to' => 'home' , 'class' => 'icon-globe' , 'roles'=>array(1)),
											),
							'My Account' => array ('roles'=>array(1 , 2 , 3),
												'0'      => array( 'label' => 'Account Settings' , 'key' => 'accountsettings' , 'link_to' => 'accountsettings' , 'class' => 'icon-cog' , 'roles'=>array(1 , 2, 3)),
												'2'      => array( 'label' => 'Logout' , 'key' => 'logout' , 'link_to' => 'logout' , 'class' => 'icon-exclamation-sign' , 'roles'=>array(1 , 2, 3)),
											),				
                                    );
                                    
/*$config['menus']['backend'] = array(
										'dashboard'     => array(
                                                                'label'     => 'Dashboard',
                                                                'key'       => 'dashboard',
                                                                'link_to'   => 'dashboard/home',
                                                                'child' => array(
                                                                                    '1'     => array(
                                                                                                        'label'     => 'Home',
                                                                                                        'key'       => 'home',
                                                                                                        'link_to'   => 'dashboard/home',
                                                                                                    ),      
                                                                                ),
                                                            ),
                                    'documents'     => array(
                                                                'label'     => 'Documents',
                                                                'key'       => 'documents',
                                                                'link_to'   => 'documents/view-all',
                                                                'child' => array(
                                                                                    '1'     => array(
                                                                                                        'label'     => 'View All',
                                                                                                        'key'       => 'view-all',
                                                                                                        'link_to'   => 'documents/view-all',
                                                                                                    ),      
                                                                                    '2'     => array(
                                                                                                        'label'     => 'Add/Upload',
                                                                                                        'key'       => 'upload',
                                                                                                        'link_to'   => 'documents/upload',
                                                                                                    ),                                                                                                        
                                                                                    '3'     => array(
                                                                                                        'label'     => 'Share',
                                                                                                        'key'       => 'share',
                                                                                                        'link_to'   => 'documents/share',
                                                                                                    ),                
                                                                                ),
                                                            ),                                                                    
                                    'search'     => array(
                                                                'label'     => 'Search',
                                                                'key'       => 'search',
                                                                'link_to'   => 'search-basic',
                                                                'child' => array(
                                                                                    '1'     => array(
                                                                                                        'label'     => 'Basic',
                                                                                                        'key'       => 'search-basic',
                                                                                                        'link_to'   => 'search-basic',
                                                                                                    ),      
                                                                                    '2'     => array(
                                                                                                        'label'     => 'Advanced',
                                                                                                        'key'       => 'search-advanced',
                                                                                                        'link_to'   => 'search-advanced',
                                                                                                    ),
                                                                                ),
                                                            ),
                                    
                                    'invite'     => array(
                                                                'label'     => 'Invite',
                                                                'key'       => 'invite',
                                                                'link_to'   => 'invite',
                                                                'child' => array(
                                                                                    '1'     => array(
                                                                                                        'label'     => 'Invite',
                                                                                                        'key'       => 'invite',
                                                                                                        'link_to'   => 'invite',
                                                                                                    ),      
                                                                                ),
                                                            ), 
                                    );*/                                    

/* End of file menus.php */
/* Location: ./application/config/menus.php */


