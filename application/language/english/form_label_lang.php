<?php

$lang['label_home']       						=     "Home";
$lang['label_dashboard']       					=     "Dashboard";
$lang['label_admin_dashboard']       			=     "Admin Dashboard";
$lang['label_introduction']       				=     "Introduction";
$lang['label_welcome']       					=     "Welcome back";
$lang['label_welcome_to_int_dms']       		=     "Welcome to {_int} DMS";

$lang['label_last_login']       				=     "Last Login";
$lang['label_ip_address']       				=     "IP Address";

$lang['label_help']       						=     "Help";
$lang['label_login_page_title']       			=     "Login";
$lang['label_one_time_login_page_title']       	=     "Confirm/Reset Password";
$lang['label_forgot_password']       			=     "Forgot Password?";
$lang['label_forgot_password_title']       		=     "Forgot Password";
$lang['label_forgot_password_description'] 		=     "To reset your password, enter the email address associated with your {_int} DMS account. <br /><br />We will send an email to you with a link to reset to password.";
$lang['label_login_form_title']       			=     "Login Form";
$lang['label_username']	              			=     "Username";
$lang['label_username_title']	      			=     "Enter your username";
$lang['label_passcurr']	              			=     "Current Password";
$lang['label_passcurr_title']	      			=     "Enter your current password";
$lang['label_password']	              			=     "Password";
$lang['label_password_title']	      			=     "Enter your password";
$lang['label_passconf']	              			=     "Confirm Password";
$lang['label_passconf_title']	      			=     "Enter your password again";

$lang['label_account_password_help']	        =     "Provide a password for the new account in both fields.";
$lang['label_account_email_help']	        	=     "A valid e-mail address. All e-mails from the system will be sent to this address. The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail.";
$lang['label_account_username_help']	        =     "Letters and numbers are allowed; punctuation is not allowed except for underscores.";

$lang['label_pricing_page_title']    			=     "Pricing & Features";
$lang['label_terms_page_title']    				=     "Terms of Service";
$lang['label_privacy_page_title']    			=     "Privacy";
$lang['label_security_page_title']    			=     "Security";
$lang['label_cookiepolicy_page_title']    		=     "Cookie Policy";
$lang['label_your_cookiepolicy_page_title']    	=     "Your Cookie Policy";
$lang['label_credits_page_title']    			=     "Credits and Proprietary Technologies";
$lang['label_alphawarning_page_title']    		=     "Alpha Software Warning";

$lang['label_contact_form_title']    			=     "Contact form";

$lang['label_register_page_title']    			=     "Registration";
$lang['label_register_individual_form_title']   =     "<span `lass='green-text'>Individual Package</span>";
$lang['label_register_corporate_form_title']    =     "<span class='green-text'>Corporate Package</span>";
$lang['label_registration_invite_code_title'] 	=     "Invitation Code";
$lang['label_registration_step_1_title'] 	    =     "Account Settings";
$lang['label_registration_step_2_title'] 	    =     "Personal Details";
$lang['label_registration_step_3_title'] 	    =     "Company Details";
$lang['label_registration_step_4_title'] 	    =     "Confirmation";
$lang['label_registration_step_1_description'] 	=     "";
$lang['label_registration_step_2_description'] 	=     "";
$lang['label_registration_step_3_description'] 	=     "";
$lang['label_registration_step_4_description'] 	=     "";
$lang['label_package_name']	          			=     "Account Type";
$lang['label_package_name_title']	  			=     "Select your account type";
$lang['label_language_name']	          		=     "Language";
$lang['label_language_name_title']	  			=     "Select your preferred language";
$lang['label_language_name_help_text']	  		=     "Choose the preferred language. This setting determines the language in which the application is displayed.";
$lang['label_invite_code_name']	          		=     "Invitation Code";
$lang['label_invite_code_title']	  			=     "Enter the invitation code";
$lang['label_invite_code_help_text']	  		=     "Please enter the invitation code which was provided by site administrator or member to finish the registration.";
$lang['label_timezone_name']	          		=     "Timezone";
$lang['label_timezone_name_title']	  			=     "Select your timezone";
$lang['label_timezone_name_help_text']	  		=     "Choose your time zone. This setting determines the date/time used in the application.";
$lang['label_timezone_name_help_text1']	  		=     "Choose the time zone. This setting determines the date/time used in the application.";
$lang['label_first_name']	          			=     "First Name";
$lang['label_first_name_title']	      			=     "What's your first name?";
$lang['label_last_name']	          			=     "Last Name";
$lang['label_last_name_title']	      			=     "What's your last name?";
$lang['label_email']	          				=     "Email Address";
$lang['label_check_availability']	          	=     "Check Availability";
$lang['label_email_title']	      				=     "Valid E-mail please, you will need it to verify your account!";
$lang['label_forgot_password_email_title']	    =     "Enter the email address you used to register.";
$lang['label_register_username_title']	      	=     "Choose your username: letters, numbers and '_'";
$lang['label_register_password_title']	      	=     "At least 5 characters: letters, numbers and '_'";
$lang['label_register_retype_password_title']	=     "Retype Password";
$lang['label_company_name']	          			=     "Company Name";
$lang['label_address']	          				=     "Address";
$lang['label_company_name_title']	      		=     "What's your company name?";
$lang['label_company_street_address']	        =     "Street Address";
$lang['label_company_street_address_title']	   	=     "What's your company's street address?";
$lang['label_company_city']	        			=     "City";
$lang['label_company_city_title']	      		=     "Which city your company is based in?";
$lang['label_company_postcode']	        		=     "Postal Code";
$lang['label_company_postcode_title']	     	=     "What's your company's postal code?";
$lang['label_terms_and_condition']	            =     "<strong>I agree to the Google <a target='_blank' href='terms'>Terms of Service</a> and <a target='_blank' href='privacy'>Privacy Policy</a></strong>";
$lang['label_submit']	              			=     "Submit";

$lang['label_invite_codes']	          			=     "Invitation Codes";
$lang['label_invite_codes_description']	        =     "Invitation Codes to be used during registration process, if <b>Invitation code</b> field is set mandatory for registration in <b>Site Settings</b>.";
$lang['label_is_active']	          			=     "Is Active?";

// Registration disabled labels & message
$lang['label_register_disabled_page_title'] 	=     "Registration disabled";
$lang['label_register_disabled_sub_title']	    =     "Sorry, Registration has been disabled by the administrator.";
$lang['label_register_disabled_description'] 	=     "<p>We aplogise for any inconvenience caused.</p><p>Please get in touch using the <a href='contact'>Contact form</a> if you have any queries.</p>";

// Website maintenance page labels & messages
$lang['label_maintenance_page_title'] 			=     "Site under maintenance";
$lang['label_maintenance_page_description'] 	=     "<p>{_int} DMS website is currently under maintenance. We should be back shortly. Thank you for your patience.</p><p>Administrators can <a href='login'>click here</a> to login and perform maintenance.</p>";
$lang['label_maintenance_mode_message'] 		=     "Operating in maintenance mode. <a href='admin/sitemaintenance' style='cursor: pointer;'>Go online.</a>";

// Website maintenance - Admin form labels & messages
$lang['label_maintenance_form_title'] 			=     "Maintenance mode";
$lang['label_maintenance_mode'] 				=     "Put site into maintenance mode";
$lang['label_maintenance_form_description'] 	=     "When enabled, only users with the 'Super Admin' role will be able to access your site to perform maintenance. All other visitors see the maintenance mode message.";

$lang['label_new_user']	              			=     "New user?";
$lang['label_need_help']	              		=     "Having trouble signing in?";
$lang['label_number_of_user']	              	=     "Users Count";
$lang['label_list_of_user']	              		=     "Users List";
$lang['label_list']	              				=     "List";
$lang['label_new_user_description']	            =     "By creating an account at Int DMS you will be able to upload, view and share documents more efficiently and securely.";
$lang['label_new_user_link']	              	=     "Get Started";

$lang['label_signup_for_trial']	              	=     "Sign up for a free 30 day trial. <span class='green-text'>No credit card required.</span>";
$lang['label_why_register']	              		=     "Why register?";
$lang['label_why_register_point_1']	            =     "Manage your documents in one place";
$lang['label_why_register_point_2']	            =     "Upload and view documents whenever/whereever you are";
$lang['label_why_register_point_3']	            =     "No need to duplicate/send your documents and lose the changes made";
$lang['label_why_register_point_4']	            =     "Collabrate with your team by sharing documents";
$lang['label_why_register_point_5']	            =     "Share documents securely with friends";
$lang['label_why_register_point_6']	            =     "Download original document";

$lang['label_after_trial_title']	            =     "What happens after the trial?";
$lang['label_after_trial_text']	                =     "You can upgrade your trial to a paid account at any point. Upgrading takes minutes. Near the end of the trial we will contact you to see how things are going. If you upgrade, all your data and documents will be saved.";

$lang['label_new_folder']	            		=     "New Folder";
$lang['label_folder']	            			=     "Folder";
$lang['label_edit_folder']	            		=     "Edit Folder";
$lang['label_delete_folder']	            	=     "Delete Folder";
$lang['label_confirm_delete']	            	=     "Confirm Delete";
$lang['label_save']	            				=     "Save";
$lang['label_yes']	            				=     "Yes";
$lang['label_close']	            			=     "Close";
$lang['label_cancel']	            			=     "Cancel";

$lang['label_folder_name']	            		=     "Name";
$lang['label_folder_title']	            		=     "Title";
$lang['label_folder_description']          		=     "Description";
$lang['label_folder_name_title']	            =     "Please enter folder name";
$lang['label_folder_name_help_text']	        =     "Please enter folder name";

$lang['label_edit']	            				=     "Edit";
$lang['label_delete']	            			=     "Delete";
$lang['label_done']	            				=     "Done";

$lang['label_files']	            			=     "Files";
$lang['label_file_info']	            		=     "File Info";
$lang['label_file_name']	            		=     "File Name";
$lang['label_name']	            				=     "Name";
$lang['label_type']	            				=     "Type";
$lang['label_size']	            				=     "Size";
$lang['label_uploaded_date']	            	=     "Uploaded Date";
$lang['label_actions']	            			=     "Action";

$lang['label_file_manager']           			=     "File Manager";
$lang['label_file_upload']           			=     "File Upload";
$lang['label_upload']           				=     "Upload";
$lang['label_file_upload_finished']           	=     "Please click here once you are done with the upload.";

$lang['label_users']       						=     "Users";
$lang['label_user_add']       					=     "Add User";
$lang['label_total_users']       				=     "Total User";
$lang['label_pro_users']       					=     "Pro Members";
$lang['label_sales']       						=     "Sales";
$lang['label_messages']       					=     "Messages";
$lang['label_main']       						=     "Main";
$lang['label_admin']       						=     "Admin";

$lang['label_on']       						=     "on";
$lang['label_created_by']       				=     "Create by";
$lang['label_date_created']       				=     "Date Created";

$lang['label_download']       					=     "Download";
$lang['label_delete']       					=     "Delete";
$lang['label_copy_to']       					=     "Copy to";
$lang['label_move_to']       					=     "Move to";
$lang['label_selected_items']       			=     "Selected Items";

$lang['label_roles']       						=     "Roles";
$lang['label_account_settings']       			=     "Account settings";
$lang['label_logout']       					=     "Logout";
$lang['label_change_password']       			=     "Change Password";
$lang['label_reset_password']       			=     "Reset Password";

// Account settings form - labels and messages
$lang['label_change_password_help']       		=     "To change the current user password, enter the new password in both fields.";
$lang['label_your_details']       				=     "Your Details";

$lang['label_select']	            			=     "Select";
$lang['label_folders']	            			=     "Folders";
$lang['label_documents']	            		=     "Documents";
$lang['label_all']	            				=     "All";
$lang['label_none']	            				=     "None";

$lang['label_companies']	            		=     "Companies";
$lang['label_company_name']	            		=     "Company Name";
$lang['label_company_add']	            		=     "Add Company";
$lang['label_company_edit']	            		=     "Edit Company";

$lang['label_invite_code_add']	            	=     "Add Invitation Code";
$lang['label_invite_code_edit']	            	=     "Edit Invitation Code";

// Backend - Site settings labels and messages
$lang['label_site_settings']	            	=     "Site Settings";
$lang['label_site_email_settings']	            =     "Site E-mail Settings";
$lang['label_users_can_register']	           	=     "Users can register?";
$lang['label_users_can_register_help']	        =     "Allows the user to register online for individual/corporate packages, if ticked.";
$lang['label_invite_code_mandatory']	        =     "Is the invite code mandatory?";
$lang['label_invite_code_mandatory_help']	    =     "Is the invitation code mandatory to complete the registration? Invite code field will be made mandatory, if ticked";
$lang['label_memcache_enabled']	            	=     "Memcache enabled?";
$lang['label_memcache_enabled_help']	        =     "Enables the use of memcache server to stores the data objects. <br /><span class='label label-important'>IMPORTANT</span> Please enable this option only if the memcache server is installed and running and necessary settings are configured in /system/application/config/memcached.php";
$lang['label_global_from_email_address']	    =     "From E-mail Address";
$lang['label_global_from_email_address_help']	=     "The From address in automated e-mails sent during registration and new password requests, and other notifications. (Use an address ending in your site's domain to help prevent this e-mail being flagged as spam.)";
$lang['label_global_from_email_name']	    	=     "From E-mail Name";
$lang['label_global_from_email_name_help']		=     "The From name in automated e-mails sent during registration and new password requests, and other notifications.";
$lang['label_contact_us_receiving_email_address']	    =     "Contact E-mail Addresses";
$lang['label_contact_us_receiving_email_address_help']	=     "All the website Contact Us form submission will be sent to these email addresses. To specify multiple recipients, separate each e-mail address with a comma.<br />Example: 'webmaster@example.com' or 'sales@example.com,support@example.com'.";


$lang['label_message_templates']	            =     "Message Templates";
$lang['label_message_template']	    			=     "Message Template";
$lang['label_message_template_add']	    		=     "New Message Template";
$lang['label_message_title']	            	=     "Message Title";
$lang['label_message_subject']	            	=     "Message Subject";
$lang['label_message_text_format']	            =     "Plain text format";
$lang['label_message_html_format']	            =     "HTML format";

$lang['label_message_title_help']	            =     "Descriptive title of message.";
$lang['label_message_subject_help']	            =     "Subject for email message.";
$lang['label_message_text_format_help']	        =     "Text formatted message.";
$lang['label_message_html_format_help']	        =     "You may optionally create an HTML formatted version of this message.";

$lang['label_registered']	        			=     "Registered";

$lang['label_edit_user']	            		=     "Edit User";
$lang['label_delete_user']	            		=     "Delete User";
$lang['label_warning']	            			=     "Warning!";
$lang['label_select']	            			=     "Select";

$lang['label_individual_add']	            	=     "Add Individual";
$lang['label_corporate_add']	            	=     "Add Corporate";

$lang['label_upload_to']	            		=     "Upload files to";

$lang['label_in']	            				=     "in";

$lang['label_account_status']	            	=     "Account Status";
$lang['label_account_status_title']	            =     "Account Status";

$lang['label_password_current_help']	        =     "For security reasons, please enter your current password to change any account information.";

$lang['label_full_name']                        =     "Name";
$lang['label_full_name_title']                  =     "Enter your full name";

$lang['label_full_email_title']                 =     "Enter your email address";

$lang['label_comments']                         =     "Comments";
$lang['label_comments_title']                   =     "Enter your message/comments";

$lang['label_send']                             =     "Send";

/* End of file form_label_lang.php */
/* Location: ./system/language/english/form_label_lang.php */