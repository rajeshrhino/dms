<?php

$lang['message_registration_success']		= "Registration successful. Please check your email to activate your account and for further instructions.";
$lang['message_registration_failed']		= "For some reason, your attempt to register has failed. Please try again later or contact the website administrator.";
$lang['message_account_locked']	        	= "Your account has been locked for security reasons. Please contact site administrator.";
$lang['message_account_pending']	    	= "Your account activation is pending. Please check your account confirmation email.";
$lang['message_account_suspended']	    	= "Your account has been suspended. Please contact site administrator.";
$lang['message_account_closed']	        	= "Your account has been closed.";
$lang['message_invalid_login']	        	= "Invalid login credentails. Please try again.";
$lang['message_account_not_exists']	    	= "Account does not exist. Please check your username and try again.";

$lang['message_confirmation_success']		= "Congratulations! Your account has been verified. A welcome email has been sent to your email account.";
$lang['message_account_already_activated']	= "Your account is already activated. Please login to use the system.";

$lang['file_too_large']						= "File too large or empty. Maximum Upload Size is ";
$lang['megabytes']							= " megabytes";
$lang['file_type_invalid']					= "Invalid file type. Only DOC , XLS , TXT , CSV , PPT , PDF, JPG, GIF and PNG types are accepted.";

$lang['message_folder_empty']				= "No file available in this folder.";
$lang['message_login_to_continue']			= "Please login to continue.";
$lang['message_access_denied']				= "Access Denied";

$lang['message_delete_confirm']				= "Are you sure you want to delete the file?";
$lang['message_delete_confirm_multiple']	= "Are you sure you want to delete the select files/folders?";
$lang['message_delete_user_confirm']		= "Deleting this user will result in lose of all data including any folders and files created/owned by the user.<br /><br />Are you sure you want to delete the user?<br /><br />This action is irreversible.";
$lang['message_delete_folder_confirm']		= "Are you sure you want to delete the folder?";
$lang['message_delete_company_confirm']		= "Are you sure you want to delete the company?";
$lang['message_delete_invitecode_confirm']	= "Are you sure you want to delete the Invitation Code?";

$lang['message_delete_file_confirmation']	= "File deleted.";
$lang['message_cannot_delete_file']			= "Cannot delete file.";
$lang['selected_delete_confirmation']		= "Selected files/folders delete.";

$lang['settings_saved_confirmation']		= "The configuration options have been saved.";

$lang['message_folder_create_confirmation']	= "Folder created.";
$lang['message_folder_edit_confirmation']	= "Folder updated.";

$lang['message_enter_mandatory_fields']		= "Please enter all mandatory fields.";

$lang['message_company_saved']				= "Company details saved.";

$lang['message_invite_code_saved']			= "Invitation Code saved.";

$lang['message_contact_us_form']			= "If you have any questions or comments, please use the form below and we will get back to you as soon as possible.";
$lang['message_contact_us_form_alt_msg']    = "Alternatively you can contact us using the below details.";
$lang['message_contact_us_form_thank_you']  = "Thank you for contact us. We will get back to you as soon as possible.";

$lang['message_password_set_form']			= "You have just used your one-time login link. It is no longer necessary to use this link to log in. Please change your password.";

$lang['message_acct_settings_updated']		= "Account settings updated successful.";
$lang['message_current_pass_incorrect']		= "Current password is incorrect.";
$lang['message_acct_settings_update_fail']	= "For some reason, your attempt to update account settings has failed. Please try again later or contact the administrator.";

$lang['message_forgot_password_msg']		= "Further instructions have been sent to your e-mail address.";

$lang['one_time_login_link_expired']		= "You have tried to use a one-time login link that has expired. Please request a new one using the form below.";

$lang['one_time_login_link_used']			= "You have tried to use a one-time login link that has either been used or is no longer valid. Please request a new one using the form below.";

/* End of file message_lang.php */
/* Location: ./system/language/english/message_lang.php */