<?php

$lang['error_username_exists']	    = "Username is already taken.";
$lang['error_email_exists']	        = "Email Address is already registered. <br /><a href=\"#\">Forgot your password?</a>";
$lang['error_invite_code_invalid']	= "Invitation Code you have entered in invalid or expired. Please try again.";
$lang['error_email_exists_without_link'] = "Email Address is already registered.";
$lang['error_login_required']	    = "Please enter your %s";
$lang['error_register_required']    = "%s is required field";
$lang['min_length_required']        = "The value you have entered for %s is too short";
$lang['password_match_required']    = "The value you have entered doesnt match with confirm password";
$lang['alpha_dash_required']        = "Please use only letters, number and _ for %s";
$lang['error_company_name_exists']	= "Seems your company is already registered. <br /><a href=\"#\">Click here</a> to contact your company account administrator.";
$lang['valid_email_required']       = "Invalid email address";

$lang['error_invite_code_exists']	= "Invitation Code already exists. Please try a different code.";

$lang['folder_name_mandatory']		= "Please enter folder name.";
$lang['folder_name_exists']			= "Folder name already exist.";
$lang['cannot_delete_folder']		= "Folder cannot be deleted, as there are files/folders inside the folder.";
$lang['cannot_delete_folders']		= "folder(s) cannot be deleted, as there are files/folders inside the folder.";

$lang['company_name_exists']		= "Company name already exist.";

$lang['company_name_exists']		= "Company name already exist.";

$lang['cannot_delete_main_admin']	= "You wont be able to delete this admin user.";

/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */