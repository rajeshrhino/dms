<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li class="active"><?php echo lang('label_account_settings');?></li>
	</ul>
</div>

<?php if (isset($error_message) && ($error_message != '')): ?>  
<!--<div class="alert alert-error">
	<button data-dismiss="alert" class="close" type="button">×</button>
	<?php echo $error_message;?>
</div>-->
<a href='#' style="display:none;" id="error_message_link" data-noty-options='{"text":"<?php echo $error_message;?>", "layout":"topCenter","type":"error"}' class="btn btn-primary noty"></a>
<?php elseif (isset($message) && ($message != '')): ?>  
<!--<div class="alert alert-success">
	<button data-dismiss="alert" class="close" type="button">×</button>
	<?php echo $message;?>
</div>-->
<a href='#' style="display:none;" id="success_message_link" data-noty-options='{"text":"<?php echo $message;?>","layout":"topCenter","type":"success"}' class="btn btn-primary noty"></a>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-cog"></i> <?php echo lang('label_account_settings');?></h2>
			
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
			
		</div>
		
		<div class="box-content">

					<?php echo form_open('accountsettings', array('id' => 'accountsettings' , 'class' => 'form-horizontal'));?>
					
					<fieldset>
					<legend><?php echo lang('label_your_details');?></legend>
					
					<!--<p> Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget.</p>-->
					<?php if (form_error('username')): ?>
						<div class="control-group error">
					<?php else: ?>
						<div class="control-group">
					<?php endif; ?>
						<label class="control-label"><?php echo lang('label_username');?><span class="form-required" title="This field is required.">*</span></label>
						<div class="controls">
						  <div class="input-prepend">
							<?php echo $username; ?>
						  </div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label"><?php echo lang('label_first_name');?></label>
						<div class="controls">
						  <div class="input-prepend">
							<?php echo form_input(array('name' => 'first_name', 'id' => 'first_name' , 'value' => set_value('first_name' , $first_name)  , 'title' => lang('label_first_name_title')));?>
							<span class="help-inline"><?php echo form_error('first_name'); ?></span>
						  </div>
						  <!--<p class="help-block"><?php echo lang('label_message_title_help');?></p>-->
						</div>
					</div>
					<div class="control-group">
						<label class="control-label"><?php echo lang('label_last_name');?></label>
						<div class="controls">
						  <div class="input-prepend">
							<?php echo form_input(array('name' => 'last_name', 'id' => 'last_name' , 'value' => set_value('last_name' , $last_name)  , 'title' => lang('label_last_name_title')));?>
							<span class="help-inline"><?php echo form_error('last_name'); ?></span>
						  </div>
						</div>
					</div>
					<?php if (form_error('email')): ?>
						<div class="control-group error">
					<?php else: ?>
						<div class="control-group">
					<?php endif; ?>
						<label class="control-label"><?php echo lang('label_email');?><span class="form-required" title="This field is required.">*</span></label>
						<div class="controls">
						  <div class="input-prepend">
							<?php echo form_input(array('name' => 'email', 'id' => 'email' , 'value' => set_value('email' , $email)  , 'title' => lang('label_email_title')));?>
							<span class="help-inline"><?php echo form_error('email'); ?></span>
						  </div>
						</div>
					</div>
					<?php if (form_error('passcurr')): ?>
						<div class="control-group error">
					<?php else: ?>
						<div class="control-group">
					<?php endif; ?>
						<label class="control-label"><?php echo lang('label_passcurr');?>&nbsp;<span class="form-required" title="This field is required.">*</span></label>
						<div class="controls">
						  <div class="input-prepend">
							<?php echo form_password(array('name' => 'passcurr', 'id' => 'passcurr' , 'title' => lang('label_passcurr_title')));?>
							<span class="help-inline"><?php if(!empty($passcurr_error)) echo $passcurr_error; ?><?php echo form_error('passcurr'); ?></span>
							<br />
							<p class="help-block"><?php echo lang('label_password_current_help');?></p>
						  </div>
						</div>
					</div>
					
					</fieldset>
					
					<fieldset>
					<legend><?php echo lang('label_change_password');?></legend>
					
					<?php if (form_error('password')): ?>
						<div class="control-group error">
					<?php else: ?>
						<div class="control-group">
					<?php endif; ?>
						<label class="control-label"><?php echo lang('label_password');?></label>
						<div class="controls">
						  <div class="input-prepend">
							<?php echo form_password(array('name' => 'password', 'id' => 'password' , 'title' => lang('label_password_title')));?>
							<span class="help-inline"><?php echo form_error('password'); ?></span>
						  </div>
						</div>
					</div>
					<?php if (form_error('passconf')): ?>
						<div class="control-group error">
					<?php else: ?>
						<div class="control-group">
					<?php endif; ?>
						<label class="control-label"><?php echo lang('label_passconf');?></label>
						<div class="controls">
						  <div class="input-prepend">
							<?php echo form_password(array('name' => 'passconf', 'id' => 'passconf' , 'title' => lang('label_passconf_title')));?>
							<span class="help-inline"><?php echo form_error('passconf'); ?></span>
							<p class="help-block"><?php echo lang('label_change_password_help');?></p>
						  </div>
						</div>
					</div>
					
					</fieldset>
					
					<div class="form-actions">
						<?php echo form_submit(array('name'=>'submit', 'value'=>'Update', 'class' => 'btn btn-primary' , 'id' => 'submit'))?>
					</div>

				<?php echo form_close(); ?>
						
		</div>
	</div><!--/span-->

</div><!--/row-->

		<!-- content ends -->
</div><!--/#content.span10-->
