<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

    //the min chars for username
    var min_chars = 3;
    
    //result texts
    var characters_error = 'Please type atleast 3 characters to check availability';
    var checking_html = '<img src="<?php echo base_url();?>misc/website/img/loading.gif">&nbsp;Checking...';
    
    //when button is clicked
    $('#check_username_availability').click(function(){
    	//run the character number check
    	if($('#username').val().length < min_chars){
    		//if it's bellow the minimum show characters_error text '
    		$('#username_availability_result').html(characters_error);
    	}else{
    		//else show the cheking_text and run the function to check
    		$('#username_availability_result').html(checking_html);
    		check_availability();
    	}
    });
});

//function to check username availability
function check_availability(){

    //get the username
    var username = $('#username').val();
    $('#username_availability_result').removeClass("success-message");
    $('#username_availability_result').removeClass("error-message");
    
    //use ajax to run the check
    $.ajax({url: "<?php echo site_url("user/register/check")?>", data : { username: username },
        type: "POST",
    	success: function(result){
    		//if the result is 1
    		if(result == 1){
    			//show that the username is available
                $('#username_availability_result').addClass("success-message");
    			$('#username_availability_result').html(username + ' is available');
    		}else{
    			//show that the username is NOT available
                $('#username_availability_result').addClass("error-message");
    			$('#username_availability_result').html(username + ' is already taken');
    		}
    }});

}   
</script>
 
<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_register_page_title');?></h1><span class="subheading"></span>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
		<!-- page content -->
      	<div id="page-content" class="clearfix">
           
			<!-- 2 cols -->
			<div class="full-page-block">
				<h4 class="heading"><?php echo lang('label_register_corporate_form_title');?></h4>
                
                <center><div id="message" class="error-message"><?php if ((isset($message)) && ($message !== '')) echo $message;?></div></center>
                
				<!-- form -->
				<?php echo form_open('registercompany' , array('id' => 'loginForm', 'class' => 'niceForm'));?>
					<!--<p> Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget.</p>-->
								
								<?php if ($invite_code_mandatory == 1): ?>
								<fieldset>
                                <h6 class="StepTitle"><?php echo lang('label_registration_invite_code_title');?></h6>
                                <p><?php echo lang('label_registration_invite_code_description');?></p>
                                <div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
								
								<tr>
									<td width="30%">
										<?php echo lang('label_invite_code_name' , 'invite_code');?><span class="form-required" title="This field is required.">*</span>
									</td>
                                    <td width="40%">
											<?php echo form_input(array('name' => 'invite_code', 'id' => 'invite_code' , 'title' => lang('label_invite_code_title') , 'value' => set_value('invite_code'), 'class' => 'form-poshytip'))?>
											<p class="help-text"><?php echo lang('label_invite_code_help_text'); ?></p>
									</td>
                                    <td align="left"><span class="error-message"><?php if(!empty($invite_code_error)) echo $invite_code_error; ?><?php echo form_error('invite_code'); ?></span></td>
                                </tr>
								</table>
								</div>
								</fieldset>
								<?php endif; ?>
                    		
                                <fieldset>
                                <h6 class="StepTitle"><?php echo lang('label_registration_step_1_title');?></h6>
                                <p><?php echo lang('label_registration_step_1_description');?></p>
                                <div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="30%"><?php echo lang('label_language_name' , 'language_name');?></td>
                                    <td width="40%">
                                        <?php echo form_dropdown('language_id', $languages, null, 'id="language_name" title="'.lang('label_language_name_title').'" class="form-poshytip"');?>
                                        <br /><br /><p class="help-text"><?php echo lang('label_language_name_help_text'); ?></p>
                                    </td>
                                    <td width="30%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="30%"><?php echo lang('label_timezone_name' , 'timezone_name');?></td>
                                    <td width="40%">
                                        <?php echo form_dropdown('timezone_id', $timezones, null, 'id="timezone_name" title="'.lang('label_timezone_name_title').'" class="form-poshytip"');?>
                                        <br /><br /><p class="help-text"><?php echo lang('label_timezone_name_help_text'); ?></p>
                                    </td>
                                    <td width="30%">&nbsp;</td>
                                </tr> 
								</table>
								</div>
								</fieldset>
                        
                            	<fieldset>
                                <h6 class="StepTitle"><?php echo lang('label_registration_step_2_title');?></h6>
                                <p><?php echo lang('label_registration_step_2_description');?></p>
                                <div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="30%"><?php echo lang('label_first_name' , 'first_name');?></td>
                                    <td width="40%"><?php echo form_input(array('name' => 'first_name', 'id' => 'first_name' , 'title' => lang('label_first_name_title') , 'value' => set_value('first_name'), 'class' => 'form-poshytip'))?></td>
                                    <td width="30%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('label_last_name' , 'last_name');?></td>
        							<td><?php echo form_input(array('name' => 'last_name', 'id' => 'last_name' , 'title' => lang('label_last_name_title') , 'value' => set_value('last_name') , 'class' => 'form-poshytip'))?></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('label_email' , 'email');?><span class="form-required" title="This field is required.">*</span></td>
                                    <td><?php echo form_input(array('name' => 'email', 'id' => 'email', 'title' => lang('label_email_title') , 'value' => set_value('email')  , 'class' => 'form-poshytip'))?></td>
                                    <td align="left"><span class="error-message"><?php if(!empty($email_error)) echo $email_error; ?><?php echo form_error('email'); ?></span></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('label_username' , 'username');?><span class="form-required" title="This field is required.">*</span></td>
                                    <td><?php echo form_input(array('name' => 'username', 'id' => 'username', 'title' => lang('label_register_username_title') , 'value' => set_value('username') , 'class' => 'form-poshytip'))?></td>
                                    <td align="left"><span class="error-message"><?php if(!empty($username_error)) echo $username_error; ?><?php echo form_error('username'); ?></span></td>
                                </tr>
                                <tr>                                                                                        
                                    <td>&nbsp;</td>
                                    <td><p><?php echo form_button(array('name' => 'button', 'content' => lang('label_check_availability') , 'id' => 'check_username_availability')); ?></p></td>
                                    <td><div id='username_availability_result'></div></td>
                                </tr>
                                
                                <!--<tr>
                                    <td><?php echo lang('label_password' , 'password');?><span class="form-required" title="This field is required.">*</span></td>                            
                                    <td><?php echo form_password(array('name' => 'password', 'id' => 'password', 'title' => lang('label_register_password_title')  , 'value' => '' , 'class' => 'form-poshytip'))?></td>
                                    <td align="left"><span class="error-message"><?php echo form_error('password'); ?></span></td>
                                </tr>
        
                                <tr>
                                    <td><label>Retype Password</label><span class="form-required" title="This field is required.">*</span></td>
                                    <td><?php echo form_password(array('name' => 'passconf', 'id' => 'passconf', 'title' => lang('label_register_retype_password_title') , 'value' => '' , 'class' => 'form-poshytip'))?></td>
                                    <td align="left"><span class="error-message"><?php echo form_error('passconf'); ?></span></td>
                                </tr>-->
        						<!--<tr>
                                    <td>&nbsp;</td>
        							<td><?php echo form_checkbox('terms_and_conditions', 'accept', set_checkbox('terms_and_conditions', 'accept')); ?>&nbsp;<?php echo lang('label_terms_and_condition'); ?></td>
        							<td align="left"><span class="error-message"><?php echo form_error('terms_and_conditions'); ?></span></td>
        						</tr>	
        						
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="left"><p><?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_submit'), 'id' => 'submit'))?></p></td>
                                    <td>&nbsp;</td>
                                </tr>-->
                                
                                </table>
                                </div>
        					   </fieldset>        
                               
                               	<fieldset>
                                <h6 class="StepTitle"><?php echo lang('label_registration_step_3_title');?></h6>
                                <p><?php echo lang('label_registration_step_3_description');?></p>
                                <br />
                                <div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="30%"><?php echo lang('label_company_name' , 'company_name');?><span class="form-required" title="This field is required.">*</span></td>
                                    <td width="40%"><?php echo form_input(array('name' => 'company_name', 'id' => 'company_name' , 'title' => lang('label_company_name_title') , 'value' => set_value('company_name'), 'class' => 'form-poshytip'))?></td>
                                    <td align="left"><span class="error-message"><?php if(!empty($company_name_error)) echo $company_name_error; ?><?php echo form_error('company_name'); ?></span></td>
                                </tr>
								<tr>
                                    <td width="30%"><?php echo lang('label_company_street_address' , 'company_street_address');?></td>
                                    <td width="40%"><?php echo form_input(array('name' => 'company_street_address', 'id' => 'company_street_address' , 'title' => lang('label_company_street_address_title') , 'value' => set_value('company_street_address'), 'class' => 'form-poshytip'))?></td>
                                    <td width="30%">&nbsp;</td>
                                </tr>
								<tr>
                                    <td width="30%"><?php echo lang('label_company_city' , 'company_city');?></td>
                                    <td width="40%"><?php echo form_input(array('name' => 'company_city', 'id' => 'company_city' , 'title' => lang('label_company_city_title') , 'value' => set_value('company_city'), 'class' => 'form-poshytip'))?></td>
                                    <td width="30%">&nbsp;</td>
                                </tr>
								<tr>
                                    <td width="30%"><?php echo lang('label_company_postcode' , 'company_postcode');?></td>
                                    <td width="40%"><?php echo form_input(array('name' => 'company_postcode', 'id' => 'company_postcode' , 'title' => lang('label_company_postcode_title') , 'value' => set_value('company_postcode'), 'class' => 'form-poshytip'))?></td>
                                    <td width="30%">&nbsp;</td>
                                </tr>
								</table>
								</div>
								</fieldset>  

                    
                        		<fieldset>
                                <h6 class="StepTitle"><?php echo lang('label_registration_step_4_title');?></h6>
                                <p><?php echo lang('label_registration_step_4_description');?></p>
                                <div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>&nbsp;</td>
        							<td><?php echo form_checkbox('terms_and_conditions', 'accept', set_checkbox('terms_and_conditions', 'accept')); ?>&nbsp;<?php echo lang('label_terms_and_condition'); ?></td>
        							<td align="left"><span class="error-message"><?php echo form_error('terms_and_conditions'); ?></span></td>
        						</tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="left"><p><?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_submit'), 'id' => 'submit'))?></p></td>
                                    <td>&nbsp;</td>
                                </tr>
								</table>
								</div>
								</fieldset>                			
                    </div>
              	
				<?php echo form_close(); ?>
                                
				<!--<p id="sent-form-msg" class="success">Form data sent. Thanks for your comments.</p>-->
				<!-- ENDS form -->
			</div>

    		</div>
    		<!-- ENDS page content -->
	<!-- Fold image -->
	<div id="fold"></div>
	</div>
</div>
<!-- ENDS MAIN -->
