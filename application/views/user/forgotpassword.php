<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_forgot_password_title');?></h1><span class="subheading"></span>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		<!-- page content -->
      	<div id="page-content" class="clearfix">
           
			<!-- 2 cols -->
			<div class="left-page-block">
			
                <?php if (isset($message) && ($message != '')): ?>  
                    <p id="sent-form-msg" class="failure"><?php echo $message; ?></p>
                <?php endif; ?>
                
				<!-- form -->
				<?php echo form_open('forgotpassword' , array('id' => 'forgotPasswordForm', 'class' => 'niceForm'));?>
					<!--<p> Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget.</p>-->
					<fieldset>
						<div>
                        <table width="100%" border="0" cellspaing="0" cellpadding="0">
                        <tr>
							<td><?php echo lang('label_email' , 'email');?><span class="form-required" title="This field is required.">*</span></td>
							<td><?php echo form_input(array('name' => 'email', 'id' => 'email', 'title' => lang('label_forgot_password_email_title') , 'value' => set_value('email')  , 'class' => 'form-poshytip'))?>
							<br /><span class="error-message"><?php echo form_error('email'); ?></span></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left"><p><?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_submit'), 'id' => 'submit', 'class'=>'enviar'))?></p></td>
                        </tr>
                        </table>    
						</div>
					</fieldset>
					
				<?php echo form_close(); ?>
                                
				
				<!-- ENDS form -->
			</div>
			
			<div class="right-page-block">
				<h4 class="heading"><?php echo lang('label_help');?></h4>
                <p><?php echo lang('label_forgot_password_description');?></p>
			</div>
			<!-- ENDS 2 cols -->
    		</div>
    		<!-- ENDS page content -->
	<!-- Fold image -->
	<div id="fold"></div>
	</div>
</div>
<!-- ENDS MAIN -->
