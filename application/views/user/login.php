<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_login_page_title');?></h1><span class="subheading"></span>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		<!-- page content -->
      	<div id="page-content" class="clearfix">
           
			<!-- 2 cols -->
			<div class="left-page-block">
				<h4 class="heading"><?php echo lang('label_login_form_title');?></h4>
                  
                <?php if (isset($message) && ($message != '')): ?>  
                    <p id="sent-form-msg" class="failure"><?php echo $message; ?></p>
                <?php endif; ?>
                
                <?php if (isset($registration_message) && ($registration_message != '')): ?>  
                    <p id="sent-form-msg" class="success"><?php echo $registration_message; ?></p>
                <?php endif; ?>
                
				<!-- form -->
				<?php echo form_open('login' , array('id' => 'loginForm', 'class' => 'niceForm'));?>
					<!--<p> Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget.</p>-->
					<fieldset>
						<div>
                        <table width="100%" border="0" cellspaing="0" cellpadding="0">
                        <tr>
                            <td><?php echo lang('label_username' , 'username');?></td>
							<td><?php echo form_input(array('name'=>'username', 'id'=>'username', 'data-icon'=>'U', 'title'=>lang('label_username_title'), 'value' => '', 'class'=> 'form-poshytip'))?>
                            <span class="error-message"><?php echo form_error('username'); ?></span>
                            </td>
						</tr>
                        <tr>
                            <td><?php echo lang('label_password' , 'password');?></td>
							<td><?php echo form_password(array('name'=>'password', 'id'=>'password', 'data-icon'=>'x', 'title'=>lang('label_password_title') , 'value' => '', 'class'=> 'form-poshytip'))?>
                            <span class="error-message"><?php echo form_error('password'); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left"><p><?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_submit'), 'id' => 'submit', 'class'=>'enviar'))?></p></td>
                        </tr>
                        </table>    
						</div>
					</fieldset>
					
				<?php echo form_close(); ?>
                                
				
				<!-- ENDS form -->
			</div>
			
			<div class="right-page-block">
				<?php if (isset($is_maintenance) AND $is_maintenance == 1): ?>
					<!-- Dont show register, forgot password links -->
				<?php else: ?>
					<h4 class="heading"><?php echo lang('label_new_user');?></h4>
					<p><?php echo lang('label_new_user_description');?></p>
					<ul>
						<li><?php echo anchor('pricing', lang('label_new_user_link'), array('class'=>'signup'));?></li>
					</ul>
					<br />
					<h4 class="heading"><?php echo lang('label_need_help');?></h4>
					<p><?php echo anchor('forgotpassword', lang('label_forgot_password'));?></p>
				<?php endif; ?>
			</div>
			<!-- ENDS 2 cols -->
    		</div>
    		<!-- ENDS page content -->
	<!-- Fold image -->
	<div id="fold"></div>
	</div>
</div>
<!-- ENDS MAIN -->
