<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_register_disabled_page_title');?></h1><span class="subheading"></span>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
		<!-- page content -->
      	<div id="page-content" class="clearfix">
           
			<div class="one-full">
				<h4 class="heading"><?php echo lang('label_register_disabled_sub_title');?></h4>
				<?php echo lang('label_register_disabled_description'); ?>
			</div>
		</div>

    </div>
    <!-- ENDS page content -->
	<!-- Fold image -->
	<div id="fold"></div>
</div>
<!-- ENDS MAIN -->
