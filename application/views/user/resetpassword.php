<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li class="active"><?php echo lang('label_reset_password');?></li>
	</ul>
</div>

<?php if (isset($error_message) && ($error_message != '')): ?>  
<!--<div class="alert alert-error">
	<button data-dismiss="alert" class="close" type="button">×</button>
	<?php echo $error_message;?>
</div>-->
<a href='#' style="display:none;" id="error_message_link" data-noty-options='{"text":"<?php echo $error_message;?>", "layout":"topCenter","type":"error"}' class="btn btn-primary noty"></a>
<?php elseif (isset($message) && ($message != '')): ?>  
<!--<div class="alert alert-success">
	<button data-dismiss="alert" class="close" type="button">×</button>
	<?php echo $message;?>
</div>-->
<a href='#' style="display:none;" id="success_message_link" data-noty-options='{"text":"<?php echo $message;?>","layout":"topCenter","type":"success"}' class="btn btn-primary noty"></a>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-cog"></i> <?php echo lang('label_reset_password');?></h2>
			
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
			
		</div>
		
		<div class="box-content">

					<?php echo form_open('resetpassword/'.$checksum, array('id' => 'resetpassword' , 'class' => 'form-horizontal'));?>
					<?php echo form_hidden('checksum', $checksum); ?>
					<?php if (form_error('password')): ?>
						<div class="control-group error">
					<?php else: ?>
						<div class="control-group">
					<?php endif; ?>
						<label class="control-label"><?php echo lang('label_password');?>&nbsp;<span class="form-required" title="This field is required.">*</span></label>
						<div class="controls">
						  <div class="input-prepend">
							<?php echo form_password(array('name' => 'password', 'id' => 'password' , 'title' => lang('label_password_title')));?>
							<span class="help-inline"><?php echo form_error('password'); ?></span>
						  </div>
						</div>
					</div>
					<?php if (form_error('passconf')): ?>
						<div class="control-group error">
					<?php else: ?>
						<div class="control-group">
					<?php endif; ?>
						<label class="control-label"><?php echo lang('label_passconf');?>&nbsp;<span class="form-required" title="This field is required.">*</span></label>
						<div class="controls">
						  <div class="input-prepend">
							<?php echo form_password(array('name' => 'passconf', 'id' => 'passconf' , 'title' => lang('label_passconf_title')));?>
							<span class="help-inline"><?php echo form_error('passconf'); ?></span>
						  </div>
						</div>
					</div>
					
					<div class="form-actions">
						<?php echo form_submit(array('name'=>'submit', 'value'=>'Update', 'class' => 'btn btn-primary' , 'id' => 'submit'))?>
					</div>

				<?php echo form_close(); ?>
						
		</div>
	</div><!--/span-->

</div><!--/row-->

		<!-- content ends -->
</div><!--/#content.span10-->
