<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_one_time_login_page_title');?></h1><span class="subheading"></span>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		<!-- page content -->
      	<div id="page-content" class="clearfix">
           
			<?php if (isset($message) && ($message != '')): ?>  
				<p id="sent-form-msg" class="success"><?php echo $message; ?></p>
			<?php endif; ?>
		   
			<div class="one-full">
				<p>This is a one-time login for <span class="highlight"><?php echo $username; ?></span> and will expire on <span class="highlight"><?php echo $valid_until; ?></span>.</p>
				<p>Click on this button to log in to the site and change your password.</p>
				<p>This login can be used only once.</p>
				
				<div class="clearfix"></div>	
				
				<?php echo form_open('user/reset/'.$checksum , array('id' => 'ActiveaccountForm', 'class' => 'niceForm'));?>
					<?php echo form_hidden('checksum', $checksum); ?>
					<?php echo form_hidden('user_id', $user_id); ?>
					<?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_login_page_title'), 'id' => 'submit', 'class'=>'enviar'))?>
				<?php echo form_close(); ?>
			
			</div>

			<!-- ENDS 2 cols -->
    		</div>
    		<!-- ENDS page content -->
	<!-- Fold image -->
	<div id="fold"></div>
	</div>
</div>
<!-- ENDS MAIN -->
