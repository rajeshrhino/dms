<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/dashboard' , lang('label_admin_dashboard'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/companies' , lang('label_companies'))?><span class="divider">/</span></li>
		<li class="active"><?php echo anchor('admin/userslist' , lang('label_users'))?>
	</ul>
</div>

<?php if (isset($message) && ($message != '')): ?>  
	<ul class="noty_cont noty_layout_topLeft">
		<div class="noty_message"><span class="noty_text"><?php echo $message; ?></span></div>
	</ul>
<?php endif; ?>

<?php if (isset($error_message) && ($error_message != '')): ?>  
	<a href='#' style="display:none;" id="error_message_link" data-noty-options='{"text":"<?php echo $error_message;?>", "layout":"topCenter","type":"error"}' class="btn btn-primary noty"></a>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-user"></i> <?php echo lang('label_users');?> <?php echo lang('label_in');?> <?php echo $company_name;?></h2>
			
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
			
		</div>
		
		<div class="box-content">
			
			
			<div class="span12 well">
					<div class="span6">
						<h3><?php echo lang('label_company_name');?></h3>
						<p><?php echo $company_name;?><br />
					</div>
					<div class="span6">
						<?php if (!empty($full_address)):?>
						<h3><?php echo lang('label_address');?></h3>
						<?php echo $full_address;?>
						<?php endif;?>
					</div>
			  </div>
			
				<?php echo anchor('admin/userslist/'.$company_id.'/add' , '<i class="icon-plus"></i> '.lang('label_user_add') , array('class'=>"btn") )?>&nbsp;
					
			
			
			<?php echo $table; ?>
		</div>
	</div><!--/span-->

</div><!--/row-->

		<!-- content ends -->
</div><!--/#content.span10-->