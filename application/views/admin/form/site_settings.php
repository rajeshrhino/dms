<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/dashboard' , lang('label_admin_dashboard'))?><span class="divider">/</span></li>
		<li class="active"><?php echo lang('label_site_settings')?>
	</ul>
</div>

<?php if (isset($message) && ($message != '')): ?>  
	<a href='#' style="display:none;" id="success_message_link" data-noty-options='{"text":"<?php echo $message;?>","layout":"topCenter","type":"success"}' class="btn btn-primary noty"></a>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-cog"></i> <?php echo lang('label_site_settings');?></h2>
		</div>	
		
		<div class="box-content">
			
			<?php if (isset($error_message) && ($error_message != '')): ?>  
				<div class="alert alert-error">
					<?php echo $error_message;?>
				</div>
			<?php endif; ?>
			
			<?php echo form_open('admin/sitesettings' , array('id' => 'SiteSettingsForm', 'class' => 'form-horizontal'));?>
			
			<fieldset>
			<legend><?php echo lang('label_site_settings'); ?></legend>
			
			<?php if (form_error('users_can_register')): ?>
				<div class="control-group error">
			<?php else: ?>
				<div class="control-group">
			<?php endif; ?>
				<label class="control-label"><?php echo lang('label_users_can_register');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_checkbox(array('name' => 'users_can_register', 'id' => 'users_can_register' , 'value' => 1  , 'checked' => $users_can_register));?>
				  </div>
				  <p class="help-block"><?php echo lang('label_users_can_register_help');?></p>
				</div>
			</div>
			<?php if (form_error('invite_code_mandatory')): ?>
				<div class="control-group error">
			<?php else: ?>
				<div class="control-group">
			<?php endif; ?>
				<label class="control-label"><?php echo lang('label_invite_code_mandatory');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_checkbox(array('name' => 'invite_code_mandatory', 'id' => 'invite_code_mandatory' , 'value' => 1  , 'checked' => $invite_code_mandatory));?>
				  </div>
				  <p class="help-block"><?php echo lang('label_invite_code_mandatory_help');?></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_memcache_enabled');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_checkbox(array('name' => 'memcache_enabled', 'id' => 'memcache_enabled' , 'value' => 1  , 'checked' => $memcache_enabled));?>
				  </div>
				  <p class="help-block"><?php echo lang('label_memcache_enabled_help');?></p>
				</div>
			</div>
			</fieldset>
			
			<fieldset>
			<legend><?php echo lang('label_site_email_settings'); ?></legend>
			
			<?php if (form_error('global_from_email_address')): ?>
				<div class="control-group error">
			<?php else: ?>
				<div class="control-group">
			<?php endif; ?>
				<label class="control-label"><?php echo lang('label_global_from_email_address');?><span class="form-required" title="This field is required.">*</span></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'global_from_email_address', 'id' => 'global_from_email_address' , 'value' => set_value('global_from_email_address' , $global_from_email_address)  , 'title' => lang('label_global_from_email_address_help')));?>
				  </div>
				  <p class="help-block"><?php echo lang('label_global_from_email_address_help');?></p>
				</div>
			</div>
			
			<?php if (form_error('global_from_email_name')): ?>
				<div class="control-group error">
			<?php else: ?>
				<div class="control-group">
			<?php endif; ?>
				<label class="control-label"><?php echo lang('label_global_from_email_name');?><span class="form-required" title="This field is required.">*</span></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'global_from_email_name', 'id' => 'global_from_email_name' , 'value' => set_value('global_from_email_name' , $global_from_email_name)  , 'title' => lang('label_global_from_email_name_help')));?>
				  </div>
				  <p class="help-block"><?php echo lang('label_global_from_email_name_help');?></p>
				</div>
			</div>
			
			<?php if (form_error('contact_us_receiving_email_address')): ?>
				<div class="control-group error">
			<?php else: ?>
				<div class="control-group">
			<?php endif; ?>
				<label class="control-label"><?php echo lang('label_contact_us_receiving_email_address');?><span class="form-required" title="This field is required.">*</span></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_textarea(array('name' => 'contact_us_receiving_email_address', 'id' => 'contact_us_receiving_email_address' , 'value' => set_value('contact_us_receiving_email_address' , $contact_us_receiving_email_address)  , 'title' => lang('label_global_from_email_address_help') , 'rows' => 3));?>
				  </div>
				  <p class="help-block"><?php echo lang('label_contact_us_receiving_email_address_help');?></p>
				</div>
			</div>
			</fieldset>
			
			<div class="form-actions">
				<?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_save'), 'id' => 'submit', 'class'=>'btn btn-primary'))?>
			</div>
			
			<?php echo form_close(); ?>
			
		</div>
			
		</div>
	</div><!--/span-->
		<!-- content ends -->
</div><!--/#content.span10-->