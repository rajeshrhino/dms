<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/dashboard' , lang('label_admin_dashboard'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/invitecode' , lang('label_invite_codes'))?><span class="divider">/</span></li>
		<li class="active"><?php echo lang('label_invite_code_add')?>
	</ul>
</div>

<?php if (isset($message) && ($message != '')): ?>  
	<ul class="noty_cont noty_layout_topLeft">
		<div class="noty_message"><span class="noty_text"><?php echo $message; ?></span></div>
	</ul>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-tag"></i> <?php if ($operation == 'edit') { echo lang('label_invite_code_edit'); } else {echo lang('label_invite_code_add');} ;?></h2>
		
		</div>	
		<div class="box-content">
			
			<?php if (isset($error_message) && ($error_message != '')): ?>  
				<div class="alert alert-error">
					<?php echo $error_message;?>
				</div>
			<?php endif; ?>
			
			<?php echo form_open('admin/invitecode/save' , array('id' => 'InvitationCodeForm', 'class' => 'form-horizontal'));?>
			
			<?php echo form_hidden('operation', $operation); ?>
			
			<?php if ($operation == 'edit') { echo form_hidden('invite_code_id', $invite_code_id); } ?>
			
			<?php if (form_error('invite_code')): ?>
				<div class="control-group error">
			<?php else: ?>
				<div class="control-group">
			<?php endif; ?>
				<label class="control-label"><?php echo lang('label_invite_code_name');?><span class="form-required" title="This field is required.">*</span></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'invite_code', 'id' => 'invite_code' , 'value' => set_value('invite_code' , $invite_code)  , 'title' => lang('label_invite_code_name')));?>
				  </div>
				  <!--<p class="help-block"><?php echo lang('label_message_title_help');?></p>-->
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_is_active');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_checkbox(array('name' => 'is_active', 'id' => 'is_active' , 'value' => 1  , 'checked' => $is_active));?>
				  </div>
				</div>
			</div>
			<div class="form-actions">
				<?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_save'), 'id' => 'submit', 'class'=>'btn btn-primary'))?>
				<?php echo anchor('admin/invitecode' , lang('label_cancel') , array('class' => 'btn'))?>
			</div>
			
			<?php echo form_close(); ?>
			
		</div>
			
		</div>
	</div><!--/span-->
		<!-- content ends -->
</div><!--/#content.span10-->