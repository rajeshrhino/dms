<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/dashboard' , lang('label_admin_dashboard'))?><span class="divider">/</span></li>
		<li class="active"><?php echo lang('label_maintenance_form_title')?>
	</ul>
</div>

<?php if (isset($message) && ($message != '')): ?>  
<a href='#' style="display:none;" id="success_message_link" data-noty-options='{"text":"<?php echo $message;?>","layout":"topCenter","type":"success"}' class="btn btn-primary noty"></a>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-cog"></i> <?php echo lang('label_maintenance_form_title');?></h2>
		</div>	
		
		
		
		<div class="box-content">
			
			<?php echo form_open('admin/sitemaintenance' , array('id' => 'SiteMaintenanceForm', 'class' => 'form-horizontal'));?>
			
			<?php if (form_error('site_under_maintenance')): ?>
				<div class="control-group error">
			<?php else: ?>
				<div class="control-group">
			<?php endif; ?>
				<label class="control-label"><?php echo lang('label_maintenance_mode');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_checkbox(array('name' => 'site_under_maintenance', 'id' => 'site_under_maintenance' , 'value' => 1  , 'checked' => $site_under_maintenance));?>
				  </div>
				  <p class="help-block"><?php echo lang('label_maintenance_form_description');?></p>
				</div>
			</div>

			<div class="form-actions">
				<?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_save'), 'id' => 'submit', 'class'=>'btn btn-primary'))?>
			</div>
			
			<?php echo form_close(); ?>
			
		</div>
			
		</div>
	</div><!--/span-->
		<!-- content ends -->
</div><!--/#content.span10-->