<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/dashboard' , lang('label_admin_dashboard'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/userslist' , lang('label_users'))?><span class="divider">/</span></li>
		<li class="active"><?php echo lang('label_user_add')?>
	</ul>
</div>

<?php if (isset($message) && ($message != '')): ?>  
	<ul class="noty_cont noty_layout_topLeft">
		<div class="noty_message"><span class="noty_text"><?php echo $message; ?></span></div>
	</ul>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-user"></i> <?php echo lang('label_user_add');?></h2>
		
		</div>	
		<div class="box-content">
			
			<?php if (isset($error_message) && ($error_message != '')): ?>  
				<div class="alert alert-error">
					<?php echo $error_message;?>
				</div>
			<?php endif; ?>
			
			<?php echo form_open('admin/userslist/save' , array('id' => 'CompanyForm', 'class' => 'form-horizontal'));?>
			
			<?php echo form_hidden('operation', $operation); ?>
			
			<?php if (isset($is_corporate_user) AND $is_corporate_user == 1): ?>
				<?php if (isset($company_id)) { 
				echo form_hidden('company_id', $company_id); 
				echo form_hidden('is_corporate_user', $is_corporate_user); 
				} ?>
			<?php endif; ?>
			
			<?php if (isset($is_corporate_user) AND $is_corporate_user == 1): ?>
			
			<fieldset>
				<legend><?php echo lang('label_registration_step_3_title');?></legend>
			
				<div class="control-group">
					<label class="control-label"><?php echo lang('label_company_name'); ?></label>
					<div class="controls">
					  <div class="input-prepend">
						<h3><?php echo $company_name;?></h3>
					  </div>
					</div>
				</div>
			</fieldset>
			
			<?php endif; ?>
			
			<fieldset>
			
				<legend><?php echo lang('label_registration_step_2_title');?></legend>
				
				<div class="control-group">
					<label class="control-label"><?php echo lang('label_first_name');?></label>
					<div class="controls">
					  <div class="input-prepend">
						<?php echo form_input(array('name' => 'first_name', 'id' => 'first_name' , 'value' => set_value('first_name' , $first_name)  , 'title' => lang('label_first_name_title')));?>
						<span class="help-inline"><?php echo form_error('first_name'); ?></span>
					  </div>
					  <!--<p class="help-block"><?php echo lang('label_message_title_help');?></p>-->
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><?php echo lang('label_last_name');?></label>
					<div class="controls">
					  <div class="input-prepend">
						<?php echo form_input(array('name' => 'last_name', 'id' => 'last_name' , 'value' => set_value('last_name' , $last_name)  , 'title' => lang('label_last_name_title')));?>
						<span class="help-inline"><?php echo form_error('last_name'); ?></span>
					  </div>
					</div>
				</div>
				<?php if (form_error('email') OR isset($email_error)): ?>
					<div class="control-group error">
				<?php else: ?>
					<div class="control-group">
				<?php endif; ?>
					<label class="control-label"><?php echo lang('label_email');?><span class="form-required" title="This field is required.">*</span></label>
					<div class="controls">
					  <div class="input-prepend">
						<?php echo form_input(array('name' => 'email', 'id' => 'email' , 'value' => set_value('email' , $email)  , 'title' => lang('label_email_title')));?>
						<span class="help-inline"><?php if(!empty($email_error)) echo $email_error; ?><?php echo form_error('email'); ?></span>
						<p class="help-block"><?php echo lang('label_account_email_help');?></p>
					  </div>
					</div>
				</div>
				<?php if (form_error('username') OR isset($username_error)): ?>
					<div class="control-group error">
				<?php else: ?>
					<div class="control-group">
				<?php endif; ?>
					<label class="control-label"><?php echo lang('label_username');?><span class="form-required" title="This field is required.">*</span></label>
					<div class="controls">
					  <div class="input-prepend">
						<?php echo form_input(array('name' => 'username', 'id' => 'username' , 'value' => set_value('username' , $user_name)  , 'title' => lang('label_username_title')));?>
						<span class="help-inline">
						<?php if(!empty($username_error) AND isset($username_error)) echo $username_error; ?><?php echo form_error('username'); ?>
						</span>
						<p class="help-block"><?php echo lang('label_account_username_help');?></p>
					  </div>
					</div>
				</div>
				
				<div class="control-group">
					<div class="controls">
						<?php echo form_button(array('name' => 'button', 'content' => lang('label_check_availability') , 'id' => 'check_username_availability')); ?>
						<span class="help-inline"><div id='username_availability_result' class="success-message"></div></span>
					</div>
				</div>
				
				<!--<?php if (form_error('password')): ?>
					<div class="control-group error">
				<?php else: ?>
					<div class="control-group">
				<?php endif; ?>
					<label class="control-label"><?php echo lang('label_password');?><span class="form-required" title="This field is required.">*</span></label>
					<div class="controls">
					  <div class="input-prepend">
						<?php echo form_password(array('name' => 'password', 'id' => 'password' , 'value' => set_value('')  , 'title' => lang('label_password_title')));?>
						<span class="help-inline"><?php echo form_error('password'); ?></span>
					  </div>
					</div>
				</div>
				<?php if (form_error('passconf')): ?>
					<div class="control-group error">
				<?php else: ?>
					<div class="control-group">
				<?php endif; ?>
					<label class="control-label"><?php echo lang('label_passconf');?><span class="form-required" title="This field is required.">*</span></label>
					<div class="controls">
					  <div class="input-prepend">
						<?php echo form_password(array('name' => 'passconf', 'id' => 'passconf' , 'value' => set_value('')  , 'title' => lang('label_passconf_title')));?>
						<span class="help-inline"><?php echo form_error('passconf'); ?></span>
						<p class="help-block"><?php echo lang('label_account_password_help');?></p>
					  </div>
					</div>
				</div>-->
			</fieldset>
			
			<fieldset>
			
				<legend><?php echo lang('label_registration_step_1_title');?></legend>
			
				<div class="control-group">
					<label class="control-label"><?php echo lang('label_language_name'); ?></label>
					<div class="controls">
					  <div class="input-prepend">
						<?php echo form_dropdown('language_id', $languages, null, 'id="language_name" title="'.lang('label_language_name_title').'" class="form-poshytip"');?>
					  </div>
					  <p class="help-block"><?php echo lang('label_language_name_help_text');?></p>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label"><?php echo lang('label_timezone_name'); ?></label>
					<div class="controls">
					  <div class="input-prepend">
						<?php echo form_dropdown('timezone_id', $timezones, null, 'id="timezone_name" title="'.lang('label_timezone_name_title').'" class="form-poshytip"');?>
					  </div>
					  <p class="help-block"><?php echo lang('label_timezone_name_help_text1');?></p>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label"><?php echo lang('label_account_status'); ?></label>
					<div class="controls">
					  <div class="input-prepend">
						<?php echo form_dropdown('account_status', $account_statuses, null, 'id="timezone_name" title="'.lang('label_account_status_title').'" class="form-poshytip"');?>
					  </div>
					  <!--<p class="help-block"><?php echo lang('label_language_name_help_text');?></p>-->
					</div>
				</div>
			
			</fieldset>
		
			<div class="form-actions">
				<?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_save'), 'id' => 'submit', 'class'=>'btn btn-primary'))?>
				<?php echo anchor('admin/companieslist' , lang('label_cancel') , array('class' => 'btn'))?>
			</div>
			
			<?php echo form_close(); ?>
			
		</div>
			
		</div>
	</div><!--/span-->
		<!-- content ends -->
</div><!--/#content.span10-->