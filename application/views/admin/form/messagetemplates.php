<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/dashboard' , lang('label_admin_dashboard'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/templates' , lang('label_message_templates'))?><span class="divider">/</span></li>
		<li class="active"><?php echo lang('label_message_template_add')?>
	</ul>
</div>

<?php if (isset($message) && ($message != '')): ?>  
	<ul class="noty_cont noty_layout_topLeft">
		<div class="noty_message"><span class="noty_text"><?php echo $message; ?></span></div>
	</ul>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-envelope"></i> <?php echo lang('label_message_template_add');?></h2>
		
		</div>	
		<div class="box-content">
			
			<?php if (isset($error_message) && ($error_message != '')): ?>  
				<div class="alert alert-error">
					<?php echo $error_message;?>
				</div>
			<?php endif; ?>
			
			<?php echo form_open('admin/templates/save' , array('id' => 'MessagetemplateForm', 'class' => 'form-horizontal'));?>
			<?php if (form_error('message_title')): ?>
				<div class="control-group error">
			<?php else: ?>
				<div class="control-group">
			<?php endif; ?>
				<label class="control-label"><?php echo lang('label_message_title');?><span class="form-required" title="This field is required.">*</span></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'message_title', 'id' => 'message_title' , 'value' => set_value('message_title')  , 'title' => lang('label_message_title')));?>
					<span id="folder_name_error_message" class="help-inline" style="display:none;"><?php echo lang('message_title_mandatory');?></span>
					<span id="folder_name_already_exists" class="help-inline" style="display:none;"><?php echo lang('message_title_exists');?></span>
				  </div>
				  <p class="help-block"><?php echo lang('label_message_title_help');?></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_message_subject');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'message_subject', 'id' => 'message_subject' , 'value' => set_value('message_subject')  , 'title' => lang('label_message_subject')));?>
				  </div>
				  <p class="help-block"><?php echo lang('label_message_subject_help');?></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_message_text_format');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_textarea(array('name' => 'message_text', 'id' => 'message_text' , 'value' => set_value('message_text')  , 'title' => lang('label_message_text_format') , 'rows' => 3));?>
				  </div>
				  <p class="help-block"><?php echo lang('label_message_text_format_help');?></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_message_html_format');?></label>
				<div class="controls">
				  <?php echo form_textarea(array('name' => 'message_html', 'id' => 'message_html' , 'value' => set_value('message_html')  , 'title' => lang('label_message_html_format') , 'rows' => 10 , 'class' => 'cleditor'));?>
				  <p class="help-block"><?php echo lang('label_message_html_format_help');?></p>
				</div>
			</div>
			
			<div class="form-actions">
				<?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_save'), 'id' => 'submit', 'class'=>'btn btn-primary'))?>
				<?php echo anchor('admin/templates' , lang('label_cancel') , array('class' => 'btn'))?>
			</div>
			
			<?php echo form_close(); ?>
			
		</div>
			
		</div>
	</div><!--/span-->
		<!-- content ends -->
</div><!--/#content.span10-->