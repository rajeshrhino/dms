<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/dashboard' , lang('label_admin_dashboard'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/companies' , lang('label_companies'))?><span class="divider">/</span></li>
		<li class="active"><?php echo lang('label_company_add')?>
	</ul>
</div>

<?php if (isset($message) && ($message != '')): ?>  
	<ul class="noty_cont noty_layout_topLeft">
		<div class="noty_message"><span class="noty_text"><?php echo $message; ?></span></div>
	</ul>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-flag"></i> <?php if ($operation == 'edit') { echo lang('label_company_edit'); } else {echo lang('label_company_add');} ;?></h2>
		
		</div>	
		<div class="box-content">
			
			<?php if (isset($error_message) && ($error_message != '')): ?>  
				<div class="alert alert-error">
					<?php echo $error_message;?>
				</div>
			<?php endif; ?>
			
			<?php echo form_open('admin/companies/save' , array('id' => 'CompanyForm', 'class' => 'form-horizontal'));?>
			
			<?php echo form_hidden('operation', $operation); ?>
			
			<?php if ($operation == 'edit') { echo form_hidden('company_id', $company_id); } ?>
			
			<?php if (form_error('company_name')): ?>
				<div class="control-group error">
			<?php else: ?>
				<div class="control-group">
			<?php endif; ?>
				<label class="control-label"><?php echo lang('label_company_name');?><span class="form-required" title="This field is required.">*</span></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'company_name', 'id' => 'company_name' , 'value' => set_value('company_name' , $company_name)  , 'title' => lang('label_company_name')));?>
				  </div>
				  <!--<p class="help-block"><?php echo lang('label_message_title_help');?></p>-->
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_company_street_address');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'company_street_address', 'id' => 'company_street_address' , 'value' => set_value('company_street_address' , $company_street_address)  , 'title' => lang('label_company_street_address')));?>
				  </div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_company_city');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'company_city', 'id' => 'company_city' , 'value' => set_value('company_city' , $company_city)  , 'title' => lang('label_company_city')));?>
				  </div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_company_postcode');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'company_postcode', 'id' => 'company_postcode' , 'value' => set_value('company_postcode' , $company_postcode)  , 'title' => lang('label_company_street_address')));?>
				  </div>
				</div>
			</div>
			
			<div class="form-actions">
				<?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_save'), 'id' => 'submit', 'class'=>'btn btn-primary'))?>
				<?php echo anchor('admin/companieslist' , lang('label_cancel') , array('class' => 'btn'))?>
			</div>
			
			<?php echo form_close(); ?>
			
		</div>
			
		</div>
	</div><!--/span-->
		<!-- content ends -->
</div><!--/#content.span10-->