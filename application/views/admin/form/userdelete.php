<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/dashboard' , lang('label_admin_dashboard'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/userslist' , lang('label_users'))?><span class="divider">/</span></li>
		<li class="active"><?php echo lang('label_delete_user')?>
	</ul>
</div>

<?php if (isset($message) && ($message != '')): ?>  
	<ul class="noty_cont noty_layout_topLeft">
		<div class="noty_message"><span class="noty_text"><?php echo $message; ?></span></div>
	</ul>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-user"></i> <?php echo lang('label_delete_user');?></h2>
		
		</div>	
		<div class="box-content">
		
			<h4><?php echo lang('label_username');?>: <?php echo $delete_username;?></h4><br />
			
			<div class="alert alert-block ">
				<h4 class="alert-heading"><?php echo lang('label_warning');?></h4><br />
				<p><?php echo lang('message_delete_user_confirm');?></p>
			</div>
			
			<div class="form-actions">
				<?php echo anchor('admin/userslist/delete/'.$delete_user_id.'/'.$company_id , lang('label_delete') , array('class' => 'btn btn-primary' , 'data-dismiss'=>"modal"))?>
				<?php echo anchor('admin/userslist' , lang('label_cancel') , array('class' => 'btn' , 'data-dismiss'=>"modal"))?>
			</div>
			
		</div>
			
		</div>
	</div><!--/span-->
		<!-- content ends -->
</div><!--/#content.span10-->