<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/dashboard' , lang('label_admin_dashboard'))?><span class="divider">/</span></li>
		<li class="active"><?php echo anchor('admin/invitecode' , lang('label_invite_codes'))?>
	</ul>
</div>

<?php if (isset($message) && ($message != '')): ?>  
	<a href='#' style="display:none;" id="success_message_link" data-noty-options='{"text":"<?php echo $message;?>","layout":"topCenter","type":"success"}' class="btn btn-primary noty"></a>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-tag"></i> <?php echo lang('label_invite_codes');?></h2>
			
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
			
		</div>
		
		<div class="box-content">
			<p><?php echo lang('label_invite_codes_description');?></p>
			<div class="alert alert-info">
					<?php echo anchor('admin/invitecode/add' , '<i class="icon-plus"></i> '.lang('label_invite_code_add') , array('class'=>"btn") )?>
			</div>

			<?php echo $table; ?>
						
		</div>
	</div><!--/span-->

</div><!--/row-->

		<!-- content ends -->
</div><!--/#content.span10-->