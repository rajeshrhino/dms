<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/dashboard' , lang('label_admin_dashboard'))?><span class="divider">/</span></li>
		<li class="active"><?php echo anchor('admin/companies' , lang('label_companies'))?>
	</ul>
</div>

<?php if (isset($message) && ($message != '')): ?>  
	<ul class="noty_cont noty_layout_topLeft">
		<div class="noty_message"><span class="noty_text"><?php echo $message; ?></span></div>
	</ul>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-flag"></i> <?php echo lang('label_companies');?></h2>
			
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
			
		</div>
		
		<div class="box-content">
		
			<div class="alert alert-info">
					<?php echo anchor('admin/companies/add' , '<i class="icon-plus"></i> '.lang('label_company_add') , array('class'=>"btn") )?>
			</div>

			<?php echo $table; ?>
						
		</div>
	</div><!--/span-->

</div><!--/row-->

		<!-- content ends -->
</div><!--/#content.span10-->