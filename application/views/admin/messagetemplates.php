<div id="content" class="span10">
<!-- content starts -->

<div>
	<ul class="breadcrumb">
		
		<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
		<li><?php echo anchor('admin/dashboard' , lang('label_admin_dashboard'))?><span class="divider">/</span></li>
		<li class="active"><?php echo anchor('admin/templates' , lang('label_message_templates'))?>
	</ul>
</div>

<?php if (isset($message) && ($message != '')): ?>  
	<ul class="noty_cont noty_layout_topLeft">
		<div class="noty_message"><span class="noty_text"><?php echo $message; ?></span></div>
	</ul>
<?php endif; ?>

<?php elseif (isset($message) && ($message != '')): ?>  
<a href='#' style="display:none;" id="success_message_link" data-noty-options='{"text":"<?php echo $message;?>","layout":"topCenter","type":"success"}' class="btn btn-primary noty"></a>
<?php endif; ?>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-envelope"></i> <?php echo lang('label_message_templates');?></h2>
			
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
			
		</div>
		
		<div class="box-content">
		
			<div class="alert alert-info">
					<?php echo anchor('admin/templates/add' , '<i class="icon-plus"></i> '.lang('label_message_template_add') , array('class'=>"btn") )?>
			</div>

			<?php echo $table; ?>
						
		</div>
	</div><!--/span-->

</div><!--/row-->

		<!-- content ends -->
</div><!--/#content.span10-->