<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">
		
		
    		<!-- home-block -->
      	<div class="home-block clearfix" >
      		<h1 class="home-headline">FEATURES</h1>
      		
      		<!-- thumbs -->
      		<div class="clearfix" >
      			
      			<figure>
	        		<?php echo anchor('features#webbased' , img(array('src'=>'misc/website/img/dummies/webbased-small.jpg','alt'=>'Web based')) , array('class' => 'simple-thumb'));?>
					<figcaption>
      					<?php echo anchor('features#webbased' , '<strong>Web Based</strong>');?>
      					<span>There is nothing to download, no software to install. All you need is your web browser.</span>
	        		</figcaption>
        		</figure>
        		
        		<figure>
					<?php echo anchor('features#multipleusers' , img(array('src'=>'misc/website/img/dummies/users-small.jpg','alt'=>'Multiple Users')) , array('class' => 'simple-thumb'));?>
	        		<figcaption>
      					<?php echo anchor('features#multipleusers' , '<strong>Multiple Users</strong>');?>
      					<span>With single or multiple user option, {_int} DMS is the perfect solution for individuals and companies.</span>
	        		</figcaption>
        		</figure>
        		
        		<figure>
					<?php echo anchor('features#ssl' , img(array('src'=>'misc/website/img/dummies/ssl-small.jpg','alt'=>'Transport Layer Security')) , array('class' => 'simple-thumb'));?>
	        		<figcaption>
      					<?php echo anchor('features#ssl' , '<strong>Transport Layer Security</strong>');?>
      					<span>{_int} DMS uses SSL technology to ensure that all of your information is encrypted.</span>
	        		</figcaption>
        		</figure>
        		
        		<figure>
					<?php echo anchor('features#docsecurity' , img(array('src'=>'misc/website/img/dummies/doc-security-small.jpg','alt'=>'Document Security')) , array('class' => 'simple-thumb'));?>
	        		<figcaption>
						<?php echo anchor('features#docsecurity' , '<strong>Document Security</strong>');?>
      					<span>Your documents are never stored in physical disc as it is. Even server/system administrator cannot view your plain documents.</span>
	        		</figcaption>
        		</figure>
        		
        		<figure>
					<?php echo anchor('features#securelogin' , img(array('src'=>'misc/website/img/dummies/secure-login-small.jpg','alt'=>'Highly secure login')) , array('class' => 'simple-thumb'));?>
	        		<figcaption>
						<?php echo anchor('features#securelogin' , '<strong>Highly secure login</strong>');?>
      					<span>We use unique encryption algorithm for each user i.e the same password 'password' will be encrypted to 2 different string for 2 different users.</span>
	        		</figcaption>
        		</figure>
        		
        		<figure>
					<?php echo anchor('features#search' , img(array('src'=>'misc/website/img/dummies/search-small.jpg','alt'=>'Faster & Advanced Search')) , array('class' => 'simple-thumb'));?>
	        		<figcaption>
						<?php echo anchor('features#search' , '<strong>Faster & Advanced Search</strong>');?>
      					<span>You can do more advanced search even using any text in your thousands of documents along with document name, type, etc.</span>
	        		</figcaption>
        		</figure>
				
				<!--<figure>
	        		<a href="single.html"  class="thumb"><img src="<?php echo base_url();?>misc/website/img/dummies/7.jpg" alt="Alt text" /></a>
	        		<figcaption>
      					<strong>Simple File Transfer</strong>
      					<span>Using our simple file browser, you can upload a single file, or entire folder to your {_int} DMS.</span>
	        		</figcaption>
        		</figure>
        		
        		<figure>
	        		<a href="single.html"  class="thumb"><img src="<?php echo base_url();?>misc/website/img/dummies/featured.jpg" alt="Alt text" /></a>
	        		<figcaption>
      					<strong>Find Files Easy</strong>
      					<span>By adding keyword and title, you can find what you need in seconds.</span>
	        		</figcaption>
        		</figure>
        		
        		<figure>
	        		<a href="single.html"  class="thumb"><img src="<?php echo base_url();?>misc/website/img/dummies/featured.jpg" alt="Alt text" /></a>
	        		<figcaption>
      					<strong>Multiple File Formats</strong>
      					<span>{_int} DMS is flexible, allowing you to upload, store and share any file format.</span>
	        		</figcaption>
        		</figure>-->
        		
      		</div>
      		<!-- ENDS thumbs -->
      		
      		<!--<a href="work.html" class="theme-link-button">See All Work</a>-->
      		
      	</div>
      	<!-- ENDS home-block -->
      	
      	
      	<!-- additional blocks -->
      	<!--<div class="home-add clearfix">
      		
      		<div class="left-home-block home-posts">
      			<h4 class="heading">FROM THE BLOG</h4>
      			<article class="format-standard">
					<div class="entry-date"><div class="number">23</div><div class="month">JAN</div> <div class="year">2011</div><em></em></div>
					<div  class="post-heading">
						<h4><a href="single.html">Lorem ipsum dolor </a></h4>
					</div>
					<div class="excerpt">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.
					</div>
				</article>
				
				<article class="format-standard">
					<div class="entry-date"><div class="number">23</div><div class="month">JAN</div> <div class="year">2011</div><em></em></div>
					<div  class="post-heading">
						<h4><a href="single.html">Lorem ipsum dolor </a></h4>
					</div>
					<div class="excerpt">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.
					</div>
				</article>
				
				<article class="format-standard">
					<div class="entry-date"><div class="number">23</div><div class="month">JAN</div> <div class="year">2011</div><em></em></div>
					<div  class="post-heading">
						<h4><a href="single.html">Lorem ipsum dolor </a></h4>
					</div>
					<div class="excerpt">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.
					</div>
				</article>
				
				<article class="format-standard">
					<div class="entry-date"><div class="number">23</div><div class="month">JAN</div> <div class="year">2011</div><em></em></div>
					<div  class="post-heading">
						<h4><a href="single.html">Lorem ipsum dolor </a></h4>
					</div>
					<div class="excerpt">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.
					</div>
				</article>
				
				
      		</div>
      		
      		<div class="right-home-block">
      			
      			
      			<h4 class="heading">CLIENTS</h4>
      			
      			
      			<ul class="clients-logos clearfix">
      				<li><a href="#"><img src="<?php echo base_url();?>misc/website/img/dummies/logo1.png" alt="logo" /></a></li>
      				<li><a href="#"><img src="<?php echo base_url();?>misc/website/img/dummies/logo2.png" alt="logo" /></a></li>
      				<li><a href="#"><img src="<?php echo base_url();?>misc/website/img/dummies/logo3.png" alt="logo" /></a></li>
      				<li><a href="#"><img src="<?php echo base_url();?>misc/website/img/dummies/logo4.png" alt="logo" /></a></li>
      				<li><a href="#"><img src="<?php echo base_url();?>misc/website/img/dummies/logo5.png" alt="logo" /></a></li>
      				<li><a href="#"><img src="<?php echo base_url();?>misc/website/img/dummies/logo6.png" alt="logo" /></a></li>
      			</ul>
      			
      			
      			
      			<h4 class="heading">TESTIMONIALS</h4>
      			
      			
		        <div class="flexslider testimonial-slider">
				  <ul class="slides">
				    <li>
				      Lorem ipsum dolor sit amet consectetuer Aenean pellentesque nisl nisl nulla. Felis eu nibh Nulla quis tincidunt ut semper id tellus Vivamus.
				      <div class="client-name">- Rajesh</div>
				    </li>
					<li>
				      Lorem ipsum dolor sit amet consectetuer Aenean pellentesque nisl nisl nulla. Felis eu nibh Nulla quis tincidunt ut semper id tellus Vivamus.
				      <div class="client-name">- Boby</div>
				    </li>
				    
				   <li>
				     Lorem ipsum dolor sit amet consectetuer Aenean pellentesque nisl nisl nulla. Felis eu nibh Nulla quis tincidunt ut semper id tellus Vivamus.
				     <div class="client-name">- Ryan</div>
				    </li>
				  </ul>
				  
				</div>
	        
        	
        		
      		</div>
      		
      		
      	</div>-->
    		<!-- ENDS additional blocks -->

	<!-- Fold image -->
	<div id="fold"></div>
	</div>
	
</div>
<!-- ENDS MAIN -->