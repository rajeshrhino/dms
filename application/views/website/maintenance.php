<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_maintenance_page_title');?></h1>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
		<!-- page content -->
      	<div id="page-content" class="clearfix">        	

			<div class="one-full">
				<?php echo lang('label_maintenance_page_description'); ?>
			</div>
    	<!-- ENDS page content -->

	<!-- Fold image -->
	<div id="fold"></div>
	</div>
	
</div>
</div>
<!-- ENDS MAIN -->