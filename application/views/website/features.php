<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1>FEATURES</h1>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
			<div id="posts-list" class="single clearfix">        	
				
				<article class="format-standard">
					<div class="entry-date"><div class="number">1</div></div>
					<div class="post-heading">
						<h4><a name="webbased">Web Based</a></h4>
						<div class="meta">
							<span class="user"><a href="#">Do it all here.</a></span>
						</div>
					</div>
					<div class="feature-image">
						<a href="<?php echo base_url();?>/misc/website/img/slides/webbased-large.jpg" data-rel="prettyPhoto">
							<img src="<?php echo base_url();?>/misc/website/img/slides/webbased-large.jpg" alt="Web based" />
						</a>
					</div>
					<div class="excerpt">
						{_int} DMS is a web based application which can be run straight in your web browser. All you need is a modern web browser. You dont need to download or install any software, or worry about upgrades -  you can just do your work.
						<br /><br />
						We handle all the security and uptime and backups and upgrades and “IT guy” stuff. You can stay focused on what you’re good at and we’ll handle the rest.
						<br /><br />
						When you use our web-based software, your data is stored on secure, always-updated, backed-up daily enterprise-class servers in a state-of-the-art, highly-secure data center. A key advantage is that all your data is centralized and accessible over the web from any computer at any time. 
					</div>
					<a href="javascript:void(0)" class="backtotop">Back to Top</a>
				</article>
				
				<article class="format-standard">
					<div class="entry-date"><div class="number">2</div></div>
					<div class="post-heading">
						<h4><a name="multipleusers">Multiple Users</a></h4>
						<div class="meta">
							<span class="user"><a href="#">For you and your company.</a></span>
						</div>
					</div>
					<div class="feature-image">
						<a href="<?php echo base_url();?>/misc/website/img/slides/users-large.jpg" data-rel="prettyPhoto">
							<img src="<?php echo base_url();?>/misc/website/img/slides/users-large.jpg" alt="Web based" />
						</a>
					</div>
					<div class="excerpt">
						With single or multiple user option, {_int} DMS is the perfect solution for individuals and companies. We offer Individual and Corporate packages based on your requirements.
						<br /><br />
						If you an Individual and you are looking for storing your documents in a secure central location, you can choose our Individual package.
						<br /><br />
						If you are representing an organisation or a team, you can choose our Corporate package to store your documents in a secure central location and share the documents with the team members or any staff in your organisation. 
					</div>
					<a href="javascript:void(0)" class="backtotop">Back to Top</a>
				</article>
				
				<article class="format-standard">
					<div class="entry-date"><div class="number">3</div></div>
					<div class="post-heading">
						<h4><a name="ssl">Transport Layer Security</a></h4>
						<div class="meta">
							<span class="user"><a href="#">Secure data transmission.</a></span>
						</div>
					</div>
					<div class="feature-image">
						<a href="<?php echo base_url();?>/misc/website/img/slides/ssl-large.jpg" data-rel="prettyPhoto">
							<img src="<?php echo base_url();?>/misc/website/img/slides/ssl-large.jpg" alt="Web based" />
						</a>
					</div>
					<div class="excerpt">
						{_int} DMS uses SSL technology to ensure that all of your information is encrypted. We use SSL technology to securely process your personal details, documents and all data transmitted.
						<br /><br />
						SSL (Secure Sockets Layer) is an industry standard for encrypting private data sent over the Internet. In other words, it helps keep your information safe!
						<br /><br />
						<h6>How to tell if SSL is being used</h6>
						Look for the "s" in the start of the Web address:
						<div class="lists-check">
						<ul>
							<li>SSL is being used: https://</li>
							<li>SSL is not being used: http://</li>
						</ul>
						</div>
						<h6>Why using SSL is important</h6>
						<div class="lists-check">
						<ul>
							<li>Helps to protect your account from hackers.</li>
							<li>Helps ensure security of all private data sent over the Internet, like credit cards and passwords.</li>
						</ul>
						</div>
		
					</div>
					<a href="javascript:void(0)" class="backtotop">Back to Top</a>
				</article>
				
				<article class="format-standard">
					<div class="entry-date"><div class="number">4</div></div>
					<div class="post-heading">
						<h4><a name="docsecurity">Document Security</a></h4>
						<div class="meta">
							<span class="user"><a href="#">Keep away from others, even from system/database administrator.</a></span>
						</div>
					</div>
					<div class="feature-image">
						<a href="<?php echo base_url();?>/misc/website/img/slides/doc-security-large.jpg" data-rel="prettyPhoto">
							<img src="<?php echo base_url();?>/misc/website/img/slides/doc-security-large.jpg" alt="Web based" />
						</a>
					</div>
					<div class="excerpt">
						Your documents are never stored in physical disc as it is. Even server/database administrator cannot view your plain documents. YES WE MEAN IT!
						<br /><br />
						We store all your documents in a secure MongoDB database and the administrators who are responsible for running the server/machine will never be able to view your plain documents. This will prevent any data centre staff from viewing your documents.
						<br /><br />
						Only our approved {_int} DMS administrators can login into the application and view your documents. This is for reviewing purpose only and our administrators will never perform any action on your documents.
					</div>
					<a href="javascript:void(0)" class="backtotop">Back to Top</a>
				</article>
				
				<article class="format-standard">
					<div class="entry-date"><div class="number">5</div></div>
					<div class="post-heading">
						<h4><a name="securelogin">Highly secure login</a></h4>
						<div class="meta">
							<span class="user"><a href="#">Encryption unique to you.</a></span>
						</div>
					</div>
					<div class="feature-image">
						<a href="<?php echo base_url();?>/misc/website/img/slides/secure-login-large.jpg" data-rel="prettyPhoto">
							<img src="<?php echo base_url();?>/misc/website/img/slides/secure-login-large.jpg" alt="Web based" />
						</a>
					</div>
					<div class="excerpt">
						We use unique encryption algorithm for each user store your password in a encrypted format which is SO SO unique to you.
						<br /><br />
						For example, if a user has password 'password' and lets say it is encrypted and saved as '04hjQubPF6dNQ' and another user uses the same password 'password' but it will be saved as '78s0lu8msIB.Y'. This will prevent anyone (even the Database Administrator, YES WE MEAN IT AGAIN!) from decrypting or guessing your password based on the encypted string. 
						<br /><br />
						AND we dont decrypt any encrypted passwords and sent to you in plain text through email. You can only update your password and set a new one.
					</div>
					<a href="javascript:void(0)" class="backtotop">Back to Top</a>
				</article>
      	
	        	<article class="format-standard">
					<div class="entry-date"><div class="number">6</div></div>
					<div class="post-heading">
						<h4><a name="search">Faster & Advanced Search</a></h4>
						<div class="meta">
							<span class="user"><a href="#">Find them quickly.</a></span>
						</div>
					</div>
					<div class="feature-image">
						<a href="<?php echo base_url();?>/misc/website/img/slides/search-large.jpg" data-rel="prettyPhoto">
							<img src="<?php echo base_url();?>/misc/website/img/slides/search-large.jpg" alt="Web based" />
						</a>
					</div>
					<div class="excerpt">
						Along with searching by document name, title, type, etc. you can also search using any text/word in your thousands of documents. This will help you in finding the document even if you dont remember the name and title of the document.
						<br /><br />
						We also provide a quick search option, where you can search using document name/title and jump straight to the document details page.
						<br /><br />
					</div>
					<a href="javascript:void(0)" class="backtotop">Back to Top</a>
				</article>
				
			</div>
			
			<aside id="sidebar">
					<ul>	
						<li class="block">
							<h4>Jump to</h4>
							<ul>
								<li class="cat-item"><a title="title" href="#webbased">Web Based</a></li>
								<li class="cat-item"><a title="title" href="#multipleusers">Multiple Users</a></li>
								<li class="cat-item"><a title="title" href="#ssl">Transport Layer Security</a></li>
								<li class="cat-item"><a title="title" href="#docsecurity">Document Security</a></li>
								<li class="cat-item"><a title="title" href="#securelogin">Highly secure login</a></li>
								<li class="cat-item"><a title="title" href="#search">Faster & Advanced Search</a></li>
							</ul>
						</li>
					</ul>
				</aside>
			
			
			
			<!-- ENDS 2 cols -->
		
    		

	<!-- Fold image -->
	<div id="fold"></div>
	</div>
	
</div>
<!-- ENDS MAIN -->
<script type='text/javascript'> $('.backtotop').click(function(){ $('html, body').animate({scrollTop:0}, 'slow'); }); </script>