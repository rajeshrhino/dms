<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_cookiepolicy_page_title');?></h1><span class="subheading"></span>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
		<!-- page content -->
      	<div id="page-content" class="clearfix">
          <div class="one-full">
          
            <h4 class="heading">What are Cookies?</h4>
              
			A cookie is a small file of letters and numbers that is downloaded on to your computer when you visit a website. Cookies are used by many websites and can do a number of things eg remembering your preferences, recording what you have put in your shopping cart, and counting the number of people looking at a website.
			<br /><br />
			The rules on cookies are covered by the Privacy and Electronic Communications Regulations. The Privacy and Electronic Communications Regulations (PECR) requires all UK website operators and owners to gain consent from its visitors for the use of cookies and other local storage technologies. 
			<br /><br />
			<h4 class="heading">Cookies on intdms.com</h4>
			At intdms.com we use cookies to track which pages are popular amongst our customers and to manage the backend application. We do not use cookies to store any personal data of any sort.
			<br /><br />  
			<table width="100%">
			<tbody>
			<tr>
				<td width="15%"><h6>Cookie</h6></td>
				<td width="15%"><h6>Name</h6></td>
				<td width="70%"><h6>Purpose</h6></td>
			</tr>
			<tr>
				<td><p>Cookie acceptance</p></td>
				<td><p>hmCookieAgreed</p></td>
				<td><p>This cookie is used to record if a user has accepted the use of cookies on intdms.com website.</p></td>
			</tr>
			<tr>
				<td><p>Google Analytics</p></td>
				<td><p>_utma<br>_utmb<br>_utmc<br>_utmz</p></td>
				<td><p>These cookies are used to collect information about how visitors use our site, which we use to help improve it. The cookies collect information in an anonymous form, including the number of visitors to the site, where visitors have come to the site from and the pages they visited.</p>
				<p><a href="http://www.google.com/intl/en_uk/analytics/privacyoverview.html" target="_new">Click here for an overview of privacy at Google</a></p></td>
			</tr>
			<tr>
				<td><p>DMS Application</p></td>
				<td><p>PHPSESSID</p></td>
				<td><p>This cookie is used to ensure the back end document management application works correctly.This cookie is deleted when you close your browser.</p></td>
			</tr>
			</tbody>
			</table>
			  
        </div>
		
    	</div>
    		<!-- ENDS page content -->
	<!-- Fold image -->
	<div id="fold"></div>
	</div>
</div>
<!-- ENDS MAIN -->
