<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_pricing_page_title');?></h1><span class="subheading"></span>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
		<!-- page content -->
      	<div id="page-content" class="clearfix">
            
          <h4 class="heading"><?php echo lang('label_signup_for_trial');?></h4>
          
          <div class="one-third">
          
              <h6><?php echo lang('label_why_register');?></h6>
              <br />  
              <div class="lists-check">
      			<ul>
      				<li><?php echo lang('label_why_register_point_1');?></li>
      				<li><?php echo lang('label_why_register_point_2');?></li>
      				<li><?php echo lang('label_why_register_point_3');?></li>
                    <li><?php echo lang('label_why_register_point_4');?></li>
                    <li><?php echo lang('label_why_register_point_5');?></li>
                    <li><?php echo lang('label_why_register_point_6');?></li>
      			</ul>
      		</div> 
        
        </div>
        
        <div>
            
            <table class="features-table">
				<thead>
					<tr>
						<td></td>
						<td>Individual</td>
						<td>Corporate</td>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td>Monthly</td>
						<td>&pound;TBC</td>
						<td>&pound;TBC</td>
					</tr>
				</tfoot>					
				<tbody>
                    <tr>
						<td>Storage</td>
						<td>2 GB</td>
						<td>5 GB</td>
					</tr>
					<tr>
						<td>Share Documents</td>
						<td><img src="<?php echo base_url();?>misc/website/img/check.png" alt="check"></td>
						<td><img src="<?php echo base_url();?>misc/website/img/check.png" alt="check"></td>
					</tr>
					<tr>
						<td>Unlimited support</td>
						<td><img src="<?php echo base_url();?>misc/website/img/check.png" alt="check"></td>
						<td><img src="<?php echo base_url();?>misc/website/img/check.png" alt="check"></td>
					</tr>
					<tr>
						<td>Multi-user</td>
						<td><img src="<?php echo base_url();?>misc/website/img/cross.png" alt="cross"></td>
						<td><img src="<?php echo base_url();?>misc/website/img/check.png" alt="check"></td>
					</tr>
                    <tr>
						<td></td>
						<td><?php echo anchor('register', 'Start Free Trial', array('class'=>'signup')); ?></td>
						<td><?php echo anchor('registercompany', 'Start Free Trial', array('class'=>'signup')); ?></td>
					</tr>
				</tbody>
		  </table>
          </div>
        
          <br />  
          <div>
            <h6 class="heading"><?php echo lang('label_after_trial_title');?></h6>
            <?php echo lang('label_after_trial_text');?>      
          </div>

    	</div>
    		<!-- ENDS page content -->
	<!-- Fold image -->
	<div id="fold"></div>
	</div>
</div>
<!-- ENDS MAIN -->
