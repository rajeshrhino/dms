<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_privacy_page_title');?></h1>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
		<!-- page content -->
      	<div id="page-content" class="clearfix">        	

			<div class="one-full">
				<h4 class="heading">General Information</h4>
				We collect the e-mail addresses of those who communicate with us via e-mail, aggregate information on what pages consumers access or visit, and information volunteered by the consumer (such as survey information and/or site registrations). The information we collect is used to improve the content of our Web pages and the quality of our service, and is not shared with or sold to other organizations for commercial purposes, except to provide products or services you've requested, when we have your permission, or under the following circumstances:
				<br/><br/>
				<div class="lists-check">
				<ul>
					<li>It is necessary to share information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of <?php echo anchor('terms','Terms of Service');?>, or as otherwise required by law.</li>
					<li>We transfer information about you if {_int} DMS is acquired by or merged with another company. In this event, {_int} DMS will notify you before information about you is transferred and becomes subject to a different privacy policy.</li>
				</ul>
				</div>
			</div>	
			
			<div class="one-full">
				<h4 class="heading">Information Gathering and Usage</h4>
				<div class="lists-check">
				<ul>
					<li>When you register for {_int} DMS we ask for information such as your name, email address, billing address, credit card information. Members who sign up for the free account are not required to enter a credit card.</li>
					<li>{_int} DMS uses collected information for the following general purposes: products and services provision, billing, identification and authentication, services improvement, contact, and research.</li>
				</ul>
				</div>
			</div>	
			<div class="clearfix"></div>
			
			<div class="one-full">
				<h4 class="heading">Cookies</h4>
				<div class="lists-check">
				<ul>
				<li>A cookie is a small amount of data, which often includes an anonymous unique identifier, that is sent to your browser from a web site's computers and stored on your computer's hard drive.</li>
				<li>Cookies are required to use the {_int} DMS service.</li>
				<li>We use cookies to record current session information, but do not use permanent cookies. You are required to re-login to your {_int} DMS account after a certain period of time has elapsed to protect you against others accidentally accessing your account contents.</li>
				</ul>
			</div>	
			<div class="clearfix"></div>
			
			<div class="one-full">
				<h4 class="heading">Data Storage</h4>
				{_int} DMS uses third party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to run {_int} DMS. Although {_int} DMS owns the code, databases, and all rights to the {_int} DMS application, you retain all rights to your data.
			</div>	
			<div class="clearfix"></div>
			
			<div class="one-full">
				<h4 class="heading">Disclosure</h4>
				{_int} DMS may disclose personally identifiable information under special circumstances, such as to comply with subpoenas or when your actions violate the <?php echo anchor('terms','Terms of Service');?>.
			</div>	
			<div class="clearfix"></div>
			
			<div class="one-full">
				<h4 class="heading">EU and Swiss Safe Harbor</h4>
				If you choose to provide {_int} DMS with your information, you consent to the transfer and storage of that information on our servers located in the India.
				<br/><br/>
				{_int} DMS adheres to the US-EU and US-Swiss Safe Harbor Privacy Principles of Notice, Choice, Onward Transfer, Security, Data Integrity, Access and Enforcement.
				<br/><br/>
				For European Union and Swiss residents, any questions or concerns regarding the use or disclosure of your information should be directed to {_int} DMS by sending an email to privacy@intdms.com. 
				We will investigate and attempt to resolve complaints and disputes regarding use and disclosure of your information in accordance with this Privacy Policy. 
				For complaints that cannot be resolved, and consistent with the Safe Harbor Enforcement Principle, we have committed to cooperate with data protection authorities located within Switzerland or the European Union (or their authorized representatives).
			</div>	
			<div class="clearfix"></div>
			
			<div class="one-full">
				<h4 class="heading">Changes</h4>
				{_int} DMS may periodically update this policy. We will notify you about significant changes in the way we treat personal information by sending a notice to the primary email address specified in your {_int} DMS primary account holder account or by placing a prominent notice on our site.
			</div>	
			<div class="clearfix"></div>
			
			<div class="one-full">
				<h4 class="heading">Questions</h4>
				Any questions about this Privacy Policy should be addressed to <?php echo mailto('support@intdms.com', 'support@intdms.com'); ?>.
			</div>
    	</div>
    	<!-- ENDS page content -->

	<!-- Fold image -->
	<div id="fold"></div>
	</div>
	
</div>
</div>
<!-- ENDS MAIN -->