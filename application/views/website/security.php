<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_security_page_title');?></h1>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
		<!-- page content -->
      	<div id="page-content" class="clearfix">        	

			<div class="one-full">
				We know your documents is extremely important to you and your business, and we're very protective of it.
			</div>	
			
			<div class="one-full">
				<h4 class="heading">Software Security</h4>
				We are commited to keep our software and its dependencies up to date eliminating potential security vulnerabilities. 
				We employ a wide range of monitoring solutions for preventing and eliminating attacks to the site.
			</div>	
			<div class="clearfix"></div>
			
			<div class="one-full">
				<h4 class="heading">Communications</h4>
				All private data exchanged with {_int} DMS is always transmitted over SSL (which is why your dashboard is served over HTTPS, for instance). All transfer of data is done over SSH authenticated with keys, or over HTTPS using your {_int} DMS username and password.
			</div>	
			<div class="clearfix"></div>
			
			<div class="one-full">
				<h4 class="heading">File system and backups</h4>
				Every document we store is saved on a minimum of three different servers, including an off-site backup just in case a meteor ever hits our data centers (we'll keep our fingers crossed that doesn't happen).
			</div>	
			<div class="clearfix"></div>
			
			<div class="one-full">
				<h4 class="heading">Employee access</h4>
				No {_int} DMS employees ever access private repositories unless required to for support reasons. 
				Staff working directly in the file store access the compressed {_int} DMS database, your document is never present as plaintext files like it would be in a local machine. 
			</div>	
			<div class="clearfix"></div>
			
			<div class="one-full">
				<h4 class="heading">Maintaining security</h4>
				We protect your login from brute force attacks with rate limiting. All passwords are filtered from all our logs and are one-way encrypted in the database. Login information is always sent over SSL.
			</div>	
			<div class="clearfix"></div>
			
			<div class="one-full">
				<h4 class="heading">Credit card safety</h4>
				When you sign up for a paid account on {_int} DMS, we do not store any of your card information on our servers. All payments are processed by <a href="http://paypal.com/">Paypal</a>, which is a global e-commerce business allowing payments to be made through the Internet. 
			</div>	
			<div class="clearfix"></div>
    	</div>
    	<!-- ENDS page content -->

	<!-- Fold image -->
	<div id="fold"></div>
	</div>
	
</div>
</div>
<!-- ENDS MAIN -->