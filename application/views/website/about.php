<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1>ABOUT</h1>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
		<!-- page content -->
      	<div id="page-content" class="clearfix">        	
      	
			<div class="one-full">
				Organisations produce piles of documents, images and other information electronically. The location of this information is a time consuming task. Users tend to file papers and to save documents in folders on their own computers. Nobody knows what information is across the company and what information is needed.
				<br /><br />
				{_int} DMS is a web base document management application that uses standards and Open Source technologies. {_int} DMS provides full document management capabilities including version control and file history, metadata, scanning, workflow, search, and more. It also allows the social activities around content to be used to connect people to other people, information to information, and people to information; helping to manage, more efficiently, the collective intelligence of the human resources of the company.
				<br /><br />
				{_int} DMS integrates all essential document management, collaboration and advanced search functionality into one easy to use solution
			</div>
		
			<!-- 2 cols -->
			<div class="one-half">
				<h4 class="heading">Why use {_int} DMS ?</h4>
			    <p>{_int} DMS builds a highly valuable repository of corporate information assets to facilitate knowledge creation and improve business decision making. The result is:</p>
				<div class="lists-check">
				<ul>
                <li>Improved productivity through shared practices</li>
                <li>Greater cost efficiencies</li>
                <li>Better customer relations, faster sales cycles</li>
                <li>Shortened product time-to-market, and better decision-making</li>
				</ul>
				</div>
			</div>
			
			<div class="one-half last">
				<h4 class="heading">Complex made easy</h4>
				<p>{_int} DMS  provides a one-stop solution by managing both structured and unstructured information. Use OpenKM is simple thanks to its intuitive and easy to use user interface that allows to:</p>
				<div class="lists-check">
                <ul>
                <li>Collect information from any digital source.</li>
                <li>Collaborate with colleagues on documents and projects.</li>
                <li>Empower organisations to capitalize on accumulated knowledge by locating documents, experts, and information sources.</li>
				</ul>
				</div>
			</div>
			<div class="clearfix"></div>
			<!-- ENDS 2 cols -->
			
			
			<!-- 3 
			<div class="one-third">
				<h4 class="heading">One third columns</h4>
				Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus
			</div>
			
			<div class="one-third">
				<h4 class="heading">One third columns</h4>
				Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus
			</div>
			
			<div class="one-third last">
				<h4 class="heading">One third columns</h4>
				Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus
			</div>
			<div class="clearfix"></div>
			
			
			<div class="one-fourth">
				<h4 class="heading">One fourth columns</h4>
				Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
			</div>
			
			<div class="one-fourth">
				<h4 class="heading">One fourth columns</h4>
				Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
			</div>
			
			<div class="one-fourth">
				<h4 class="heading">One fourth columns</h4>
				Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
			</div>
			
			<div class="one-fourth last">
				<h4 class="heading">One fourth columns</h4>
				Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
			</div>
			<div class="clearfix"></div>
			-->
			
			
		
    		</div>
    		<!-- ENDS page content -->
    	
    	

	<!-- Fold image -->
	<div id="fold"></div>
	</div>
	
</div>
<!-- ENDS MAIN -->