<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_credits_page_title');?></h1>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
		<!-- page content -->
      	<div id="page-content" class="clearfix">        	

			<div class="one-full">
				<p>{_int} DMS utilizes a number of Open Source technologies. This page is a list of those technologies, we give credit and acknowledge the important contributions from these developers.</p>
				<p><span class="highlight">NOTE:</span> All the below links open in new window.</p>
				<div class="post-heading">
				<h5><?php echo anchor('http://httpd.apache.org/', 'Apache HTTP Server Project' , array('target' => '_blank'));?></h5>
				<div class="meta">
					<span class="user">Copyright &copy; 1997-<?php echo date('Y');?> The Apache Software Foundation.</span>
				</div>
				</div>
				<br />
				<div class="post-heading">
				<h5><?php echo anchor('http://php.net/', 'PHP', array('target' => '_blank'));?></h5>
				<div class="meta">
					<span class="user">Copyright &copy; 2001-<?php echo date('Y');?> The PHP Group.</span>
				</div>
				</div>
				<br />
				<div class="post-heading">
				<h5><?php echo anchor('http://www.mongodb.org/', 'MongoDB', array('target' => '_blank'));?></h5>
				<div class="meta">
					<span class="user">Copyright &copy; <?php echo date('Y');?> MongoDB, Inc.</span>
				</div>
				</div>
				<!--<br />
				<div class="post-heading">
				<h5><?php echo anchor('http://ellislab.com/codeigniter', 'CodeIgniter', array('target' => '_blank'));?></h5>
				<div class="meta">
					<span class="user">&copy;2002–2013, EllisLab, Inc.</span>
				</div>
				</div>-->
			</div>
			</div>	
    	<!-- ENDS page content -->

	<!-- Fold image -->
	<div id="fold"></div>
	</div>
	
</div>
</div>
<!-- ENDS MAIN -->