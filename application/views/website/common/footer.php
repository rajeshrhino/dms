		<footer>	
			
			<div class="wrapper clearfix">
				
				<!-- widgets -->
				<!--<ul  class="widget-cols clearfix">
					<li class="first-col">
						
						<div class="widget-block">
							<h4>RECENT POSTS</h4>
							<div class="recent-post clearfix">
								<a href="#" class="thumb"><img src="<?php echo base_url();?>misc/website/img/dummies/54x54.gif" alt="Post" /></a>
								<div class="post-head">
									<a href="#">Pellentesque habitant morbi senectus</a><span> March 12, 2011</span>
								</div>
							</div>
							<div class="recent-post clearfix">
								<a href="#" class="thumb"><img src="<?php echo base_url();?>misc/website/img/dummies/54x54.gif" alt="Post" /></a>
								<div class="post-head">
									<a href="#">Pellentesque habitant morbi senectus</a><span> March 12, 2011</span>
								</div>
							</div>
							<div class="recent-post clearfix">
								<a href="#" class="thumb"><img src="<?php echo base_url();?>misc/website/img/dummies/54x54.gif" alt="Post" /></a>
								<div class="post-head">
									<a href="#">Pellentesque habitant morbi senectus</a><span> March 12, 2011</span>
								</div>
							</div>
						</div>
					</li>
					
					<li class="second-col">
						
						<div class="widget-block">
							<h4>FREE TEMPLATES &amp; THEMES</h4>
							<p>Visit <a href="home" >Template Creme</a> and browse the selection of well-made FREE Templates and WordPress Themes.</p>
						</div>
						
					</li>
					
					<li class="third-col">
						
						<div class="widget-block">
							<div id="tweets" class="footer-col tweet">
		         				<h4>TWITTER WIDGET</h4>
		         			</div>
		         		</div>
		         		
					</li>
					
					<li class="fourth-col">
						
						<div class="widget-block">
							<h4>FREE TEMPLATES &amp; THEMES</h4>
							<p>Visit <a href="home" >Template Creme</a> and browse the selection of well-made FREE Templates and WordPress Themes.</p>
						</div>
		         		
					</li>	
				</ul>-->
				<!-- ENDS widgets -->	
				
				
				<!-- bottom -->
				<div class="footer-bottom">
					<div class="left">
                            &copy; <?php echo date('Y'); ?> {_int} DMS
							<?php if (isset($is_maintenance) AND $is_maintenance == 1): ?>
								<!-- Dont display menus -->
							<?php else: ?>
                            <?php $menus = config_item('menus');?>
                            <?php foreach ( $menus['website_footer'] as $key => $value): ?>
                                 &nbsp;<?php echo anchor($value['link_to'], $value['label']); ?>
                            <?php endforeach; ?>
							<?php endif; ?>
                            <!--Template by <a href="http://www.luiszuno.com" >luiszuno.com</a>-->
                    </div>
					<div class="right">
						<ul id="social-bar">
							<li><a href="http://www.facebook.com"  title="Become a fan" class="poshytip"><img src="<?php echo base_url();?>misc/website/img/social/facebook.png"  alt="Facebook" /></a></li>
							<li><a href="http://www.twitter.com" title="Follow my tweets" class="poshytip"><img src="<?php echo base_url();?>misc/website/img/social/twitter.png"  alt="twitter" /></a></li>
							<li><a href="http://www.google.com"  title="Add to the circle" class="poshytip"><img src="<?php echo base_url();?>misc/website/img/social/plus.png" alt="Google plus" /></a></li>
						</ul>
					</div>
				</div>	
				<!-- ENDS bottom -->		

			</div>
		</footer>
		
			
	</body>
    <script>
    $(document).ready(function () {
        $.cookieCuttr({
            cookieAnalytics: false,
            cookieMessage: 'We use cookies on this website, you can <a href="{{cookiePolicyLink}}" title="read about our cookies">read about them here</a>. To use the website as intended please...',
            cookiePolicyLink: 'cookiepolicy'
        });
    }); 
    </script>

    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-36990433-1', 'auto');
	  ga('send', 'pageview');

	</script>
	
</html>