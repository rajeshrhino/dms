<!doctype html>
<html class="no-js">

	<head>
		<meta charset="utf-8"/>
		<title>{ _int } DMS - Intelligent Document Management System, a web base document management application</title>
		
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link rel="stylesheet" media="all" href="<?php echo base_url();?>misc/website/css/style.css"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<!-- Adding "maximum-scale=1" fixes the Mobile Safari auto-zoom bug: http://filamentgroup.com/examples/iosScaleBug/ -->
		
		<!-- JS -->
		<script src="<?php echo base_url();?>misc/website/js/jquery-1.6.4.min.js"></script>
        
		<script src="<?php echo base_url();?>misc/website/js/css3-mediaqueries.js"></script>
		<script src="<?php echo base_url();?>misc/website/js/custom.js"></script>
		<script src="<?php echo base_url();?>misc/website/js/tabs.js"></script>
		
		<!-- Tweet -->
		<link rel="stylesheet" href="<?php echo base_url();?>misc/website/css/jquery.tweet.css" media="all"  /> 
		<script src="<?php echo base_url();?>misc/website/js/tweet/jquery.tweet.js" ></script> 
		<!-- ENDS Tweet -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="<?php echo base_url();?>misc/website/css/superfish.css" /> 
		<script  src="<?php echo base_url();?>misc/website/js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script  src="<?php echo base_url();?>misc/website/js/superfish-1.4.8/js/superfish.js"></script>
		<script  src="<?php echo base_url();?>misc/website/js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- prettyPhoto -->
		<script  src="<?php echo base_url();?>misc/website/js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>misc/website/js/prettyPhoto/css/prettyPhoto.css"  media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="<?php echo base_url();?>misc/website/js/poshytip-1.1/src/tip-twitter/tip-twitter.css"  />
		<link rel="stylesheet" href="<?php echo base_url();?>misc/website/js/poshytip-1.1/src/tip-yellowsimple/tip-yellowsimple.css"  />
		<script  src="<?php echo base_url();?>misc/website/js/poshytip-1.1/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- GOOGLE FONTS -->
		<!--<link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>-->
		
		<!-- Flex Slider -->
		<link rel="stylesheet" href="<?php echo base_url();?>misc/website/css/flexslider.css" >
		<script src="<?php echo base_url();?>misc/website/js/jquery.flexslider-min.js"></script>
		<!-- ENDS Flex Slider -->
		
		<!-- Masonry -->
		<script src="<?php echo base_url();?>misc/website/js/masonry.min.js" ></script>
		<script src="<?php echo base_url();?>misc/website/js/imagesloaded.js" ></script>
		<!-- ENDS Masonry -->
		
		<!-- Less framework -->
		<link rel="stylesheet" media="all" href="<?php echo base_url();?>misc/website/css/lessframework.css"/>
		
		<!-- modernizr -->
		<script src="<?php echo base_url();?>misc/website/js/modernizr.js"></script>
		
		<!-- SKIN -->
		<link rel="stylesheet" media="all" href="<?php echo base_url();?>misc/website/css/skin.css"/>
        <link rel="stylesheet" media="all" href="<?php echo base_url();?>misc/website/css/custom.css"/>
        <link  rel="stylesheet" media="all" href="<?php echo base_url();?>misc/website/css/pricing.css">
        
        <link  rel="stylesheet" media="all" href="<?php echo base_url();?>misc/website/css/cookiecuttr.css">
        
        <script src="<?php echo base_url();?>misc/website/js/jquery.cookie.js"></script>
        <script src="<?php echo base_url();?>misc/website/js/jquery.cookiecuttr.js"></script>

        <link rel="icon" href="<?php echo base_url();?>misc/website/img/favicon.ico" type="image/x-icon">
        
	</head>
	<body lang="en">
	
		<header>
			<div class="wrapper clearfix">
				
				<div id="logo">
                    <?php 
                        echo anchor('home',img(array('src'=>'misc/website/img/logo.png','alt'=>'{ _int } DMS'))); 
                    ?>
				</div>
				<!--<div id="caption"><?php echo anchor('alphawarning' , 'alpha v0.1');?></div>-->
                
                <!-- nav -->
				<ul id="nav" class="sf-menu">
				<?php if (isset($is_maintenance) AND $is_maintenance == 1): ?>
					<!-- Dont display menus -->
				<?php else: ?>
					<?php $menus = config_item('menus');?>
					
					<!-- Switch label/link of login to logout, if user in logged in -->
					<?php if (isset($user_logged_in) AND $user_logged_in == 1): ?>
						<?php $menus['website']['login']['label'] = 'LOGOUT' ?>
						<?php $menus['website']['login']['link_to'] = 'logout' ?>
					<?php else: ?>	
						<?php unset($menus['website']['dashboard']); ?>
				    <?php endif; ?>
					
					<?php foreach ( $menus['website'] as $key => $value): ?>
					   <li<?php if ($this->uri->segment(1) == $value['key']): ?> class="current-menu-item"<?php endif; ?>><?php echo anchor($value['link_to'], $value['label']); ?>
					   <?php if (isset($value['child'])): ?>
							<ul>
							<?php foreach ( $value['child'] as $child_key => $child_value): ?>
								<li><?php echo anchor($child_value['link_to'], $child_value['label']); ?></li>
							<?php endforeach; ?>
							</ul>
					   <?php endif; ?>
					   </li>                     
					<?php endforeach; ?>
				<?php endif; ?>
                </ul>
				
				<!-- ends nav -->
				<!-- Maintenance mode message to Super Admins, if the site is operating in maintenance mode -->
				<?php if (isset($display_maintenance_mode_message) AND $display_maintenance_mode_message == 1): ?>
				<div class="clearfix"></div>
				<center><div class="maintenance-mode-warning">
					<?php echo lang('label_maintenance_mode_message'); ?>
				</div></center>
				<?php endif; ?>
                
				<!-- nav -->
				<!--<ul id="nav" class="sf-menu">
					<li class="current-menu-item"><a href="index.html">HOME</a></li>
					<li><a href="blog.html">BLOG</a></li>
					<li><a href="page.html">ABOUT</a>
						<ul>
							<li><a href="page-elements.html">Elements</a></li>
							<li><a href="page-icons.html">Icons</a></li>
							<li><a href="page-typography.html">Typography</a></li>
						</ul>
					</li>
					<li><a href="portfolio.html">WORK</a></li>
					<li><a href="contact.html">CONTACT</a></li>
					<li><a href="http://luiszuno.com/blog/downloads/simpler-template">DOWNLOAD NOW</a></li>
				</ul>-->
				<!-- ends nav -->
				
				<!-- comboNav -->
				<select id="comboNav">
					<?php foreach ( $menus['website'] as $key => $value): ?>
						<option value="<?php echo $value['link_to']; ?>" <?php if ($this->uri->segment(1) == $value['key']): ?> selected="selected"<?php endif; ?>><?php echo $value['label']; ?></option>
					<?php endforeach;?>
				</select>
				<!-- comboNav -->
				
                <?php if ($this->router->fetch_class() == 'home'): ?>
                <!-- slider holder -->
				<div class="clearfix"></div>
				<div id="slider-holder" class="clearfix">
					
					<!-- slider -->
			        <div class="flexslider home-slider">
    				  <ul class="slides">
    				    <li>
    				      <img src="<?php echo base_url();?>misc/website/img/slides/documents.jpg" alt="Apache" />
                          <p class="flex-caption">Manage your Documents Online</p>
    				    </li>
    				    <li>
    				      <img src="<?php echo base_url();?>misc/website/img/slides/secure.jpg" alt="MongoDB" />
                          <p class="flex-caption">Highly Secure Document Storage</p>
    				    </li>
                        <li>
    				      <img src="<?php echo base_url();?>misc/website/img/slides/search.jpg" alt="PHP" />
                          <p class="flex-caption">More Advanced Search Facilities</p>
    				    </li>
    				  </ul>
    				</div>
		        	<!-- ENDS slider -->
		        	
		        	<div class="home-slider-clearfix "></div>
		        	
		        	<!-- Headline -->
		        	<div id="headline">
		        		<p class="headline-text">Welcome to</p>
						<h4>INTELLIGENT DOCUMENT MANAGEMENT SYSTEM</h4>
						<p class="headline-text">A state of art document management system.</p>
	        		    <p class="headline-text">Share, Store and Manage your Documents and Files Online.</a></p>
	        		    <div class="clearfix"></div>
	        		    <ul>
							<li><?php echo anchor('pricing', lang('label_new_user_link'), array('class'=>'signup'));?></li>
						</ul>
						<!--<div class="lists-heart">
						<ul>		
							<li>Share files with colleagues/clients anywhere in the world!</li>
							<li>Transfer files between work and home.</li>
							<li>Your files are securely stored.</li>
						</ul>
						</div>-->
		        	</div>
		        	<!-- ENDS headline -->
		        	
				</div>
				<!-- ENDS slider holder -->
                
                <?php endif;?>
				
			</div>
		</header>