<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1><?php echo lang('label_alphawarning_page_title');?></h1>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
		<!-- page content -->
      	<div id="page-content" class="clearfix">        	

			<div class="one-full">
				<p>{_int} DMS Alpha Version 0.1 software package should be considered "alpha". This means that the software is still undergoing internal testing and is not considered appropriate for customer use. 
				Any problems arising from using the {_int} DMS software are the responsibility of the customer. This software will continue to be released as "alpha" until its code and schemas are considered stable ans secure.</p>

				<p>The {_int} DMS development team understands the importance of reliable software as well protecting user investments through the creation of a solid application that is stable and secure.</p>

				<p>Until then, {_int} DMS application is not supported for production use.</p>

				<p><span class="alphawarning">YOU HAVE BEEN WARNED.</span></p>
				
				<p>If you would like to participate in the testing process, please get in touch using the <?php echo anchor('contact' , lang('label_contact_form_title'));?>, and if possible we will include you in the beta testing process.</p>
			</div>	
    	</div>
    	<!-- ENDS page content -->

	<!-- Fold image -->
	<div id="fold"></div>
	</div>
	
</div>
</div>
<!-- ENDS MAIN -->