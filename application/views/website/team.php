<!-- MAIN -->
<div id="main">	
	<div class="wrapper clearfix">

		<!-- masthead -->
		<div class="masthead clearfix">
			<h1>TEAM</h1>
		</div>
		<div class='mh-div'></div>
		<!-- ENDS masthead -->
		
		<!-- page content -->
      	<div id="page-content" class="clearfix">        	
      	
				<p>
					<span class="pullquote-left"><img src="<?php echo base_url();?>misc/website/img/user_128.png" alt="Rajesh"/></span> 
					<h4 class="heading">Rajesh</h4>Project Manager, Lead Developer, Webmaster.
					<br /><br /><a href="mailto:rajeshrhino@gmail.com">Contact Rajesh</a> 
				</p>
				<div class="block-divider"></div>
				<p>
					<span class="pullquote-left"><img src="<?php echo base_url();?>misc/website/img/user_128.png" alt="Boby"/></span> 
					<h4 class="heading">Boby</h4><br />Project Architect, Lead Developer, Webmaster.
					<br /><br /><a href="mailto:boby.pv@gmail.com">Contact Boby</a> 
				</p>

		
    	</div>
    	<!-- ENDS page content -->

	<!-- Fold image -->
	<div id="fold"></div>
	</div>
	
</div>
<!-- ENDS MAIN -->