<!-- MAIN -->
<div id="main">	
    <div class="wrapper clearfix">

        <!-- masthead -->
        <div class="masthead clearfix">
            <h1>CONTACT</h1><span class="subheading">Get in touch</span>
        </div>
        <div class='mh-div'></div>
        <!-- ENDS masthead -->

        <!-- page content -->
        <div id="page-content" class="clearfix"> 

            <!-- GOOGLE MAPS -->
            <!--<script type="text/javascript" src="http://www.google.com/jsapi?key=9OPxl8LsanPxgusaqi9Cl9Nu" ></script>-->
            <!--<script src="//maps.googleapis.com/maps/api/js?v=3&client=9OPxl8LsanPxgusaqi9Cl9Nu&sensor=true" type="text/javascript"></script>
            <script type="text/javascript" >google.load("maps","3.x");</script>

            <script type="text/javascript">

                jQuery(document).ready(function($) {

                //##########################################
                // Google maps
                //##########################################

                // You can get the latitud and Longitude values at http://itouchmap.com/latlong.html

                var latitude = 44.8011821;
                var longitude = -68.7778138;

                // center map
                var map = new GMap2(document.getElementById("map")); 
                var point = new GLatLng(latitude, longitude); 
                map.setCenter(point, 16);

                // Set marker
                marker = new GMarker(point); 
                map.addOverlay(marker);

                // controls
                map.addControl(new GLargeMapControl());

                });
            </script>-->
            <h4 class="heading">Location</h4>
            <div id="map"></div>
            <!-- ENDS GOOGLE MAPS -->

            <div class='section-div'></div>

            <!-- 2 cols -->
            <div class="left-page-block">
                <h4 class="heading"><?php echo lang('label_contact_form_title');?></h4>
                <!-- form -->
                <script src="<?php echo base_url();?>misc/website/js/form-validation.js" ></script>
                <?php echo form_open('contact/send_message' , array('id' => 'contactForm', 'class' => 'niceForm'));?>
                    
                    <p><?php echo lang('message_contact_us_form');?></p>
                    
                    <fieldset>
                        <div>
                            <?php echo form_input(array('name'=>'name', 'id'=>'name', 'data-icon'=>'U', 'title'=>lang('label_full_name_title'), 'value' => '', 'class'=> 'form-poshytip'))?>    
                            <label><?php echo lang('label_full_name' , 'name');?></label>
                            <span class="error-message"><?php echo form_error('name'); ?></span>
                        </div>
                        <div>
                            <?php echo form_input(array('name'=>'email', 'id'=>'email', 'data-icon'=>'U', 'title'=>lang('label_full_email_title'), 'value' => '', 'class'=> 'form-poshytip'))?>    
                            <label><?php echo lang('label_email' , 'email');?></label>
                            <span class="error-message"><?php echo form_error('email'); ?></span>
                        </div>
                        <!--<div>
                            <input name="web"  id="web" type="text" class="form-poshytip" title="Enter your website" />
                            <label>Website</label>
                        </div>-->
                        <div>
                            <?php echo form_textarea(array('name'=>'comments', 'id'=>'comments', 'data-icon'=>'U', 'title'=>lang('label_comments_title'), 'value' => '', 'rows' => 5 , 'cols' => 20, 'class'=> 'form-poshytip'))?>    
                            <span class="error-message"><?php echo form_error('comments'); ?></span>
                        </div>

                        <!-- send mail configuration -->
                        <!--<input type="hidden" value="your@email.com" name="to" id="to" />
                        <input type="hidden" value="Enter the subject here" name="subject" id="subject" />
                        <input type="hidden" value="send-mail.php" name="sendMailUrl" id="sendMailUrl" />-->
                        <!-- ENDS send mail configuration -->

                        <p><?php echo form_submit(array('name'=>'submit', 'value'=>lang('label_send'), 'id' => 'submit', 'class'=>'enviar'))?> <span class="error-message" id="error">Message</span> </p>
                    </fieldset>

                <?php echo form_close(); ?>
                <p id="sent-form-msg" class="success"><?php echo lang('message_contact_us_form_thank_you');?></p>
                <!-- ENDS form -->
            </div>

            <div class="right-page-block">
                <h4 class="heading">Address info</h4>
                
                <p><?php echo lang('message_contact_us_form_alt_msg');?></p>
                
                <!--<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor si tincidunt quis, accumsan porttitor, facilisis luctus, mets.</p>-->

                <ul class="address-block">
                    <!--<li class="address">Address line, city, state ZIP</li>
                    <li class="phone">+123 456 789</li>
                    <li class="mobile">+123 456 789</li>-->
                    <li class="email"><a href="mailto:email@server.com">info@intdms.com</a></li>
                </ul>
            </div>
            <!-- ENDS 2 cols -->

        </div>
        <!-- ENDS page content -->

    <!-- Fold image -->
    <div id="fold"></div>
    </div>

</div>
<!-- ENDS MAIN -->
<!--<script type="text/javascript">
    // hide messages 
	$("#error").hide();
	$("#sent-form-msg").hide();
</script>-->