			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
					<?php foreach ( $breadcrumbs as $key => $value): ?>
							<li><?php echo anchor($value[1], $value[0]); ?><span class="divider">/</span></li>
					<?php endforeach; ?>
					<li class="active"><?php echo lang('label_upload');?></li>
				</ul>	
					
			</div>
			
			<?php if (isset($message) && ($message != '')): ?>  
				<div class="alert alert-error">
							<button data-dismiss="alert" class="close" type="button">×</button>
							<?php echo $message; ?>
				</div>
            <?php endif; ?>
			
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-upload"></i> <?php echo lang('label_file_upload');?></h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
					
						<h3><small><?php echo lang('label_upload_to');?></small> <?php echo $folder_name;?> <small><?php echo lang('label_folder');?></small></h3>
					
						<?php $hidden = array('folder_id' => $folder_id);?>
					
						<?php echo form_open_multipart('document/document/uploadfile', array('id' => 'documentupload' , 'class' => 'dropzone'), $hidden);?>
						
						<script language="javascript">

						  init: function() {
							this.on("success", function(file, responseText) {
							  // Handle the responseText here. For example, add the text to the preview element:
							  file.previewTemplate.appendChild(document.createTextNode(responseText));
							});
						  }

						</script>
						
						<?php echo form_close(); ?>   
						<p><?php echo lang('label_file_upload_finished');?></p>
						<?php echo anchor('document/'.$folder_id.'/list' , '<i class="icon-ok"></i> '.lang('label_done') , array('class'=>"btn btn-large") )?>
						
					</div>
					
					
					
				</div><!--/span-->

			</div><!--/row-->

			
    
					<!-- content ends -->
			</div><!--/#content.span10-->