<div id="content" class="span10">
			<!-- content starts -->

			<div>
				<ul class="breadcrumb">
					<?php $final_breab_crumb = max(array_keys($breadcrumbs)); ?>
					<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
					<?php foreach ( $breadcrumbs as $key => $value): ?>
						<?php if ($key == $final_breab_crumb): ?>
							<li class="active"><?php echo $value[0]; ?></li>
						<?php else: ?>
							<li><?php echo anchor($value[1], $value[0]); ?></li>
						<?php endif; ?>
						<?php if ($key != $final_breab_crumb) echo '<span class="divider">/</span></li>';?>
					<?php endforeach; ?>
				
				</ul>
			</div>
			
			<?php if (isset($error_message) && ($error_message != '')): ?>  
			<!--<div class="alert alert-error">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<?php echo $error_message;?>
			</div>-->
			<a href='#' style="display:none;" id="error_message_link" data-noty-options='{"text":"<?php echo $error_message;?>", "layout":"topCenter","type":"error"}' class="btn btn-primary noty"></a>
			<?php elseif (isset($message) && ($message != '')): ?>  
			<!--<div class="alert alert-success">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<?php echo $message;?>
			</div>-->
			<a href='#' style="display:none;" id="success_message_link" data-noty-options='{"text":"<?php echo $message;?>","layout":"topCenter","type":"success"}' class="btn btn-primary noty"></a>
			<?php endif; ?>
			
			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-file"></i> <?php echo $current_folder_name; ?></h2>
						
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
						</div>
						
					</div>

					
					<div class="box-content">

						<div style="margin-bottom: 9px" class="btn-toolbar">
								<div class="btn-group">
								  <?php echo anchor('#' , '<i class="icon-folder-open"></i> '.lang('label_new_folder') , array('class'=>"btn" , 'id'=>'create_new_folder_button') )?>
								</div>
								<div class="btn-group">
								  <?php echo anchor('document/'.$folder_id.'/upload' , '<i class="icon-upload"></i> '.lang('label_upload') , array('class'=>"btn") )?>
								</div>
								<div class="btn-group">
								  <a href="#" class="btn btn-primary"><i class="icon-tasks icon-white"></i> <?php echo lang('label_selected_items');?></a>
								  <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></a>
								  <ul class="dropdown-menu">
									<!--<li><a href="#"><?php echo anchor('#' , lang('label_copy_to'), array('onClick'=>'return submit_mass_action_form(1);')); ?></a></li>
									<li><a href="#"><?php echo anchor('#' , lang('label_move_to'), array('onClick'=>'return submit_mass_action_form(2);')); ?></a></li>-->
									<li><a href="#"><?php echo anchor('#' , lang('label_delete'), array('onClick'=>'return submit_mass_action_form(3);')); ?></a></li>
								  </ul>
								</div>
							  </div>
					
						
						<?php echo form_open('document/documentlist/mass_action' , array('id' => 'massActionForm', 'class' => 'form-horizontal'));?>
						
						<input type="hidden" id="mass_action" name="mass_action" value="" />
						
						<input type="hidden" id="parent_id" name="parent_id" value="<?php echo $folder_id; ?>" />
						
						<br />
						<?php echo $table; ?>
						<?php echo form_close(); ?>
					
						            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
    
					<!-- content ends -->
			</div><!--/#content.span10-->