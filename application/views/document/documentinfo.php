<div id="content" class="span10">
			<!-- content starts -->

			<div>
				<ul class="breadcrumb">
					
					<li><?php echo anchor('edocs/dashboard/home' , lang('label_home'))?><span class="divider">/</span></li>
					<?php foreach ( $breadcrumbs as $key => $value): ?>
						<li><?php echo anchor($value[1], $value[0]); ?></li><span class="divider">/</span></li>
					<?php endforeach; ?>
					<li class="active"><?php echo $file_name;?></li>
				</ul>
			</div>
			
			<?php if (isset($message) && ($message != '')): ?>  
				<ul class="noty_cont noty_layout_topLeft">
					<div class="noty_message"><span class="noty_text"><?php echo $message; ?></span></div>
				</ul>
            <?php endif; ?>
			
			<input type="hidden" id="folder_id" name="folder_id" value="<?php echo $folder_id; ?>" />
			
			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-info-sign"></i> <?php echo lang('label_file_info');?></h2>
						
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
						</div>
						
					</div>
					
					<div class="box-content">

						<h2><?php echo $file_name; ?></h2>
						
						<div class="tooltip-demo well">
							<p style="margin-bottom: 0;" class="muted"><?php echo lang('label_created_by');?> <a data-original-title="<?php echo $name;?></a>" data-rel="tooltip" href="#"><?php echo $name;?></a> <?php echo lang('label_on');?> <?php echo $uploaded_date; ?>.</p>
							
							<br />
							
							<?php echo anchor('document/documentlist/download/'.$file_id , '<i class="icon-arrow-down"></i> '.lang('label_download') , array('class'=>"btn btn-small" , 'id'=>'icon-arrow-down') )?>&nbsp;
							<?php echo anchor('document/documentlist/delete/'.$folder_id.'/'.$file_id , '<i class=" icon-remove"></i> '.lang('label_delete') , array('class'=>"btn btn-small" , 'onClick' => "return confirm('".lang('message_delete_confirm')."')") )?>
							
						</div>
						            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
    
					<!-- content ends -->
			</div><!--/#content.span10-->