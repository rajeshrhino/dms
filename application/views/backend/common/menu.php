<!-- left menu starts -->
<div class="span2 main-menu-span">
	<div class="well nav-collapse sidebar-nav">
		<ul class="nav nav-tabs nav-stacked main-menu">
        
			<?php $menus = config_item('menus'); ?>
    	    <?php foreach ( $menus['backend'] as $key => $value): ?>
				<?php if (in_array($role_id , $menus['backend'][$key]['roles'])): ?>
					<li class="nav-header hidden-tablet"><?php echo $key; ?></li>
				<?php endif; ?>
                <?php foreach ( $value as $child_key => $child_value): ?>
					<?php if (is_int($child_key) AND in_array($role_id , $child_value['roles'])): ?>
                    <?php $link_text = '<i class="'.$child_value['class'].'"></i><span class="hidden-tablet">  '.$child_value['label'].'</span>'; ?> 
                    <li><?php echo anchor($child_value['link_to'] , $link_text , array('class' => 'ajax-link'));?></li>
					<?php endif; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>
            
		</ul>
		<!--<label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>-->
	</div><!--/.well -->
</div><!--/span-->
<!-- left menu ends -->

<noscript>
<div class="alert alert-block span10">
	<h4 class="alert-heading">Warning!</h4>
	<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
</div>
</noscript>
<div id="content" class="span10">
<!-- content starts -->