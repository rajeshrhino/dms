<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>{ _int } DMS Administration</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="{ _int } DMS Administration.">
	<meta name="author" content="rajeshrhino@gmail.com">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	  .ui-autocomplete-loading {
    	background: white url('images/ui-anim_basic_16x16.gif') right center no-repeat;
  		}
	</style>
		
	<?php echo load_cssjs('backend','css')?>
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="icon" href="<?php echo base_url();?>misc/website/img/favicon.ico" type="image/x-icon">
</head>

<body>
		<!-- topbar starts -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<?php 
                    echo anchor('edocs',img(array('src'=>'misc/website/img/logo.png','alt'=>'{ _int } DMS')),array('class'=>'brand'));
                ?>
                <!--<div id="caption"><?php echo anchor('alphawarning' , 'alpha v0.1');?></div>-->
				
				<!-- <a class="brand" href="home.html"><?php //echo img(array('src'=>'misc/website/img/logo.png','alt'=>'{ _int } DMS'))?></a> -->
                                                
				<!-- theme selector starts -->
				<!--<div class="btn-group pull-right theme-container" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-tint"></i><span class="hidden-phone"> Change Theme / Skin</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" id="themes">
						<li><a data-value="classic" href="#"><i class="icon-blank"></i> Classic</a></li>
						<li><a data-value="cerulean" href="#"><i class="icon-blank"></i> Cerulean</a></li>
						<li><a data-value="cyborg" href="#"><i class="icon-blank"></i> Cyborg</a></li>
						<li><a data-value="redy" href="#"><i class="icon-blank"></i> Redy</a></li>
						<li><a data-value="journal" href="#"><i class="icon-blank"></i> Journal</a></li>
						<li><a data-value="simplex" href="#"><i class="icon-blank"></i> Simplex</a></li>
						<li><a data-value="slate" href="#"><i class="icon-blank"></i> Slate</a></li>
						<li><a data-value="spacelab" href="#"><i class="icon-blank"></i> Spacelab</a></li>
						<li><a data-value="united" href="#"><i class="icon-blank"></i> United</a></li>
					</ul>
				</div>-->
				<!-- theme selector ends -->
				
				<!-- user dropdown starts -->
				<div class="btn-group pull-right" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i><span class="hidden-phone"> <?php echo $name;?></span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><?php echo anchor('accountsettings' , '<i class=" icon-cog"></i> '.lang('label_account_settings'))?></li>
						<li class="divider"></li>
						<li><?php echo anchor('logout' , '<i class=" icon-exclamation-sign"></i> '.lang('label_logout'))?></li>
					</ul>
				</div>
				<!-- user dropdown ends -->
				
				<div class="top-nav nav-collapse">
					<ul class="nav">
						<li>
						<div class="ui-widget">
							<form  id="quickSearchForm" class="navbar-search pull-left" action="<?php echo site_url('edocs/quicksearch')?>">
								<input id="quickFileSearch" placeholder="Search" class="search-query span2" name="query" type="text">
							</form>
						</div>	
						</li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>

		<!-- Maintenance mode message to Super Admins, if the site is operating in maintenance mode -->
		<?php if (isset($display_maintenance_mode_message) AND $display_maintenance_mode_message == 1): ?>
		<br />			
		<center><div class="alert alert-success">
			<?php echo lang('label_maintenance_mode_message'); ?>
		</div></center>
		<?php endif; ?>

	</div>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
		

        