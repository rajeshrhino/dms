<footer>

</footer>
		
	</div><!--/.fluid-container-->
	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<?php echo load_cssjs('backend','js')?>

<script src="<?php echo base_url();?>misc/backend/js/dropzone.js" type="text/javascript"></script>
<link rel="stylesheet" media="all" href="<?php echo base_url();?>misc/backend/css/dropzone.css"/>
<link rel="stylesheet" media="all" href="<?php echo base_url();?>misc/backend/css/dms.css"/>

<script>
// Disable auto discover for all elements:
Dropzone.maxFilesize = 2;
</script>

<!-- Create New Folder Modal - Start -->
<div class="modal hide fade" id="createNewFolderModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3><?php echo lang('label_new_folder');?></h3>
	</div>
	<div class="modal-body">
		<?php echo form_open('createfolder' , array('id' => 'createFolderForm', 'class' => 'create-folder form-horizontal'));?>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_folder_name');?><span class="form-required" title="This field is required.">*</span></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'folder_name', 'id' => 'folder_name' , 'title' => lang('label_folder_name_title')));?>
					<span id="folder_name_error_message" class="help-inline" style="display:none;"><?php echo lang('folder_name_mandatory');?></span>
					<span id="folder_name_already_exists" class="help-inline" style="display:none;"><?php echo lang('folder_name_exists');?></span>
				  </div>
				  <!--<p class="help-block"><?php echo lang('label_folder_name_help_text');?></p>-->
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_folder_title');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'folder_title', 'id' => 'folder_title' , 'title' => lang('label_folder_title_title')));?>
				  </div>
				  <!--<p class="help-block"><?php echo lang('label_folder_name_help_text');?></p>-->
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_folder_description');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_textarea(array('name' => 'folder_description', 'id' => 'folder_description' , 'title' => lang('label_folder_description_title') , 'rows' => 3));?>
				  </div>
				  <!--<p class="help-block"><?php echo lang('label_folder_name_help_text');?></p>-->
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
	<div class="modal-footer">
		<a href="#" id="save_new_folder" class="btn btn-primary"><?php echo lang('label_save');?></a>
		<a href="#" class="btn" data-dismiss="modal"><?php echo lang('label_cancel');?></a>
	</div>
</div>

<div class="modal hide fade" id="editFolderModal">
	<input type="hidden" id="folder_id" name="folder_id" value="" />
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3><?php echo lang('label_edit_folder');?></h3>
	</div>
	<div class="modal-body">
		<?php echo form_open('editfolder' , array('id' => 'editFolderForm', 'class' => 'create-folder form-horizontal'));?>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_folder_name');?><span class="form-required" title="This field is required.">*</span></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'edit_folder_name', 'id' => 'edit_folder_name' , 'title' => lang('label_folder_name_title')));?>
					<span id="edit_folder_name_error_message" class="help-inline" style="display:none;"><?php echo lang('folder_name_mandatory');?></span>
					<span id="edit_folder_name_already_exists" class="help-inline" style="display:none;"><?php echo lang('folder_name_exists');?></span>
				  </div>
				  <!--<p class="help-block"><?php echo lang('label_folder_name_help_text');?></p>-->
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_folder_title');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_input(array('name' => 'edit_folder_title', 'id' => 'edit_folder_title' , 'title' => lang('label_folder_title_title')));?>
				  </div>
				  <!--<p class="help-block"><?php echo lang('label_folder_name_help_text');?></p>-->
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo lang('label_folder_description');?></label>
				<div class="controls">
				  <div class="input-prepend">
					<?php echo form_textarea(array('name' => 'edit_folder_description', 'id' => 'edit_folder_description' , 'title' => lang('label_folder_description_title') , 'rows' => 3));?>
				  </div>
				  <!--<p class="help-block"><?php echo lang('label_folder_name_help_text');?></p>-->
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
	<div class="modal-footer">
		<a href="#" id="save_edit_folder" class="btn btn-primary"><?php echo lang('label_save');?></a>
		<a href="#" class="btn" data-dismiss="modal"><?php echo lang('label_cancel');?></a>
	</div>
</div>

<div id="deleteFolderModal" class="modal hide fade">
	<input type="hidden" id="delete_folder_id" name="delete_folder_id" value="" />
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button">×</button>
		<h3><?php echo lang('label_delete_folder');?></h3>
	</div>
	<div class="modal-body">
		<?php echo form_open('editfolder' , array('id' => 'editFolderForm', 'class' => 'create-folder form-horizontal'));?>
		
		<div class="alert alert-error" id="delete_folder_error_div" style="display: none;">
			<i class="icon32 icon-color icon-cross"></i> <strong>'<span id="delete_folder_name"></span>'</strong> <?php echo lang('cannot_delete_folder');?>
		</div>
		<div class="alert alert-error" id="delete_folder_confirmation_message" style="display: none;">
			<i class="icon32 icon-color icon-alert"></i> <?php echo lang('message_delete_folder_confirm');?>
		</div>
		<?php echo form_close(); ?>
	</div>
	<div class="modal-footer">
		<a class="btn btn-primary" id="save_delete_folder" href="#"><?php echo lang('label_yes');?></a>
		<a data-dismiss="modal" class="btn" href="#"><?php echo lang('label_cancel');?></a>
	</div>
</div>

<div id="deletefilesModal" class="modal hide fade">
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button">×</button>
		<h3><?php echo lang('label_confirm_delete');?></h3>
	</div>
	<div class="modal-body">
		<?php echo form_open('deletefiles' , array('id' => 'deletefiles', 'class' => 'create-folder form-horizontal'));?>
		
		<div class="alert alert-error" id="delete_folder_error_div">
			<i class="icon32 icon-color icon-alert"></i> <?php echo lang('message_delete_confirm_multiple');?>
		</div>
		<?php echo form_close(); ?>
	</div>
	<div class="modal-footer">
		<a class="btn btn-primary" id="save_delete_files_multiple" href="#"><?php echo lang('label_yes');?></a>
		<a data-dismiss="modal" class="btn" href="#"><?php echo lang('label_cancel');?></a>
	</div>
</div>

<script type="text/javascript">
jQuery(function($) {
	$('#create_new_folder_button').click(function(e){
		e.preventDefault();
		
		clear_errors();
		
		// Reset the values in the modal to empty
		$('#folder_name').val('');
		$('#folder_title').val('');
		$('#folder_description').val('');
		
		// Show the modal
		$('#createNewFolderModal').modal('show');
	});
	
	$('#save_new_folder').click(function(e){
		if ($('#folder_name').val() == '')
		{
			$('#folder_name').parent().parent().parent().attr('class', 'control-group error');
			$('#folder_name').attr('class', 'inputError');
			$('#folder_name_error_message').show();
		}
		else
		{
			// Create the folder using AJAX
			create_folder();
			
		}
	});
	
	$('#save_edit_folder').click(function(e){
		if ($('#edit_folder_name').val() == '')
		{
			$('#edit_folder_name').parent().parent().parent().attr('class', 'control-group error');
			$('#edit_folder_name').attr('class', 'inputError');
			$('#edit_folder_name_error_message').show();
		}
		else
		{
			// Update the folder using AJAX
			save_edit_folder();
			
		}
	});
	
	$('#save_delete_folder').click(function(e){
		// Delete the folder using AJAX
		save_delete_folder();
	});
	
	$("#folder_name").focusout(function() {
		if ($('#folder_name').val() != '') {
			clear_errors();
			check_if_folder_exist();
		}
		else {
			clear_errors();
		}
	});
	
	$("#edit_folder_name").focusout(function() {
		if ($('#edit_folder_name').val() != '') {
			clear_edit_errors();
			check_if_folder_exist_for_edit();
		}
		else {
			clear_edit_errors();
		}
	});
});

// EDIT FOLDER: Function trigged by the 'Edit' link for folder
function edit_folder(folder_id, name , title , description){

	clear_edit_errors();
	
	// Reset the values in the modal to empty
	$('#edit_folder_name').val(name);
	$('#edit_folder_title').val(title);
	$('#edit_folder_description').val(description);
	$('#folder_id').val(folder_id);
	
	// Show the modals
	$('#editFolderModal').modal('show');
	
	return false;
}

// DELETE FOLDER: Function trigged by the 'Delete' link for folder
function delete_folder(folder_id , folder_name){

	clear_delete_errors();
	
	check_if_delete_allowed(folder_id);
		
	// Reset the values in the modal to empty
	$('#delete_folder_id').val(folder_id);
	$('#delete_folder_name').html(folder_name);
	
	// Show the modals
	$('#deleteFolderModal').modal('show');
	
	return false;
}

/* Functions to clear already existing errors when the modal is loaded */

// CREATE FOLDER: Function to clear errors
function clear_errors() {
	// Reset the error messages in the modal, if any
	$('#folder_name').parent().parent().parent().attr('class', 'control-group');
	$('#folder_name').attr('class', '');
	$('#folder_name_error_message').hide();
	$('#folder_name_already_exists').hide();
}

// EDIT FOLDER: Function to clear errors
function clear_edit_errors() {
	// Reset the error messages in the modal, if any
	$('#edit_folder_name').parent().parent().parent().attr('class', 'control-group');
	$('#edit_folder_name').attr('class', '');
	$('#edit_folder_name_error_message').hide();
	$('#edit_folder_name_already_exists').hide();
}

// DELETE FOLDER: Function to clear errors
function clear_delete_errors() {
	// Reset the error messages in the modal, if any
	$('#delete_folder_error_div').hide();
	$('#delete_folder_confirmation_message').show();
	$('#save_delete_folder').show();
	$('#delete_folder_name').html('');
}

/* Functions to check/validate whether a folder can be added/edited/delete  */

// CREATE FOLDER: Function to check if the folder already exist in the current path/directory
function check_if_folder_exist(){
	var parent_id = $('#parent_id').val();
		
	var folder_name = $('#folder_name').val();
	
	//use ajax to save the folder
	$.ajax({url: "<?php echo site_url("document/folder/check_folder_exists")?>", data : { folder_name: folder_name , parent_id:parent_id },
		type: "POST",
		success: function(result){
			check_folder_action(result);
		}
	});
}

// EDIT FOLDER: Function to check if any other folder exist in the current path/directory with the same name of the edited folder
function check_if_folder_exist_for_edit(){
	var parent_id = $('#parent_id').val();
	
	var folder_id = $('#folder_id').val();
		
	var folder_name = $('#edit_folder_name').val();
	
	//use ajax to save the folder
	$.ajax({url: "<?php echo site_url("document/folder/check_folder_exists_for_edit")?>", data : { folder_name: folder_name , folder_id:folder_id , parent_id:parent_id },
		type: "POST",
		success: function(result){
			check_folder_action_for_edit(result);
		}
	});
}

// DELETE FOLDER: Function to check if a folder can be deleted. No subfolder or files should exist inside the folder
function check_if_delete_allowed(folder_id){
	
	//use ajax to save the folder
	$.ajax({url: "<?php echo site_url("document/folder/check_if_sub_folder_or_file_exists")?>", data : { folder_id:folder_id },
		type: "POST",
		success: function(result){
			check_folder_action_for_delete(result);
		}
	});
}

/* Functions to add/edited/delete a folder */

// CREATE FOLDER: Function to create folder
function create_folder(){

    //get the folder details
    var folder_name = $('#folder_name').val();
	var folder_title = $('#folder_title').val();
	var folder_description = $('#folder_description').val();
	var parent_id = $('#parent_id').val();
    
	var success_message;
	
    //use ajax to save the folder
    $.ajax({url: "<?php echo site_url("document/folder/create_folder")?>", data : { folder_name: folder_name , folder_title:folder_title , folder_description:folder_description , parent_id:parent_id },
        type: "POST",
    	success: function(result){
			if(result == 1)
			{
				// Hide the modal
				$('#createNewFolderModal').modal('hide');
				
				// Refresh the page
				location.reload();
			}
			else if(result == 0)
			{
				// Show error - 'Folder already exists'
				check_folder_action(1);
			}
		}
	});

} 

// EDIT FOLDER: Function to save edited folder
function save_edit_folder(){

    //get the folder details
    var folder_name = $('#edit_folder_name').val();
	var folder_title = $('#edit_folder_title').val();
	var folder_description = $('#edit_folder_description').val();
	var parent_id = $('#parent_id').val();
	var folder_id = $('#folder_id').val();
    
	var success_message;
	
    //use ajax to save the folder
    $.ajax({url: "<?php echo site_url("document/folder/edit_folder")?>", data : { folder_name: folder_name , folder_title:folder_title , folder_description:folder_description , parent_id:parent_id, folder_id: folder_id},
        type: "POST",
    	success: function(result){
			if(result == 1)
			{
				// Hide the modal
				$('#editFolderModal').modal('hide');
				
				// Refresh the page
				location.reload();
			}
			else if(result == 0)
			{
				// Show error - 'Folder already exists'
				check_folder_action_for_edit(1);
			}
		}
	});

}

// DELETE FOLDER: Function to delete a folder
function save_delete_folder(){

    //get the folder details
	var folder_id = $('#delete_folder_id').val();
    
	var success_message;
	
    //use ajax to save the folder
    $.ajax({url: "<?php echo site_url("document/folder/delete_folder")?>", data : {folder_id: folder_id},
        type: "POST",
    	success: function(result){
			if(result == 1)
			{
				// Hide the modal
				$('#deleteFolderModal').modal('hide');
				
				// Refresh the page
				location.reload();
			}
			else if(result == 0)
			{
				// Show error - 'Folder already exists'
				check_folder_action_for_delete(1);
			}
		}
	});
}

/* Actions after AJAX calls */

// CREATE FOLDER: Actions if create folder fails
function check_folder_action(result){
	//if the result is 1
	var is_exist = result;
	if(result == 1)
	{
		$('#folder_name').parent().parent().parent().attr('class', 'control-group error');
		$('#folder_name').attr('class', 'inputError');
		$('#folder_name_already_exists').show();
	}
	else
	{
		$('#folder_name').parent().parent().parent().attr('class', 'control-group');
		$('#folder_name').attr('class', '');
		$('#folder_name_already_exists').hide();
	}
	return is_exist;
}

// EDIT FOLDER: Actions if edit folder fails
function check_folder_action_for_edit(result){
	//if the result is 1
	var is_exist = result;
	if(result == 1)
	{
		$('#edit_folder_name').parent().parent().parent().attr('class', 'control-group error');
		$('#edit_folder_name').attr('class', 'inputError');
		$('#edit_folder_name_already_exists').show();
	}
	else
	{
		$('#edit_folder_name').parent().parent().parent().attr('class', 'control-group');
		$('#edit_folder_name').attr('class', '');
		$('#edit_folder_name_already_exists').hide();
	}
	return is_exist;
}

// DELETE FOLDER: Actions if delete folder fails
function check_folder_action_for_delete(result){
	if(result == 1)
	{
		$('#delete_folder_error_div').show();
		$('#delete_folder_confirmation_message').hide();
		$('#save_delete_folder').hide();
	}
	else
	{
		$('#delete_folder_error_div').hide();
		$('#delete_folder_confirmation_message').show();
		$('#save_delete_folder').show();
	}
}

// Function to select documents/folders/all
function select_files_or_folders(type) {
	// Reset all checkboxes
	$('.select-folders').parent().attr('class', '');
	$('.select-files').parent().attr('class', '');
	
	if (type == 1) {
		$('.select-folders').parent().attr('class', 'checked');
		$('.select-folders').attr('checked', true);
	}
	if (type == 2) {
		$('.select-files').parent().attr('class', 'checked');
		$('.select-files').attr('checked', true);
	}
	if (type == 3) {
		$('.select-folders').parent().attr('class', 'checked');
		$('.select-files').parent().attr('class', 'checked');
		$('.select-folders').attr('checked', true);
		$('.select-files').attr('checked', true);
	}
	if (type == 4) {
		$('.select-folders').parent().attr('class', '');
		$('.select-files').parent().attr('class', '');
		$('.select-folders').attr('checked', false);
		$('.select-files').attr('checked', false);
	}
	return false;
}

function submit_mass_action_form(action) {
	
	var files_checked = $('input[name="files[]"]:checked').length;
	var folders_checked = $('input[name="folders[]"]:checked').length;

	if ((files_checked > 0 || folders_checked > 0) && action == 3)
	{
		//$('#mass_action').val(action);
		//$('#massActionForm').submit();
		$('#deletefilesModal').modal('show');
	}

	return false;
}
jQuery(function($) {
	$("#save_delete_files_multiple").click(function() {
		$('#deletefilesModal').modal('hide');
		
		var files_checked = $('input[name="files[]"]:checked').length;
		var folders_checked = $('input[name="folders[]"]:checked').length;
		
		if (files_checked > 0 || folders_checked > 0)
		{
			$('#mass_action').val(3);
			$('#massActionForm').submit();
		}
	});
});

</script>
<!-- Create New Folder Modal - End -->
	
<script type="text/javascript">
jQuery(function($) {
	$(".document_rows").mouseover(function() {
		$(this).find('div.folder-action-links').show();
		//var html = $(this).find("td:first").html();
		//alert(html);
	});
	$(".document_rows").mouseout(function() {
		$(this).find('div.folder-action-links').hide();
		//$(this).find('div').hide();
	});
	
	$(".file-name").draggable({ 
		revert: true, 
		start: function(event, ui) {
			$('span:first' , this).attr('class' , 'icon32 icon-green icon-document'); 
		},
        stop: function(event, ui) { 
			$('span:first' , this).attr('class' , 'icon32 icon-color icon-document'); 
		} 
	});
	$( ".folder-name" ).droppable({
		drop: function( event, ui ) {
			var file_id = $(ui.draggable).attr('id');
			var folder_id = $(this).attr('id');
			drag_and_drop_update_file_folder(folder_id, file_id);
			
			if (folder_id == '' || folder_id == 'undefined') {
				ui.draggable.data("originalPosition",
					ui.draggable.data("draggable").originalPosition);
			}
		}
	});
});

// DRAG and DROP: Update file folder
function drag_and_drop_update_file_folder(folder_id, file_id){
    //use ajax to save the folder
    $.ajax({url: "<?php echo site_url("document/document/drag_and_drop_update_file_folder")?>", data : {folder_id: folder_id , file_id: file_id},
        type: "POST",
    	success: function(result){
			location.reload();
		}
	});
}

$(document).ready(function() {

    //the min chars for username
    var min_chars = 3;
    
    //result texts
    var characters_error = 'Please type atleast 3 characters to check availability';
    var checking_html = '<img src="<?php echo base_url();?>misc/website/img/loading.gif">&nbsp;Checking...';
    
    //when button is clicked
    $('#check_username_availability').click(function(){
    	//run the character number check
    	if($('#username').val().length < min_chars){
    		//if it's bellow the minimum show characters_error text '
    		$('#username_availability_result').html(characters_error);
    	}else{
    		//else show the cheking_text and run the function to check
    		$('#username_availability_result').html(checking_html);
    		check_availability();
    	}
    });
});

//function to check username availability
function check_availability(){

    //get the username
    var username = $('#username').val();
    
    //use ajax to run the check
    $.ajax({url: "<?php echo site_url("user/register/check")?>", data : { username: username },
        type: "POST",
    	success: function(result){
    		//if the result is 1
    		if(result == 1){
    			//show that the username is available
    			$('#username_availability_result').html('<font color="#5BB25B">' + username + ' is available</font>');
    		}else{
    			//show that the username is NOT available
    			$('#username_availability_result').html('<font color="#D04A48">' + username + ' is already taken</font>');
    		}
    }});

} 

</script>
<script type="text/javascript">
  jQuery(function() {
	  var qsurl = jQuery('#quickSearchForm').attr("action");
	  $( "#quickFileSearch" ).autocomplete({
	      source: function( request, response ) {
	        $.ajax({
	          url: qsurl,
	          dataType: "json",
	          data: {	            
	            search: request.term
	          },
	          success: function( data ) {
		          
	        	  response( $.map( data, function( item ) {
	                  return {
	                    label: item.file_name,
	                    value: item.link
	                  }
	                }));
	          }
	        });
	      },
	      minLength: 2,
	      select: function( event, ui ) {
	        location.href = ui.item.value;
	      }
	    });
	  });
 
  </script>


<?php if (isset($error_message) && ($error_message != '')): ?>  
<script type="text/javascript">
	jQuery(function($) {
		$('#error_message_link').click();
	});
</script>
<?php endif; ?>

<?php if (isset($message) && ($message != '')): ?>  
<script type="text/javascript">
	jQuery(function($) {
		$('#success_message_link').click();
	});
</script>
<?php endif; ?>

</body>
</html>