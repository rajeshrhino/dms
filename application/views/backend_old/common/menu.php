<!-- Custom jquery scripts -->
<script src="<?php echo base_url();?>misc/common/js/custom_backend.js" type="text/javascript"></script>

<link rel="stylesheet" media="all" href="<?php echo base_url();?>misc/common/css/backend.css"/>
<!--  start nav-outer-repeat................................................................................................. START -->
<div class="nav-outer-repeat"> 
<!--  start nav-outer -->
<div class="nav-outer"> 

		<!-- start nav-right -->
		<div id="nav-right">
		
			<div class="nav-divider">&nbsp;</div>
			<div class="showhide-account"><img src="<?php echo base_url(); ?>misc/common/images/nav/nav_myaccount.gif" width="93" height="14" alt="" /></div>
			<div class="nav-divider">&nbsp;</div>
			<?php echo anchor('logout', '<img src="'.base_url().'misc/common/images/nav/nav_logout.gif" width="64" height="14" alt="" />', array('id' => 'logout')); ?>
			<div class="clear">&nbsp;</div>
		
			<!--  start account-content -->	
			<div class="account-content">
			<div class="account-drop-inner">
            
				<?php echo anchor('accountsettings', 'Account Settings', array('id' => 'acc-settings')); ?>
				<div class="clear">&nbsp;</div>
				<div class="acc-line">&nbsp;</div>
                
				<a href="" id="acc-details">Personal details </a>
				<div class="clear">&nbsp;</div>
				<div class="acc-line">&nbsp;</div>
                
				<!--<a href="" id="acc-project">Project details</a>
				<div class="clear">&nbsp;</div>
				<div class="acc-line">&nbsp;</div>-->
                
				<a href="" id="acc-inbox">Inbox</a>
                
				<!--<div class="clear">&nbsp;</div>
				<div class="acc-line">&nbsp;</div>
				<a href="" id="acc-stats">Statistics</a>--> 
			</div>
			</div>
			<!--  end account-content -->
		
		</div>
		<!-- end nav-right -->


		<!--  start nav -->
		<div class="nav">
		<div class="table">
		
        <?php $menus = config_item('menus');?>
    	<?php foreach ( $menus['backend'] as $key => $value): ?>
           <ul <?php if ($this->uri->segment(2) == $value['key']): ?> class="current"<?php else: ?> class="select"<?php endif; ?>><li><?php echo anchor('backend/'.$value['link_to'], '<b>'.$value['label'].'</b>', array('id' => $value['key'])); ?><!--[if IE 7]><!--></a><!--<![endif]-->
            	<!--[if lte IE 6]><table><tr><td><![endif]-->
        		<div class="select_sub<?php if ($this->uri->segment(2) == $value['key']): ?> show<?php endif; ?>">
        			<ul class="sub">
                        <?php foreach ( $value['child'] as $child_key => $child_value): ?>
                            <li <?php if ($this->uri->segment(3) == $child_value['key']): ?> class="sub_show"<?php endif; ?>><?php echo anchor('backend/'.$child_value['link_to'], $child_value['label'], array('id' => $child_value['key'])); ?></li>
                        <?php endforeach; ?>
        			</ul>
        		</div>
        		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
           </li>
		   </ul> 
           <div class="nav-divider">&nbsp;</div>
    	<?php endforeach; ?>
        
		<div class="clear"></div>
		</div>
		<div class="clear"></div>
		</div>
		<!--  start nav -->

</div>
<div class="clear"></div>
<!--  start nav-outer -->
</div>
<!--  start nav-outer-repeat................................................... END -->