<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>{ _int } DMS </title>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- MAIN CSS -->
	<link rel="stylesheet" media="all" href="<?php echo base_url();?>misc/common/css/style.css"/>
    <!-- SKIN -->
	<link rel="stylesheet" media="all" href="<?php echo base_url();?>misc/common/css/skin.css"/>
    
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<!-- Adding "maximum-scale=1" fixes the Mobile Safari auto-zoom bug: http://filamentgroup.com/examples/iosScaleBug/ -->
	
	<!-- JS -->
	
	<script src="<?php echo base_url()?>misc/common/js/jquery-1.7.1.js"></script>
    <script src="<?php echo base_url();?>misc/common/js/custom.js"></script>
    
	<!-- Tweet -->
	<link rel="stylesheet" href="<?php echo base_url();?>misc/common/css/jquery.tweet.css" media="all"  /> 
	<script src="<?php echo base_url();?>misc/common/js/jquery.tweet.js" ></script> 
	<!-- ENDS Tweet -->
	
	<!-- superfish -->
	<link rel="stylesheet" media="screen" href="<?php echo base_url();?>misc/common/css/superfish.css" /> 
	<script  src="<?php echo base_url();?>misc/common/js/hoverIntent.js"></script>
	<script  src="<?php echo base_url();?>misc/common/js/superfish.js"></script>
	<script  src="<?php echo base_url();?>misc/common/js/supersubs.js"></script>
	<!-- ENDS superfish -->
	
	<!-- poshytip -->
	<link rel="stylesheet" href="<?php echo base_url();?>misc/common/css/tip-twitter.css"  />
	<script  src="<?php echo base_url();?>misc/common/js/jquery.poshytip.min.js"></script>
	<!-- ENDS poshytip -->
	
    <!-- prettyPhoto -->
    <script  src="<?php echo base_url();?>misc/common/js/jquery.prettyPhoto.js"></script>
    <link rel="stylesheet" href="<?php echo base_url();?>misc/common/css/prettyPhoto.css"  media="screen" />
    <!-- ENDS prettyPhoto -->
    
	<!-- GOOGLE FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'/>
	
	<!-- Flex Slider -->
	<link rel="stylesheet" href="<?php echo base_url();?>misc/common/css/flexslider.css" />
	<script src="<?php echo base_url();?>misc/common/js/jquery.flexslider-min.js"></script>
	<!-- ENDS Flex Slider -->
    
    <!-- Masonry -->
    <script src="<?php echo base_url();?>misc/common/js/masonry.min.js" ></script>
    <script src="<?php echo base_url();?>misc/common/js/imagesloaded.js" ></script>
    <!-- ENDS Masonry -->
	
	<!-- modernizr -->
	<script src="<?php echo base_url();?>misc/common/js/modernizr.js"></script>
        	    
    
</head>
<body lang="en">

	<header>
		<div class="wrapper clearfix">
			
			<div id="logo">
				<!--<a href="home"><img  src="<?php echo base_url();?>misc/common/images/logo.png" alt="{ _int } DMS"></a>-->
			</div>
			
            <!-- nav -->
			<ul id="nav" class="sf-menu">
                    
                <!-- load the website menus -->    

				<!-- Load the website menu only for frontend and not for backend -->	
				<?php if ($this->uri->segment(1) !== 'backend'): ?>		
					<?php $menus = config_item('menus');?>
					<?php foreach ( $menus['website'] as $key => $value): ?> 
					   <li<?php if ($this->uri->segment(1) == $key): ?> class="current-menu-item"<?php endif; ?>><?php echo anchor($key, $value); ?></li>
					<?php endforeach; ?>
						
				<?php else: ?>

					<div id="top-search">
    
					
					<!--  start top-search -->
					<table border="0" cellpadding="0" cellspacing="0">
					<tr>
					<td><input type="text" value="Search" onblur="if (this.value=='') { this.value='Search'; }" onfocus="if (this.value=='Search') { this.value=''; }" class="top-search-inp" /></td>
					<!--<td>
					<select  class="styledselect">
						<option value="">All</option>
						<option value="">Products</option>
						<option value="">Categories</option>
						<option value="">Clients</option>
						<option value="">News</option>
					</select> 
					</td>-->
					<td>
					<input type="image" src="<?php echo base_url(); ?>misc/common/images/top_search_btn.gif"  />
					</td>
					</tr>
					</table>
					<!--  end top-search -->
						
					    
					
				</div>	

				<?php endif; ?>
                
            	<!--<li><a href="blog.html">BLOG</a></li>
				<li><a href="page.html">ABOUT</a>
					<ul>
						<li><a href="page-elements.html">Elements</a></li>
						<li><a href="page-icons.html">Icons</a></li>
						<li><a href="page-typography.html">Typography</a></li>
					</ul>
				</li>
				<li><a href="portfolio.html">WORK</a></li>
				<li><a href="contact.html">CONTACT</a></li>
				<li><a href="http://luiszuno.com/blog/downloads/simpler-template">DOWNLOAD NOW</a></li>-->
                
			</ul>
			<!-- ends nav -->
			
			<!-- comboNav -->
			<!--<select id="comboNav">
				<option value="index.html" selected="selected">HOME</option>
				<option value="blog.html">BLOG</option>
				<option value="page.html">ABOUT</option>
				<option value="page-elements.html">- Elements -</option>
				<option value="page-icons.html">- Icons -</option>
				<option value="page-typography.html">- Typography -</option>
				<option value="portfolio.html">WORK</option>
				<option value="http://luiszuno.com/blog/downloads/simpler-template">DOWNLOAD NOW</option>
			</select>-->
			<!-- comboNav -->
			
            <?php if ($this->router->fetch_class() == 'home'): ?>
            
            <!-- slider holder -->
			<div class="clearfix"></div>
            
			<div id="slider-holder" class="clearfix">
				
				<!-- slider -->
		        <div class="flexslider home-slider">
				  <ul class="slides">
				    <li>
				      <img src="<?php echo base_url();?>misc/common/images/slides/01.jpg" alt="Apache" />
                      <p class="flex-caption"><a href='http://httpd.apache.org/' target='_blank'>Apache</a></p>
				    </li>
				    <li>
				      <img src="<?php echo base_url();?>misc/common/images/slides/02.jpg" alt="PHP" />
                      <p class="flex-caption"><a href='http://php.net/' target='_blank'>PHP</a></p>
				    </li>
				    <li>
				      <img src="<?php echo base_url();?>misc/common/images/slides/03.jpg" alt="MongoDB" />
                      <p class="flex-caption"><a href='http://www.mongodb.org/' target='_blank'>MongoDB</a></p>
				    </li>
				  </ul>
				</div>
	        	<!-- ENDS slider -->
	        	
	        	<div class="home-slider-clearfix "></div>
	        	
	        	<!-- Headline -->
	        	<div id="headline">
					<p class="headline-text">WELCOME TO</p> 
	        		<h4>INTELLIGENT DOCUMENT MANAGEMENT SYSTEM</h4>
	        		<p class="headline-text">A state of art document management system.</p> 
	        		<p class="headline-text">Combining the power of Apache, PHP and MongoDB</a></p>
	        	</div>
	        	<!-- ENDS headline -->
	        	
	        	
			</div>
			<!-- ENDS slider holder -->
            
            <?php endif;?>
			
		</div>
	</header>