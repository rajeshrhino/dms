<footer>			
		<div class="wrapper clearfix">			                        
			<!-- bottom -->
			<div class="footer-bottom">
				<div class="left">{ _int } DMS &copy; Copyright Rajesh Sundararajan.</div>
				<div class="right">
					<ul id="social-bar">
						<li><a href="http://www.facebook.com"  title="Become a fan" class="poshytip"><img src="<?php echo base_url();?>misc/common/images/social/facebook.png"  alt="Facebook" /></a></li>
						<li><a href="http://www.twitter.com" title="Follow my tweets" class="poshytip"><img src="<?php echo base_url();?>misc/common/images/social/twitter.png"  alt="twitter" /></a></li>
						<li><a href="http://www.google.com"  title="Add to the circle" class="poshytip"><img src="<?php echo base_url();?>misc/common/images/social/plus.png" alt="Google plus" /></a></li>
					</ul>
				</div>
			</div>	
			<!-- ENDS bottom -->		

		</div>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo base_url()?>misc/upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?php echo base_url()?>misc/upload/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo base_url()?>misc/upload/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?php echo base_url()?>misc/upload/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS and Bootstrap Image Gallery are not required, but included for the demo -->
<script src="<?php echo base_url()?>misc/upload/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>misc/upload/js/bootstrap-image-gallery.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo base_url()?>misc/upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo base_url()?>misc/upload/js/jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script src="<?php echo base_url()?>misc/upload/js/jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo base_url()?>misc/upload/js/jquery.fileupload-ui.js"></script>

<!-- The main application script -->
<script src="<?php echo base_url()?>misc/upload/js/main.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
<!--[if gte IE 8]><script src="js/cors/jquery.xdr-transport.js"></script><![endif]-->	

	</footer>
</body>
</html>